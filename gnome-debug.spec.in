Name:		gnome-debug
Summary:	GNOME debugging framework.
Version:	@VERSION@
Release:	1
License:	GPL
Group:		Development/Libraries
Source:		%{name}-%{version}.tar.gz
BuildRoot:	/var/tmp/%{name}-%{version}-root
URL:		http://www.gnome.org
Requires:	oaf >= @OAF_REQUIRED@
Requires:	gnome-vfs >= @GNOME_VFS_REQUIRED@
Requires:	gnome-libs >= @GNOME_LIBS_REQUIRED@
Requires:	bonobo >= @BONOBO_REQUIRED@
Requires:	libglade >= @LIBGLADE_REQUIRED@
Requires:	gal >= @GAL_REQUIRED@
Requires:	gdl >= @GDL_REQUIRED@
BuildRequires:  gtk+-devel >= @GTK_REQUIRED@
BuildRequires:  oaf-devel >= @OAF_REQUIRED@
BuildRequires:  gnome-vfs-devel >= @GNOME_VFS_REQUIRED@
BuildRequires:  gnome-libs-devel >= @GNOME_LIBS_REQUIRED@
BuildRequires:  bonobo-devel >= @BONOBO_REQUIRED@
BuildRequires:	libglade-devel >= @LIBGLADE_REQUIRED@
BuildRequires:	gal-devel >= @GAL_REQUIRED@
BuildRequires:	gdl-devel >= @GDL_REQUIRED@

%description
This is the GNOME Debugging Framework (GDF).  GDF provides a number of 
debugging services to applications such as Integrated Development 
Environments and standalone debugging tools similar to DDD.

If you are interested, the dryad package contains a DDD-style frontend to 
GDF.

%package devel
Summary:	Libraries and include files for gnome-debug.
Group:		Development/Libraries
Requires:	%{name} = %{version}

%description devel 
Libraries and header files if you want to make use of the GNOME debug framework
in your own programs.


%prep
%setup -q

%build
./configure --prefix=%{_prefix} \
    --bindir=%{_bindir} --mandir=%{_mandir} \
    --localstatedir=%{_localstatedir} --libdir=%{_libdir} \
    --datadir=%{_datadir} --includedir=%{_includedir} \
    --sysconfdir=%{_sysconfdir}

CFLAGS="$RPM_OPT_FLAGS" make

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{_prefix} bindir=$RPM_BUILD_ROOT%{_bindir} \
    mandir=$RPM_BUILD_ROOT%{_mandir} libdir=$RPM_BUILD_ROOT%{_libdir} \
    localstatedir=$RPM_BUILD_ROOT%{_localstatedir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} \
    sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} install


%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%post
/sbin/ldconfig

%postun
/sbin/ldconfig


%files
%doc AUTHORS COPYING ChangeLog NEWS README
%defattr (-, root, root)
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_datadir}/pixmaps/*.xpm
%{_datadir}/gnome-debug
%{_datadir}/locale/*/*/*
%{_datadir}/oaf/*.oaf

%files devel
%defattr (-, root, root)
%{_includedir}/gdf
%{_libdir}/*a
%{_libdir}/*so
%{_libdir}/*sh
%{_datadir}/idl/*.idl


%changelog
* Wed Aug 8 2001 Jens Finke <jens@gnome.org>
- created spec file