#!/bin/sh

# This is a quick hack (stolen from nautilus) to check if any gdf auxiliary 
# processes are running, and if so, list them and kill them.  It is not
# portable, and should be be expected to be used in any kind of
# production capacity.


# Add any new auxiliary programs here.
AUX_PROGS="gdf-libdryad-backend dryad";

unset FOUND_ANY;

for NAME in $AUX_PROGS; do

    EGREP_PATTERN=`echo $NAME | sed -e 's/\(.\)\(.*\)/[\1]\2/' | sed -e 's/\[\\\^\]/\[\\^\]/'`;

    COUNT=`ps auxww | egrep $EGREP_PATTERN | wc -l`;

    if [ $COUNT -gt 0 ]; then
	if [ -z $FOUND_ANY ]; then
	    echo "Stale Processes Found";
	    FOUND_ANY=true;
	fi
	echo "$NAME: $COUNT";
	killall "$NAME";
    fi
done


if [ -z $FOUND_ANY ]; then
    echo "No Stale Processes Found";
    exit 0;
fi

exit -1;
