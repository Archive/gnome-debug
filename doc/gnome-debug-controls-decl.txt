<FUNCTION>
<NAME>init_control_factories</NAME>
<RETURNS>void  </RETURNS>
void
</FUNCTION>
<STRUCT>
<NAME>GdfBreakpointManager</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfBreakpointManagerClass</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfBreakpointManagerPriv</NAME>
</STRUCT>
<MACRO>
<NAME>GDF_BREAKPOINT_MANAGER_TYPE</NAME>
#define GDF_BREAKPOINT_MANAGER_TYPE        (gdf_breakpoint_manager_get_type ())
</MACRO>
<MACRO>
<NAME>GDF_BREAKPOINT_MANAGER</NAME>
#define GDF_BREAKPOINT_MANAGER(o)          (GTK_CHECK_CAST ((o), GDF_BREAKPOINT_MANAGER_TYPE, GdfBreakpointManager))
</MACRO>
<MACRO>
<NAME>GDF_BREAKPOINT_MANAGER_CLASS</NAME>
#define GDF_BREAKPOINT_MANAGER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDF_BREAKPOINT_MANAGER_TYPE, GdfBreakpointManagerClass))
</MACRO>
<MACRO>
<NAME>GDF_IS_BREAKPOINT_MANAGER</NAME>
#define GDF_IS_BREAKPOINT_MANAGER(o)       (GTK_CHECK_TYPE ((o), GDF_BREAKPOINT_MANAGER_TYPE))
</MACRO>
<MACRO>
<NAME>GDF_IS_BREAKPOINT_MANAGER_CLASS</NAME>
#define GDF_IS_BREAKPOINT_MANAGER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_BREAKPOINT_MANAGER_TYPE))
</MACRO>
<STRUCT>
<NAME>GdfBreakpointManager</NAME>
struct GdfBreakpointManager {
	GtkFrame frame;
	
	GdfBreakpointManagerPriv *priv;
};
</STRUCT>
<STRUCT>
<NAME>GdfBreakpointManagerClass</NAME>
struct GdfBreakpointManagerClass {
	GtkFrameClass parent_class;
};
</STRUCT>
<FUNCTION>
<NAME>gdf_breakpoint_manager_get_type</NAME>
<RETURNS>GtkType  </RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_breakpoint_manager_new</NAME>
<RETURNS>GdfBreakpointManager  *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_breakpoint_manager_set_debugger</NAME>
<RETURNS>void  </RETURNS>
GdfBreakpointManager *bpm,GdfDebuggerClient *dbg
</FUNCTION>
<STRUCT>
<NAME>GdfLocalsViewer</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfLocalsViewerClass</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfLocalsViewerPriv</NAME>
</STRUCT>
<MACRO>
<NAME>GDF_LOCALS_VIEWER_TYPE</NAME>
#define GDF_LOCALS_VIEWER_TYPE        (gdf_locals_viewer_get_type ())
</MACRO>
<MACRO>
<NAME>GDF_LOCALS_VIEWER</NAME>
#define GDF_LOCALS_VIEWER(o)          (GTK_CHECK_CAST ((o), GDF_LOCALS_VIEWER_TYPE, GdfLocalsViewer))
</MACRO>
<MACRO>
<NAME>GDF_LOCALS_VIEWER_CLASS</NAME>
#define GDF_LOCALS_VIEWER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDF_LOCALS_VIEWER_TYPE, GdfLocalsViewerClass))
</MACRO>
<MACRO>
<NAME>GDF_IS_LOCALS_VIEWER</NAME>
#define GDF_IS_LOCALS_VIEWER(o)       (GTK_CHECK_TYPE ((o), GDF_LOCALS_VIEWER_TYPE))
</MACRO>
<MACRO>
<NAME>GDF_IS_LOCALS_VIEWER_CLASS</NAME>
#define GDF_IS_LOCALS_VIEWER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_LOCALS_VIEWER_TYPE))
</MACRO>
<STRUCT>
<NAME>GdfLocalsViewer</NAME>
struct GdfLocalsViewer {
	GdfVariableViewer parent;
};
</STRUCT>
<STRUCT>
<NAME>GdfLocalsViewerClass</NAME>
struct GdfLocalsViewerClass {
	GdfVariableViewerClass parent_class;
};
</STRUCT>
<FUNCTION>
<NAME>gdf_locals_viewer_get_type</NAME>
<RETURNS>GtkType  </RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_locals_viewer_new</NAME>
<RETURNS>GdfLocalsViewer  *</RETURNS>
void
</FUNCTION>
<STRUCT>
<NAME>GdfRegisterViewer</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfRegisterViewerClass</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfRegisterViewerPriv</NAME>
</STRUCT>
<MACRO>
<NAME>GDF_REGISTER_VIEWER_TYPE</NAME>
#define GDF_REGISTER_VIEWER_TYPE        (gdf_register_viewer_get_type ())
</MACRO>
<MACRO>
<NAME>GDF_REGISTER_VIEWER</NAME>
#define GDF_REGISTER_VIEWER(o)          (GTK_CHECK_CAST ((o), GDF_REGISTER_VIEWER_TYPE, GdfRegisterViewer))
</MACRO>
<MACRO>
<NAME>GDF_REGISTER_VIEWER_CLASS</NAME>
#define GDF_REGISTER_VIEWER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDF_REGISTER_VIEWER_TYPE, GdfRegisterViewerClass))
</MACRO>
<MACRO>
<NAME>GDF_IS_REGISTER_VIEWER</NAME>
#define GDF_IS_REGISTER_VIEWER(o)       (GTK_CHECK_TYPE ((o), GDF_REGISTER_VIEWER_TYPE))
</MACRO>
<MACRO>
<NAME>GDF_IS_REGISTER_VIEWER_CLASS</NAME>
#define GDF_IS_REGISTER_VIEWER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_REGISTER_VIEWER_TYPE))
</MACRO>
<STRUCT>
<NAME>GdfRegisterViewer</NAME>
struct GdfRegisterViewer {
    GtkFrame frame;

    GdfRegisterViewerPriv *priv;
};
</STRUCT>
<STRUCT>
<NAME>GdfRegisterViewerClass</NAME>
struct GdfRegisterViewerClass {
    GtkFrameClass parent_class;
};
</STRUCT>
<FUNCTION>
<NAME>gdf_register_viewer_get_type</NAME>
<RETURNS>GtkType  </RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_register_viewer_new</NAME>
<RETURNS>GtkWidget  *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_register_viewer_set_debugger</NAME>
<RETURNS>void  </RETURNS>
GdfRegisterViewer *rv,GdfDebuggerClient *dbg
</FUNCTION>
<STRUCT>
<NAME>GdfSourceViewerManager</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfSourceViewerManagerClass</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfSourceViewerManagerPriv</NAME>
</STRUCT>
<MACRO>
<NAME>GDF_SOURCE_VIEWER_MANAGER_TYPE</NAME>
#define GDF_SOURCE_VIEWER_MANAGER_TYPE        (gdf_source_viewer_manager_get_type ())
</MACRO>
<MACRO>
<NAME>GDF_SOURCE_VIEWER_MANAGER</NAME>
#define GDF_SOURCE_VIEWER_MANAGER(o)          (GTK_CHECK_CAST ((o), GDF_SOURCE_VIEWER_MANAGER_TYPE, GdfSourceViewerManager))
</MACRO>
<MACRO>
<NAME>GDF_SOURCE_VIEWER_MANAGER_CLASS</NAME>
#define GDF_SOURCE_VIEWER_MANAGER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDF_SOURCE_VIEWER_MANAGER_TYPE, GdfSourceViewerManagerClass))
</MACRO>
<MACRO>
<NAME>GDF_IS_SOURCE_VIEWER_MANAGER</NAME>
#define GDF_IS_SOURCE_VIEWER_MANAGER(o)       (GTK_CHECK_TYPE ((o), GDF_SOURCE_VIEWER_MANAGER_TYPE))
</MACRO>
<MACRO>
<NAME>GDF_IS_SOURCE_VIEWER_MANAGER_CLASS</NAME>
#define GDF_IS_SOURCE_VIEWER_MANAGER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_SOURCE_VIEWER_MANAGER_TYPE))
</MACRO>
<STRUCT>
<NAME>GdfSourceViewerManager</NAME>
struct GdfSourceViewerManager {
	GtkVBox parent;

	GdfSourceViewerManagerPriv *priv;
};
</STRUCT>
<STRUCT>
<NAME>GdfSourceViewerManagerClass</NAME>
struct GdfSourceViewerManagerClass {
	GtkVBoxClass parent_class;
};
</STRUCT>
<FUNCTION>
<NAME>gdf_source_viewer_manager_get_type</NAME>
<RETURNS>GtkType  </RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_manager_new</NAME>
<RETURNS>GdfSourceViewerManager  *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_manager_view_source</NAME>
<RETURNS>GtkWidget  *</RETURNS>
GdfSourceViewerManager *svm,const gchar *file_name
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_manager_set_debugger</NAME>
<RETURNS>void  </RETURNS>
GdfSourceViewerManager *svm,GdfDebuggerClient *dbg
</FUNCTION>
<STRUCT>
<NAME>GdfSourceViewer</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfSourceViewerClass</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfSourceViewerPriv</NAME>
</STRUCT>
<MACRO>
<NAME>GDF_SOURCE_VIEWER_TYPE</NAME>
#define GDF_SOURCE_VIEWER_TYPE        (gdf_source_viewer_get_type ())
</MACRO>
<MACRO>
<NAME>GDF_SOURCE_VIEWER</NAME>
#define GDF_SOURCE_VIEWER(o)          (GTK_CHECK_CAST ((o), GDF_SOURCE_VIEWER_TYPE, GdfSourceViewer))
</MACRO>
<MACRO>
<NAME>GDF_SOURCE_VIEWER_CLASS</NAME>
#define GDF_SOURCE_VIEWER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDF_SOURCE_VIEWER_TYPE, GdfSourceViewerClass))
</MACRO>
<MACRO>
<NAME>GDF_IS_SOURCE_VIEWER</NAME>
#define GDF_IS_SOURCE_VIEWER(o)       (GTK_CHECK_TYPE ((o), GDF_SOURCE_VIEWER_TYPE))
</MACRO>
<MACRO>
<NAME>GDF_IS_SOURCE_VIEWER_CLASS</NAME>
#define GDF_IS_SOURCE_VIEWER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_SOURCE_VIEWER_TYPE))
</MACRO>
<STRUCT>
<NAME>GdfSourceViewer</NAME>
struct GdfSourceViewer {
    GtkFrame frame;
	
    gchar *source_path;
    gint current_line;

    GdfSourceViewerPriv *priv;
};
</STRUCT>
<STRUCT>
<NAME>GdfSourceViewerClass</NAME>
struct GdfSourceViewerClass {
    GtkFrameClass parent_class;
};
</STRUCT>
<FUNCTION>
<NAME>gdf_source_viewer_get_type</NAME>
<RETURNS>GtkType  </RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_new</NAME>
<RETURNS>GtkWidget  *</RETURNS>
const gchar *source_path
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_add_breakpoint</NAME>
<RETURNS>void  </RETURNS>
GdfSourceViewer *sv,gint bp_num, gint line_num,gboolean enabled
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_enable_breakpoint</NAME>
<RETURNS>void  </RETURNS>
GdfSourceViewer *sv, gint bp_num,gboolean enabled
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_remove_breakpoint</NAME>
<RETURNS>void  </RETURNS>
GdfSourceViewer *sv, gint bp_num
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_has_breakpoint</NAME>
<RETURNS>gboolean  </RETURNS>
GdfSourceViewer *sv, gint bp_num
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_view_line</NAME>
<RETURNS>void  </RETURNS>
GdfSourceViewer *sv, gint line_num
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_set_current_line</NAME>
<RETURNS>void  </RETURNS>
GdfSourceViewer *sv, gint line_num
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_clear_current_line</NAME>
<RETURNS>void  </RETURNS>
GdfSourceViewer *sv
</FUNCTION>
<FUNCTION>
<NAME>gdf_source_viewer_set_debugger</NAME>
<RETURNS>void  </RETURNS>
GdfSourceViewer *sv,GdfDebuggerClient *dbg
</FUNCTION>
<STRUCT>
<NAME>GdfStackBrowser</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfStackBrowserClass</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfStackBrowserPriv</NAME>
</STRUCT>
<MACRO>
<NAME>GDF_STACK_BROWSER_TYPE</NAME>
#define GDF_STACK_BROWSER_TYPE        (gdf_stack_browser_get_type ())
</MACRO>
<MACRO>
<NAME>GDF_STACK_BROWSER</NAME>
#define GDF_STACK_BROWSER(o)          (GTK_CHECK_CAST ((o), GDF_STACK_BROWSER_TYPE, GdfStackBrowser))
</MACRO>
<MACRO>
<NAME>GDF_STACK_BROWSER_CLASS</NAME>
#define GDF_STACK_BROWSER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDF_STACK_BROWSER_TYPE, GdfStackBrowserClass))
</MACRO>
<MACRO>
<NAME>GDF_IS_STACK_BROWSER</NAME>
#define GDF_IS_STACK_BROWSER(o)       (GTK_CHECK_TYPE ((o), GDF_STACK_BROWSER_TYPE))
</MACRO>
<MACRO>
<NAME>GDF_IS_STACK_BROWSER_CLASS</NAME>
#define GDF_IS_STACK_BROWSER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_STACK_BROWSER_TYPE))
</MACRO>
<STRUCT>
<NAME>GdfStackBrowser</NAME>
struct GdfStackBrowser {
    GtkFrame frame;

    GdfStackBrowserPriv *priv;
};
</STRUCT>
<STRUCT>
<NAME>GdfStackBrowserClass</NAME>
struct GdfStackBrowserClass {
    GtkFrameClass parent_class;
};
</STRUCT>
<FUNCTION>
<NAME>gdf_stack_browser_get_type</NAME>
<RETURNS>GtkType  </RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_stack_browser_new</NAME>
<RETURNS>GtkWidget  *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_stack_browser_set_debugger</NAME>
<RETURNS>void  </RETURNS>
GdfStackBrowser *sb,GdfDebuggerClient *dbg
</FUNCTION>
<STRUCT>
<NAME>GdfVariableViewer</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfVariableViewerClass</NAME>
</STRUCT>
<STRUCT>
<NAME>GdfVariableViewerPriv</NAME>
</STRUCT>
<MACRO>
<NAME>GDF_VARIABLE_VIEWER_TYPE</NAME>
#define GDF_VARIABLE_VIEWER_TYPE        (gdf_variable_viewer_get_type ())
</MACRO>
<MACRO>
<NAME>GDF_VARIABLE_VIEWER</NAME>
#define GDF_VARIABLE_VIEWER(o)          (GTK_CHECK_CAST ((o), GDF_VARIABLE_VIEWER_TYPE, GdfVariableViewer))
</MACRO>
<MACRO>
<NAME>GDF_VARIABLE_VIEWER_CLASS</NAME>
#define GDF_VARIABLE_VIEWER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDF_VARIABLE_VIEWER_TYPE, GdfVariableViewerClass))
</MACRO>
<MACRO>
<NAME>GDF_IS_VARIABLE_VIEWER</NAME>
#define GDF_IS_VARIABLE_VIEWER(o)       (GTK_CHECK_TYPE ((o), GDF_VARIABLE_VIEWER_TYPE))
</MACRO>
<MACRO>
<NAME>GDF_IS_VARIABLE_VIEWER_CLASS</NAME>
#define GDF_IS_VARIABLE_VIEWER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_VARIABLE_VIEWER_TYPE))
</MACRO>
<STRUCT>
<NAME>GdfVariableViewer</NAME>
struct GdfVariableViewer {
	GtkFrame frame;

	GdfVariableViewerPriv *priv;
};
</STRUCT>
<STRUCT>
<NAME>GdfVariableViewerClass</NAME>
struct GdfVariableViewerClass {
	GtkFrameClass parent_class;

    void (*debugger_set) (GdfVariableViewer *vv, GdfDebuggerClient *client);
};
</STRUCT>
<FUNCTION>
<NAME>gdf_variable_viewer_get_type</NAME>
<RETURNS>GtkType  </RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_variable_viewer_new</NAME>
<RETURNS>GdfVariableViewer  *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdf_variable_viewer_view_symbol_set</NAME>
<RETURNS>void  </RETURNS>
GdfVariableViewer *vv,GdfSymbolSetClient *ss
</FUNCTION>
<FUNCTION>
<NAME>gdf_variable_viewer_view_locals</NAME>
<RETURNS>void  </RETURNS>
GdfVariableViewer *vv
</FUNCTION>
<FUNCTION>
<NAME>gdf_variable_viewer_view_symbol</NAME>
<RETURNS>void  </RETURNS>
GdfVariableViewer *vv, GDF_Symbol *sym
</FUNCTION>
<FUNCTION>
<NAME>gdf_variable_viewer_set_debugger</NAME>
<RETURNS>void  </RETURNS>
GdfVariableViewer *vv,GdfDebuggerClient *dbg
</FUNCTION>
<FUNCTION>
<NAME>gdf_variable_viewer_clear</NAME>
<RETURNS>void  </RETURNS>
GdfVariableViewer *vv
</FUNCTION>
