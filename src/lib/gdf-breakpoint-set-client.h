/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __GDF_BREAKPOINT_SET_CLIENT_H__
#define __GDF_BREAKPOINT_SET_CLIENT_H__

#include <bonobo.h>
#include "gnome-debug.h"

#include "gdf-debugger-client.h"

BEGIN_GNOME_DECLS

#define GDF_BREAKPOINT_SET_CLIENT_TYPE		(gdf_breakpoint_set_client_get_type ())
#define GDF_BREAKPOINT_SET_CLIENT(o)		(GTK_CHECK_CAST ((o), GDF_BREAKPOINT_SET_CLIENT_TYPE, GdfBreakpointSetClient))
#define GDF_BREAKPOINT_SET_CLIENT_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), GDF_BREAKPOINT_SET_CLIENT_TYPE, GdfBreakpointSetClientClass))
#define GDF_IS_BREAKPOINT_SET_CLIENT(o)		(GTK_CHECK_TYPE ((o), GDF_BREAKPOINT_SET_CLIENT_TYPE))
#define GDF_IS_BREAKPOINT_SET_CLIENT_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), GDF_BREAKPOINT_SET_CLIENT_TYPE))

typedef struct _GdfBreakpointSetClient        GdfBreakpointSetClient;
typedef struct _GdfBreakpointSetClientPrivate GdfBreakpointSetClientPrivate;
typedef struct _GdfBreakpointSetClientClass   GdfBreakpointSetClientClass;
typedef enum   _GdfBreakpointSetClientResult  GdfBreakpointSetClientResult;

struct _GdfBreakpointSetClient {
    GtkObject parent;
    
    CORBA_Object objref;
    GdfBreakpointSetClientPrivate *priv;
};

struct _GdfBreakpointSetClientClass {
    GtkObjectClass parent_class;

    void (*breakpoint_set) (GdfBreakpointSetClient *, gint bp_num);
    void (*breakpoint_enabled) (GdfBreakpointSetClient *, gint bp_num);
    void (*breakpoint_disabled) (GdfBreakpointSetClient *, gint bp_num);
    void (*breakpoint_deleted) (GdfBreakpointSetClient *, gint bp_num);    
};

enum _GdfBreakpointSetClientResult {
    GDF_BREAKPOINT_SET_CLIENT_OK,
    GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS,
    GDF_BREAKPOINT_SET_CLIENT_DOESNT_EXIST
};

GdfBreakpointSetClient *gdf_breakpoint_set_client_new (void);
GdfBreakpointSetClient *gdf_breakpoint_set_client_new_from_corba (GNOME_Development_BreakpointSet breakpoint_set);
GtkType gdf_breakpoint_set_client_get_type (void);

GdfBreakpointSetClientResult gdf_breakpoint_set_client_get_breakpoints (GdfBreakpointSetClient *bs,
                                                                        GNOME_Development_BreakpointList **ret);
GdfBreakpointSetClientResult gdf_breakpoint_set_client_set_breakpoint (GdfBreakpointSetClient *bs,
                                                                       const char *file_name,
                                                                       long line_num,
                                                                       const char *condition,
                                                                       long *bp_num);
GdfBreakpointSetClientResult  gdf_breakpoint_set_client_set_breakpoint_function (GdfBreakpointSetClient *bs,
                                                                                 const char *file_name,
                                                                                 const char *function_name,
                                                                                 const char *condition,
                                                                                 long *bp_num);
GdfBreakpointSetClientResult gdf_breakpoint_set_client_set_watchpoint (GdfBreakpointSetClient *bs,
                                                                       const char *expr,
                                                                       const char *condition,
                                                                       long *bp_num);
GdfBreakpointSetClientResult gdf_breakpoint_set_client_enable_breakpoint (GdfBreakpointSetClient *bs,
                                                                          long bp_num);
GdfBreakpointSetClientResult gdf_breakpoint_set_client_disable_breakpoint (GdfBreakpointSetClient *bs,
                                                                           long bp_num);
GdfBreakpointSetClientResult gdf_breakpoint_set_client_delete_breakpoint (GdfBreakpointSetClient *bs,
                                                                          long bp_num);

GdfBreakpointSetClientResult gdf_breakpoint_set_client_get_breakpoint_info (GdfBreakpointSetClient *bs,
                                                                            long bp_num,
                                                                            GNOME_Development_Breakpoint **ret);
GdfBreakpointSetClientResult gdf_breakpoint_set_client_set_debugger (GdfBreakpointSetClient *bs,
                                                                     GdfDebuggerClient *dbg);



END_GNOME_DECLS

#endif
