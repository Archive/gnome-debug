/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *                         Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __GDF_DEBUGGER_CLIENT_H__
#define __GDF_DEBUGGER_CLIENT_H__

#include <bonobo.h>
#include "gnome-debug.h"

BEGIN_GNOME_DECLS

#define GDF_DEBUGGER_CLIENT_TYPE		(gdf_debugger_client_get_type ())
#define GDF_DEBUGGER_CLIENT(o)		(GTK_CHECK_CAST ((o), GDF_DEBUGGER_CLIENT_TYPE, GdfDebuggerClient))
#define GDF_DEBUGGER_CLIENT_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), GDF_DEBUGGER_CLIENT_TYPE, GdfDebuggerClientClass))
#define GDF_IS_DEBUGGER_CLIENT(o)		(GTK_CHECK_TYPE ((o), GDF_DEBUGGER_CLIENT_TYPE))
#define GDF_IS_DEBUGGER_CLIENT_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), GDF_DEBUGGER_CLIENT_TYPE))

typedef struct _GdfDebuggerClient        GdfDebuggerClient;
typedef struct _GdfDebuggerClientPrivate GdfDebuggerClientPrivate;
typedef struct _GdfDebuggerClientClass   GdfDebuggerClientClass;

typedef enum {
    GDF_DEBUGGER_CLIENT_OK,
    GDF_DEBUGGER_CLIENT_INVALID_BINARY,
    GDF_DEBUGGER_CLIENT_NO_SYMBOLS,
    GDF_DEBUGGER_CLIENT_DOESNT_EXIST,
    GDF_DEBUGGER_CLIENT_NOT_EXECUTABLE,
    GDF_DEBUGGER_CLIENT_SECURITY_EXCEPTION,
    GDF_DEBUGGER_CLIENT_INVALID_STATE,
    GDF_DEBUGGER_CLIENT_BAD_PARAMS,
    GDF_DEBUGGER_CLIENT_INVALID_COREFILE,
    GDF_DEBUGGER_CLIENT_UNKNOWN_ERROR
} GdfDebuggerClientResult;

GdfDebuggerClient *gdf_debugger_client_new (const char *iid);
struct _GdfDebuggerClient {
    GtkObject parent;
    
    CORBA_Object objref;
    GdfDebuggerClientPrivate *priv;
};

struct _GdfDebuggerClientClass {
    GtkObjectClass parent_class;
    
    void (*unknown_event_pushed) (GdfDebuggerClient *,
                                  const char *, 
                                  CORBA_any *);
    void (*program_loaded) (GdfDebuggerClient *);
    void (*program_unloaded) (GdfDebuggerClient *);
    void (*breakpoint_set) (GdfDebuggerClient *, gint bp_num);
    void (*breakpoint_enabled) (GdfDebuggerClient *, gint bp_num);
    void (*breakpoint_disabled) (GdfDebuggerClient *, gint bp_num);
    void (*breakpoint_deleted) (GdfDebuggerClient *, gint bp_num);
    void (*attached) (GdfDebuggerClient *);
    void (*detached) (GdfDebuggerClient *);
    void (*corefile_loaded) (GdfDebuggerClient *);
    void (*corefile_unloaded) (GdfDebuggerClient *);
    void (*execution_started) (GdfDebuggerClient *);
    void (*execution_running) (GdfDebuggerClient *);
    void (*execution_stopped) (GdfDebuggerClient *);
    void (*execution_killed) (GdfDebuggerClient *, gint signal);
    void (*execution_exited) (GdfDebuggerClient *, gint exit_code);
    void (*execution_source_line) (GdfDebuggerClient *, 
                                   const gchar *filename,
                                   gint source_line);
    void (*symbol_set_changed) (GdfDebuggerClient *, gint set_id);
    void (*symbol_changed) (GdfDebuggerClient *, gint set_id, gint handle);
    void (*symbol_expanded) (GdfDebuggerClient *, gint set_id, gint handle);
    void (*stack_frame_changed) (GdfDebuggerClient *, gint new_id);
    void (*signal_received) (GdfDebuggerClient *, char *sig_id);
    void (*signal_termination) (GdfDebuggerClient *, char *sig_id);
};

GdfDebuggerClient *gdf_debugger_client_new_for_mime_type (const char *type);
GdfDebuggerClient *gdf_debugger_client_new_for_file (const char *file);
GdfDebuggerClient *gdf_debugger_client_new_from_corba (GNOME_Development_Debugger debugger);
GtkType gdf_debugger_client_get_type (void);

GNOME_Development_Debugger_StateFlag gdf_debugger_client_get_state (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_load_binary (GdfDebuggerClient *dbg, 
                                 gchar *file_name);
GdfDebuggerClientResult gdf_debugger_client_unload_binary (GdfDebuggerClient *dbg);

GdfDebuggerClientResult gdf_debugger_client_execute (GdfDebuggerClient *dbg, 
                                                     const char *args);
GdfDebuggerClientResult gdf_debugger_client_attach (GdfDebuggerClient *dbg, 
                                                    int pid);
GdfDebuggerClientResult gdf_debugger_client_detach (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_load_corefile (GdfDebuggerClient *dbg,
                                                           const char* corefile_name);
GdfDebuggerClientResult gdf_debugger_client_unload_corefile (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_get_sources (GdfDebuggerClient *dbg, 
                                                         GNOME_Development_SourceFileList **ret);
GdfDebuggerClientResult gdf_debugger_client_get_absolute_source_path (GdfDebuggerClient *dbg,
                                                                      const char *file_name,
                                                                      char **ret);
GdfDebuggerClientResult gdf_debugger_client_get_breakpoints (GdfDebuggerClient *dbg,
                                                             GNOME_Development_BreakpointList **ret);
GdfDebuggerClientResult gdf_debugger_client_set_breakpoint (GdfDebuggerClient *dbg,
                                                            const char *file_name,
                                                            long line_num,
                                                            const char *condition,
                                                            long *bp_num);
GdfDebuggerClientResult  gdf_debugger_client_set_breakpoint_function (GdfDebuggerClient *dbg,
                                                                      const char *file_name,
                                                                      const char *function_name,
                                                                      const char *condition,
                                                                      long *bp_num);
GdfDebuggerClientResult gdf_debugger_client_set_watchpoint (GdfDebuggerClient *dbg,
                                                            const char *expr,
                                                            const char *condition,
                                                            long *bp_num);
GdfDebuggerClientResult gdf_debugger_client_enable_breakpoint (GdfDebuggerClient *dbg,
                                                               long bp_num);
GdfDebuggerClientResult gdf_debugger_client_disable_breakpoint (GdfDebuggerClient *dbg,
                                                                long bp_num);
GdfDebuggerClientResult gdf_debugger_client_delete_breakpoint (GdfDebuggerClient *dbg,
                                                               long bp_num);

GdfDebuggerClientResult gdf_debugger_client_get_breakpoint_info (GdfDebuggerClient *dbg,
                                                                 long bp_num,
                                                                 GNOME_Development_Breakpoint **ret);
GdfDebuggerClientResult gdf_debugger_client_get_registers (GdfDebuggerClient *dbg,
                                                           gboolean floating_pt,
                                                           GNOME_Development_RegisterList **ret);
GdfDebuggerClientResult gdf_debugger_client_get_register_value (GdfDebuggerClient *dbg, 
                                                                const char *register_name,
                                                                char **val);
GdfDebuggerClientResult gdf_debugger_client_set_register_value (GdfDebuggerClient *dbg,
                                                                const char *register_name,
                                                                const char *register_value);
long gdf_debugger_client_current_frame (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_up_frame (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_down_frame (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_change_frame (GdfDebuggerClient *dbg, 
                                                          long new_id);
GdfDebuggerClientResult gdf_debugger_client_get_frame (GdfDebuggerClient *dbg,
                                                       long id,
                                                       GNOME_Development_StackFrame **ret);
GdfDebuggerClientResult gdf_debugger_client_get_backtrace (GdfDebuggerClient *dbg, 
                                                           GNOME_Development_Stack **ret);
GdfDebuggerClientResult gdf_debugger_client_cont (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_stop (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_restart (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_step_over (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_step_into (GdfDebuggerClient *dbg);
GdfDebuggerClientResult gdf_debugger_client_step_out (GdfDebuggerClient *dbg);

GdfDebuggerClientResult gdf_debugger_client_get_locals (GdfDebuggerClient *dbg,
                                                        GNOME_Development_SymbolSet *ret);
GdfDebuggerClientResult gdf_debugger_client_allocate_symbol_set (GdfDebuggerClient *dbg,
                                                                 GNOME_Development_SymbolSet *ret);

GdfDebuggerClientResult gdf_debugger_client_set_output_tty (GdfDebuggerClient *dbg,
                                                            const char *tty_name);
GdfDebuggerClientResult gdf_debugger_client_set_working_directory (GdfDebuggerClient *dbg,
                                                                   const char *dirname);
                                                    

END_GNOME_DECLS

#endif
