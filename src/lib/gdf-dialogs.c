/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include "gdf-dialogs.h"

#include <gnome.h>
#include <glade/glade.h>
#include <gdl/gdl-tools.h>

#include <liboaf/liboaf.h>

static GladeXML *ui = NULL;

/**
 * gdf_parameters_dialog:
 * 
 * Gets execution parameters from the user.
 * 
 * Return value: A string, allocated with g_malloc, containing the parameters
* entered by the user, or NULL if the user cancelled.
 **/
char *
gdf_parameters_dialog (void)
{
    GtkWidget *dlg;
    GtkWidget *entry;
    GtkWidget *label;
    GtkWidget *hbox;
    int btn;

    label = gtk_label_new (_("Parameters:"));
    entry = gnome_entry_new ("gdf_parameters");

    hbox = gtk_hbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
    
    dlg = gnome_dialog_new (_("Parameters"), GNOME_STOCK_BUTTON_OK,
                            GNOME_STOCK_BUTTON_CANCEL, NULL);
    gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), hbox,
                        TRUE, TRUE, 0);
    gtk_widget_show (label);
    gtk_widget_show (entry);
    gtk_widget_show (hbox);
    
    btn = gnome_dialog_run (GNOME_DIALOG (dlg));
    
    if (btn != 0) {
        gnome_dialog_close (GNOME_DIALOG (dlg));
        return NULL;
    } else {
        char *ret = g_strdup (gtk_editable_get_chars (GTK_EDITABLE (gnome_entry_gtk_entry (GNOME_ENTRY (entry))), 0, -1));
        gnome_dialog_close (GNOME_DIALOG (dlg));
        return ret;
    }
}

GdfDebuggerClientResult 
gdf_attach_dialog (GdfDebuggerClient *dbg)
{
    GtkWidget *dlg;
    GtkWidget *entry;
    GtkWidget *label;
    GtkWidget *hbox;
    GdfDebuggerClientResult res;
    char *errmsg;
    int btn;
    int pid;
    char *pidstr;
    
    label = gtk_label_new (_("pid :")); 
    entry = gnome_entry_new ("gdf_pid");

    hbox = gtk_hbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
    
    dlg = gnome_dialog_new (_("Attach"), GNOME_STOCK_BUTTON_OK, 
                            GNOME_STOCK_BUTTON_CANCEL, NULL);

    gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), hbox, 
                        TRUE, TRUE, 0);

    gtk_widget_show (label);
    gtk_widget_show (entry);
    gtk_widget_show (hbox);

    btn = gnome_dialog_run (GNOME_DIALOG (dlg));
    
    if (btn != 0) {
        gnome_dialog_close (GNOME_DIALOG (dlg));
        return GDF_DEBUGGER_CLIENT_OK;
    }
    
    pidstr = gtk_editable_get_chars (GTK_EDITABLE (gnome_entry_gtk_entry (GNOME_ENTRY (entry))), 0, -1);
    pid = atoi (pidstr);
    res = gdf_debugger_client_attach (dbg, pid);
    
    gnome_dialog_close (GNOME_DIALOG (dlg));

    errmsg = NULL;
    switch (res) {
    case GDF_DEBUGGER_CLIENT_OK :
        break;
    case GDF_DEBUGGER_CLIENT_DOESNT_EXIST :
        errmsg = _("Process doesn't exist.");
        break;
    case GDF_DEBUGGER_CLIENT_SECURITY_EXCEPTION :
        errmsg = _("Access denied.");
        break;
    case GDF_DEBUGGER_CLIENT_INVALID_STATE :
        errmsg = _("Binary must be loaded and not running.");
        break;
    case GDF_DEBUGGER_CLIENT_UNKNOWN_ERROR :
        errmsg = _("An unexpected error occured.");
        break;
    default :
        g_assert_not_reached ();
    }

    if (errmsg) {
        errmsg = 
            g_strdup_printf (_("Could not attach to process: %s"), errmsg);
        dlg = gnome_error_dialog (errmsg);
        gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
        g_free (errmsg);
    }    

    return res;
}

static gint
window_delete_event_cb (GtkWidget *widget, GdkEvent *event, gpointer data)
{
    gtk_widget_hide (widget);

    return TRUE;
}

static void
style_toggled_cb (GtkWidget *btn, gpointer data)
{
    GtkWidget *w = glade_xml_get_widget (ui, "style_function");
    if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w))) {
        w = glade_xml_get_widget (ui, "line_entry");
        gtk_widget_set_sensitive (w, FALSE);
        w = glade_xml_get_widget (ui, "line_label");
        gtk_widget_set_sensitive (w, FALSE);
        w = glade_xml_get_widget (ui, "function_entry");
        gtk_widget_set_sensitive (w, TRUE);
        w = glade_xml_get_widget (ui, "function_label");
        gtk_widget_set_sensitive (w, TRUE);
    } else {
        w = glade_xml_get_widget (ui, "line_entry");
        gtk_widget_set_sensitive (w, TRUE);
        w = glade_xml_get_widget (ui, "line_label");
        gtk_widget_set_sensitive (w, TRUE);
        w = glade_xml_get_widget (ui, "function_entry");
        gtk_widget_set_sensitive (w, FALSE);
        w = glade_xml_get_widget (ui, "function_label");
        gtk_widget_set_sensitive (w, FALSE);
    }   
}

static void
ok_clicked_cb (GtkWidget *btn, gpointer data)
{
    GtkWidget *w;
    char *filename;
    char *condition;
    GdfDebuggerClientResult res;
    GdfDebuggerClient *dbg = NULL;
    GdfBreakpointSetClient *bs = NULL;
    char *type = gtk_object_get_data (GTK_OBJECT (data), "BreakpointDlgType");

    ui = gtk_object_get_data (GTK_OBJECT (data), "GladeXML");

    g_assert (ui != NULL);

    GDL_TRACE ();

    if (!strcmp (type, "Debugger"))
        dbg = data;
    else
        bs = data;

    w = glade_xml_get_widget (ui, "file_entry");
    filename = gtk_editable_get_chars (GTK_EDITABLE (gnome_entry_gtk_entry (GNOME_ENTRY (w))), 0, -1);

    w = glade_xml_get_widget (ui, "condition_entry");
    condition = gtk_editable_get_chars (GTK_EDITABLE (gnome_entry_gtk_entry (GNOME_ENTRY (w))), 0, -1);
    
    w = glade_xml_get_widget (ui, "style_function");
    if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w))) {
        char *function;   
        w = glade_xml_get_widget (ui, "function_entry");
        function = gtk_editable_get_chars (GTK_EDITABLE (gnome_entry_gtk_entry (GNOME_ENTRY (w))), 0, -1);

        if (dbg) {
            res = gdf_debugger_client_set_breakpoint_function (dbg, filename, 
                                                               function, 
                                                               condition, 
                                                               NULL);
        } else {
            res = gdf_breakpoint_set_client_set_breakpoint_function (bs,
                                                                     filename,
                                                                     function,
                                                                     condition,
                                                                     NULL);
        }
    } else {
        int line_num;        

        w = glade_xml_get_widget (ui, "line_entry");
        g_assert (w);
        line_num = atoi (gtk_editable_get_chars (GTK_EDITABLE (gnome_entry_gtk_entry (GNOME_ENTRY (w))), 0, -1));
        
        if (dbg) {
            res = gdf_debugger_client_set_breakpoint (dbg, filename, line_num,
                                                      condition, NULL);
        } else {
            res = gdf_breakpoint_set_client_set_breakpoint (bs, filename, 
                                                            line_num, 
                                                            condition, NULL);
        }
    }
    w = glade_xml_get_widget (ui, "add_breakpoint_dlg");
    gtk_widget_hide (w);

    if (res != GDF_DEBUGGER_CLIENT_OK) {
        GtkWidget *dlg = gnome_error_dialog (_("Could not set breakpoint."));
        gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
    }

    gtk_object_unref (GTK_OBJECT (ui));
}

void
gdf_add_breakpoint_dialog (GdfDebuggerClient *dbg, GdfBreakpointSetClient *bs) 
{
    GtkWidget *dlg;
    GtkWidget *w;

    g_return_if_fail (!(dbg && bs));

    ui = glade_xml_new (GDF_GLADEDIR "/gdf-dialogs.glade", 
                        "add_breakpoint_dlg");
    
    dlg = glade_xml_get_widget (ui, "add_breakpoint_dlg");
    gtk_signal_connect (GTK_OBJECT (dlg), "delete_event",
                        GTK_SIGNAL_FUNC (window_delete_event_cb),
                        NULL);
    
    w = glade_xml_get_widget (ui, "style_function");
    gtk_signal_connect (GTK_OBJECT (w), "toggled", 
                        GTK_SIGNAL_FUNC (style_toggled_cb), (gpointer)NULL);

    w = glade_xml_get_widget (ui, "style_line_number");
    gtk_signal_connect (GTK_OBJECT (w), "toggled", 
                        GTK_SIGNAL_FUNC (style_toggled_cb), (gpointer)NULL);

    w = glade_xml_get_widget (ui, "file_entry");
    gtk_entry_set_text (GTK_ENTRY (gnome_entry_gtk_entry (GNOME_ENTRY (w))), 
                        "");
    w = glade_xml_get_widget (ui, "function_label");
    gtk_widget_set_sensitive (w, FALSE);

    w = glade_xml_get_widget (ui, "function_entry");
    gtk_entry_set_text (GTK_ENTRY (gnome_entry_gtk_entry (GNOME_ENTRY (w))), 
                        "");
    gtk_widget_set_sensitive (w, FALSE);

    w = glade_xml_get_widget (ui, "line_entry");
    gtk_entry_set_text (GTK_ENTRY (gnome_entry_gtk_entry (GNOME_ENTRY (w))), 
                        "");
    w = glade_xml_get_widget (ui, "line_label");
    w = glade_xml_get_widget (ui, "condition_entry");
    gtk_entry_set_text (GTK_ENTRY (gnome_entry_gtk_entry (GNOME_ENTRY (w))), 
                        "");

    w = glade_xml_get_widget (ui, "cancel_btn");
    gtk_signal_connect_object (GTK_OBJECT (w), "clicked", 
                               GTK_SIGNAL_FUNC (gtk_widget_hide), 
                               (gpointer)dlg);
    w = glade_xml_get_widget (ui, "ok_btn");

    gtk_object_set_data (GTK_OBJECT (dbg ? dbg : bs), 
                         "BreakpointDlgType",
                         dbg ? "Debugger" : "BreakpointSet");
    gtk_object_set_data (GTK_OBJECT (dbg ? dbg : bs), 
                         "GladeXML", ui);
    gtk_signal_connect (GTK_OBJECT (w), "clicked",
                        GTK_SIGNAL_FUNC (ok_clicked_cb), 
                        (gpointer)dbg ? dbg : bs);
    
    gtk_widget_show (dlg);
    gdk_window_raise (dlg->window);
}
