/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __GNOME_Development_DIALOGS_H__
#define __GNOME_Development_DIALOGS_H__

#include <gtk/gtkwidget.h>
#include "gdf-debugger-client.h"
#include "gdf-breakpoint-set-client.h"

char *gdf_parameters_dialog (void);
GdfDebuggerClientResult gdf_attach_dialog (GdfDebuggerClient *dbg);
void gdf_add_breakpoint_dialog (GdfDebuggerClient *dbg, 
                                GdfBreakpointSetClient *bs);

#endif
