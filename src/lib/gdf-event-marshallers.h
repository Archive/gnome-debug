/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __GDF_EVENT_MARSHALLERS__
#define __GDF_EVENT_MARSHALLERS__

#include <bonobo.h>
#include "gnome-debug.h"

typedef struct _GdfEvent                  GdfEvent;

/* FIXME: Add more marshallers/allowed types */

typedef enum _GdfEventArgType {
	GDF_EVENT_ARG_NONE,
	GDF_EVENT_ARG_BOOLEAN,
	GDF_EVENT_ARG_LONG,
	GDF_EVENT_ARG_SHORT,
	GDF_EVENT_ARG_STRING,
	GDF_EVENT_ARG_CHAR,
	GDF_EVENT_ARG_DOUBLE,
	GDF_EVENT_ARG_SOURCE_LOCATION,
    GDF_EVENT_ARG_LONG_LIST,
	GDF_EVENT_ARG_ANY
} GdfEventArgType;

struct _GdfEvent {
	char *event_name;
	GdfEventArgType arg_type;

	union {
		gboolean bool_arg;
		glong long_arg;
		gshort short_arg;
		gchar *string_arg;
		gchar char_arg;
		gdouble double_arg;
		GNOME_Development_SourceLocation *loc_arg;
        GList *list_arg;
		CORBA_any *any;
	} argument;
};

CORBA_any *gdf_marshal_event_none ();
CORBA_any *gdf_marshal_event_boolean (gboolean value);
CORBA_any *gdf_marshal_event_long (glong value);
CORBA_any *gdf_marshal_event_short (gshort value);
CORBA_any *gdf_marshal_event_string (const gchar* value);
CORBA_any *gdf_marshal_event_char (gchar value);
CORBA_any *gdf_marshal_event_double (gdouble value);
CORBA_any *gdf_marshal_event_source_location (const gchar *source_path,
											  gint source_line);
CORBA_any *gdf_marshal_event_long_list (GList *list);
GdfEvent *gdf_demarshal_event (const char *event_name, CORBA_any *any);
void gdf_event_destroy (GdfEvent *event);

#endif
