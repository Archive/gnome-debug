/*
 * Translatable strings file generated by Glade.
 * Add this file to your project's POTFILES.in.
 * DO NOT compile it as part of your application.
 */

gchar *s = N_("Add Breakpoint");
gchar *s = N_("Style");
gchar *s = N_("Line Number\n"
              "");
gchar *s = N_("Function\n"
              "");
gchar *s = N_("Line Number:");
gchar *s = N_("Source File:");
gchar *s = N_("Function:");
gchar *s = N_("Condition:");
