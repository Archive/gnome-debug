/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

/* FIXME: Add more marshallers */
#include <config.h>
#include "gdf-event-marshallers.h"

/* Gross macros to get around ORBit incompleteness. Taken from bonobo */
#define CORBA_char__alloc() (CORBA_char *) CORBA_octet_allocbuf (sizeof (CORBA_char))
#define CORBA_short__alloc() (CORBA_short *) CORBA_octet_allocbuf (sizeof (CORBA_short))
#define CORBA_unsigned_short__alloc() (CORBA_unsigned_short *) CORBA_octet_allocbuf (sizeof (CORBA_unsigned_short))
#define CORBA_long__alloc() (CORBA_long *) CORBA_octet_allocbuf (sizeof (CORBA_long))
#define CORBA_unsigned_long__alloc() (CORBA_unsigned_long *) CORBA_octet_allocbuf (sizeof (CORBA_unsigned_long))
#define CORBA_string__alloc() (CORBA_char **)ORBit_alloc(sizeof(gpointer), CORBA_string__free, GUINT_TO_POINTER(1))
#define CORBA_float__alloc() (CORBA_float *) CORBA_octet_allocbuf (sizeof (CORBA_float))
#define CORBA_double__alloc() (CORBA_double *) CORBA_octet_allocbuf (sizeof (CORBA_double))
#define CORBA_boolean__alloc() (CORBA_boolean *) CORBA_octet_allocbuf (sizeof (CORBA_boolean))

static CORBA_any *build_event (CORBA_TypeCode type,
							   gpointer value);

CORBA_any *
gdf_marshal_event_none ()
{
	/* There must be an argument; just use boolean */
	return gdf_marshal_event_boolean (FALSE);
}

CORBA_any *
gdf_marshal_event_boolean (gboolean value)
{
	CORBA_boolean *b;

	b = CORBA_boolean__alloc ();
	*b = (CORBA_boolean)value;
	
	return build_event ((CORBA_TypeCode)TC_boolean, (gpointer)b);
}

CORBA_any *
gdf_marshal_event_long (glong value)
{
	CORBA_long *l;

	l = CORBA_long__alloc ();
	*l = (CORBA_long)value;
	
	return build_event ((CORBA_TypeCode)TC_long, (gpointer)l);
}

CORBA_any *
gdf_marshal_event_short (gshort value)
{
	CORBA_short *s;

	s = CORBA_short__alloc ();
	*s = (CORBA_short)value;
	
	return build_event ((CORBA_TypeCode)TC_short, (gpointer)s);
}


CORBA_any *
gdf_marshal_event_string (const gchar* value)
{
	CORBA_char **str;
	const gchar *string_value;

	if (value == NULL)
		string_value = "";
	else 
		string_value = (const gchar *)value;
	
	str = CORBA_string__alloc ();
	*str = CORBA_string_dup (string_value);

	return build_event ((CORBA_TypeCode)TC_string, (gpointer)str);
}

CORBA_any *
gdf_marshal_event_char (gchar value)
{
	CORBA_char *c;

	c = CORBA_char__alloc ();
	*c = (CORBA_char)value;
	
	return build_event ((CORBA_TypeCode)TC_char, (gpointer)c);
}

CORBA_any *
gdf_marshal_event_double (gdouble value)
{
	CORBA_double *d;

	d = CORBA_double__alloc ();
	*d = (CORBA_double)value;

	return build_event ((CORBA_TypeCode)TC_double, (gpointer)d);
}

CORBA_any *
gdf_marshal_event_source_location (const gchar *source_path,
								   gint source_line)
{
	GNOME_Development_SourceLocation *loc;
	
	g_return_val_if_fail (source_path != NULL, NULL);
	
	loc = GNOME_Development_SourceLocation__alloc ();
	loc->file = CORBA_string_dup (source_path);
	loc->line = source_line;	

	return build_event ((CORBA_TypeCode)TC_GNOME_Development_SourceLocation,
						loc);
}

CORBA_any *
gdf_marshal_event_long_list (GList *list)
{
    GNOME_Development_LongSeq *seq;
    int len;
    int i;
    
    g_return_val_if_fail (list != NULL, NULL);

    len = g_list_length (list);
    seq = GNOME_Development_LongSeq__alloc ();
    seq->_length = len;
    seq->_maximum = len;
    seq->_buffer = (CORBA_long *) CORBA_sequence_CORBA_long_allocbuf (len);
    CORBA_sequence_set_release (seq, TRUE);
    
    for (i = 0; list != NULL; list = list->next, i++) {
        seq->_buffer[i] = GPOINTER_TO_INT (list->data);
    }

    return build_event ((CORBA_TypeCode)TC_GNOME_Development_LongSeq, seq);
}

CORBA_any *
build_event (CORBA_TypeCode type,
			 gpointer value)
{
    CORBA_any *any;

    any = CORBA_any__alloc ();
	any->_type = type;
	any->_value = value;
	CORBA_any_set_release (any, TRUE);
	
	return any;
}

GdfEvent *
gdf_demarshal_event (const char *event_name, CORBA_any *any)
{
	GdfEvent *event;
	g_return_val_if_fail (any != NULL, NULL);
	
	event = g_new0 (GdfEvent, 1);
	event->event_name = g_strdup (event_name);
	
    /* FIXME: Handle arguments that aren't in the supported arg list using
     * the any_arg field */
	if (any->_type->kind == CORBA_tk_boolean) {
		event->arg_type = GDF_EVENT_ARG_BOOLEAN;
		event->argument.bool_arg = *((gboolean*)any->_value);
	} else if (any->_type->kind == CORBA_tk_long) {
		event->arg_type = GDF_EVENT_ARG_LONG;
		event->argument.long_arg = *((glong*)any->_value);
	} else if (any->_type->kind == CORBA_tk_short) {
		event->arg_type = GDF_EVENT_ARG_SHORT;
		event->argument.short_arg = *((gshort*)any->_value);
	} else if (any->_type->kind == CORBA_tk_string) {
		event->arg_type = GDF_EVENT_ARG_STRING;
		event->argument.string_arg = 
			g_strdup (*(gchar **)any->_value);
	} else if (any->_type->kind == CORBA_tk_char) {
		event->arg_type = GDF_EVENT_ARG_CHAR;
		event->argument.char_arg = *((gchar*)any->_value);
	} else if (any->_type->kind == CORBA_tk_double) {
		event->arg_type = GDF_EVENT_ARG_DOUBLE;
		event->argument.double_arg = *((gdouble*)any->_value);
	} else if (any->_type->kind == CORBA_tk_struct) {
		if (strcmp (any->_type->name, 
					"SourceLocation") == 0) {
			event->arg_type = GDF_EVENT_ARG_SOURCE_LOCATION;
			event->argument.loc_arg = g_malloc (sizeof (GNOME_Development_SourceLocation));
			memcpy (event->argument.loc_arg, any->_value, 
					sizeof (GNOME_Development_SourceLocation));
		} else {
			g_error ("FIXME: Handle arguments that aren't a supported type");
		}
    } else if (any->_type->kind == CORBA_tk_alias) {
        if (strcmp (any->_type->name, "LongSeq") == 0) {
            GList *ret = NULL;
            GNOME_Development_LongSeq *seq;
            int i;
            event->arg_type = GDF_EVENT_ARG_LONG_LIST;
            seq = (GNOME_Development_LongSeq*)any->_value;
            for (i = 0; i < seq->_length; i++)
                ret = g_list_prepend (ret, GINT_TO_POINTER (seq->_buffer[i]));
            ret = g_list_reverse (ret);
            event->argument.list_arg = ret;
        } else {
			g_error ("FIXME: Handle arguments that aren't a supported type");
		}
	} else {
		g_error ("FIXME: Handle arguments that aren't a supported type");
	}

	return event;
}

void 
gdf_event_destroy (GdfEvent *event)
{
	if (event->arg_type == GDF_EVENT_ARG_STRING) 
		g_free (event->argument.string_arg);
	
	if (event->arg_type == GDF_EVENT_ARG_SOURCE_LOCATION)
		g_free (event->argument.loc_arg);
		
	g_free (event->event_name);
	g_free (event);
}

	
	




