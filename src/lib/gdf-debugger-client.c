/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *                         Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include "gdf-debugger-client.h"
#include <gdl/gdl-tools.h>
#include <bonobo.h>
#include <bonobo/bonobo-listener.h>

#include <unistd.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime.h>

#include <liboaf/liboaf.h>
#include <libgnome/gnome-util.h>
#include "gdf-event-marshallers.h"

static void debugger_client_class_init (GdfDebuggerClientClass *class);
static void debugger_client_init (GdfDebuggerClient *prog);
static void debugger_client_destroy (GdfDebuggerClient *prog);

static void event_pushed_cb (BonoboListener *event_channel,
                             const char *event_name,
                             CORBA_any *any,
                             CORBA_Environment *ev,
                             GdfDebuggerClient *client);
static GdfDebuggerClientResult exception_to_result (CORBA_Environment *ev);

static GtkObjectClass *parent_class;

struct _GdfDebuggerClientPrivate {
    BonoboListener *listener;

    int cur_frame;
};

enum {
    UNKNOWN_EVENT_PUSHED,
    PROGRAM_LOADED,
    PROGRAM_UNLOADED,
    BREAKPOINT_SET,
    BREAKPOINT_ENABLED,
    BREAKPOINT_DISABLED,
    BREAKPOINT_DELETED,
    ATTACHED,
    DETACHED,
    COREFILE_LOADED,
    COREFILE_UNLOADED,
    EXECUTION_STARTED,
    EXECUTION_RUNNING,
    EXECUTION_STOPPED,
    EXECUTION_KILLED,
    EXECUTION_EXITED,
    EXECUTION_SOURCE_LINE,
    STACK_FRAME_CHANGED,
    SIGNAL_RECEIVED,
    SIGNAL_TERMINATION,
    LAST_SIGNAL
};

static gint debugger_client_signals[LAST_SIGNAL];

static void 
gdf_debugger_client_construct (GdfDebuggerClient *dbg, 
                               CORBA_Object corba_object)
{
    CORBA_Object source;
    CORBA_Environment ev;
    char *cwd;
    
    dbg->objref = corba_object;
    dbg->priv = g_new0 (GdfDebuggerClientPrivate, 1);
    dbg->priv->cur_frame = -1;
    
    CORBA_exception_init (&ev);
 
    dbg->priv->listener = bonobo_listener_new (NULL, NULL);
    gtk_signal_connect (GTK_OBJECT (dbg->priv->listener),
                        "event_notify", 
                        GTK_SIGNAL_FUNC (event_pushed_cb), 
                        dbg);
    
     source = GNOME_Development_Debugger_getEventSource (corba_object,
                                                              &ev);
    
    Bonobo_EventSource_addListener (source,
                                    bonobo_object_corba_objref (BONOBO_OBJECT (dbg->priv->listener)),
                                    &ev);

    CORBA_Object_release (source, &ev);

    /* FIXME: Check env */

    CORBA_exception_free (&ev);

    cwd = g_get_current_dir ();
    gdf_debugger_client_set_working_directory (dbg, cwd);
    g_free (cwd);
}

/**
 * gdf_debugger_client_new_for_mime_type:
 * @type: The mime-type the debugger should be capable of debugging.
 * 
 * Creates a #GdfDebuggerClient object.  This function queries the OAF database
 * to find a suitable backend for debugging the given mime-type.  The first
 * backend found will be activated.  If a suitable backend cannot be found, 
 * the function will return NULL.
 *
 * Return value: A newly created #GdfDebuggerClient object.
 **/
GdfDebuggerClient *
gdf_debugger_client_new_for_mime_type (const char *type)
{
    GdfDebuggerClient *ret = NULL;
    CORBA_Environment ev;
    char *query;
    OAF_ServerInfoList *oaf_result;
    
    CORBA_exception_init (&ev);
    
    query = g_strdup_printf ("repo_ids.has ('IDL:GDF/Debugger:1.0') AND supported_mime_types.has ('%s')", type ? type : "unknown");
    
    oaf_result = oaf_query (query, NULL, &ev);

    /* FIXME: Deal with the case where a mime type is handled by more than
     * one backend */
    if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL && oaf_result->_length >= 1) {
        ret = gdf_debugger_client_new (oaf_result->_buffer[0].iid);
    }

    if (oaf_result != NULL) {
        CORBA_free (oaf_result);
    }
    g_free (query);
    return ret;
}

GdfDebuggerClient *
gdf_debugger_client_new_for_file (const char *filename)
{
    GnomeVFSURI *uri;
    const char *mime_type;
    char *path;
    GdfDebuggerClient *ret;

    if (!gnome_vfs_initialized ()) {
        gnome_vfs_init ();
    }

    if (g_path_is_absolute (filename)) {
        path = g_strdup (filename);
    } else {
        char *dir = g_get_current_dir ();
        path = g_strdup_printf ("%s%c%s", dir, G_DIR_SEPARATOR, filename);
        g_free (dir);
    }
    
    uri = gnome_vfs_uri_new (path);
    mime_type = gnome_vfs_get_mime_type (uri);
    gnome_vfs_uri_unref (uri);

    ret = gdf_debugger_client_new_for_mime_type (mime_type);

    g_free (path);

    return ret;
}

/**
 * gdf_debugger_client_new:
 * @iid: OAF IID of the backend to activate.
 *
 * Creates a #GdfDebuggerClient object.  This object activates a new 
 * GDF::Debugger server with the given OAF IID.
 *
 * Returns: A newly-created #GdfDebuggerClient object.
 */
GdfDebuggerClient *
gdf_debugger_client_new (const char *iid)
{
    CORBA_Object corba_object;
    GdfDebuggerClient *ret = NULL;
    CORBA_Environment ev;
    
    CORBA_exception_init (&ev);

    corba_object = oaf_activate_from_id ((const OAF_ActivationID)iid,
#if 1
                                         0, 
#else
                                         OAF_FLAG_PRIVATE,
#endif
                                         NULL, &ev);    
    
    if (CORBA_Object_is_nil (corba_object, &ev)) {
        CORBA_exception_free (&ev);
        return NULL;
    }
    CORBA_exception_free (&ev);
    
    ret = gtk_type_new (gdf_debugger_client_get_type ());
 
    gdf_debugger_client_construct (ret, corba_object);

    gtk_object_ref (GTK_OBJECT (ret));
    gtk_object_sink (GTK_OBJECT (ret));

    return ret;
}


/**
 * gdf_debugger_client_new_from_corba:
 * @debugger: A #GDF::Debugger CORBA object.
 * 
 * Creates a GdfDebuggerClient object.  This object will be used to manipulate
 * the backend provided by the @debugger argument.
 * 
 * The @debugger object will not be ref'ed.  The caller must ref the object
 * before passing in the object.
 * 
 * Return value: A newly-created GdfDebuggerClient object.
 **/
GdfDebuggerClient *
gdf_debugger_client_new_from_corba (GNOME_Development_Debugger debugger) 
{
    GdfDebuggerClient *ret;
    CORBA_Environment ev;

    g_return_val_if_fail (debugger != CORBA_OBJECT_NIL, NULL);

    ret = gtk_type_new (gdf_debugger_client_get_type ());
    gdf_debugger_client_construct (ret, debugger);
   
    CORBA_exception_init (&ev);

    Bonobo_Unknown_ref (debugger, &ev);

    /* FIXME: Check env */
    
    CORBA_exception_free (&ev);

    gtk_object_ref (GTK_OBJECT (ret));
    gtk_object_sink (GTK_OBJECT (ret));

    return ret;
}

GtkType
gdf_debugger_client_get_type (void)
{
    static GtkType type = 0;
    
    if (!type) {
        GtkTypeInfo info = {
            "GdfDebuggerClient",
            sizeof (GdfDebuggerClient),
            sizeof (GdfDebuggerClientClass),
            (GtkClassInitFunc) debugger_client_class_init,
            (GtkObjectInitFunc) debugger_client_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
	
        type = gtk_type_unique (gtk_object_get_type (), &info);
    }
    
    return type;
}

/**
 * gdf_debugger_client_get_state:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Gets the current state of the debugger.  The state is a bitfield with 
 * the following values defined:
 *         
 * %GNOME_Development_Debugger_BINARY_LOADED - Whether a binary has been loaded into
 *     the debugger.
 * 
 * %GNOME_Development_Debugger_TARGET_EXISTS - Whether a process that can be run,
 *     stepped through, etc. exists.
 *
 * %GNOME_Development_Debugger_TARGET_EXECUTING - Whether the target is in the 
 *     process of executing code.  %GNOME_Development_Debugger_TARGET_EXISTS must be  
 *     set for this flag to be set.
 *
 * %GNOME_Development_Debugger_TARGET_ATTACHED - Whether the debugger is attached to
 *     an external process.  %GNOME_Development_Debugger_TARGET_EXISTS must be set for
 *     this flag to be set.
 *
 * %GNOME_Development_Debugger_COREFILE - Whether the debugger has a corefile loaded.
 *     %GNOME_Development_Debugger_COREFILE and GNOME_Development_Debugger_TARGET_EXISTS are mutually
 *     exclusive.
 * 
 * Return value: A #GNOME_Development_Debugger_StateFlag value.
 **/
GNOME_Development_Debugger_StateFlag
gdf_debugger_client_get_state (GdfDebuggerClient *dbg)
{
    GNOME_Development_Debugger_StateFlag ret;
    CORBA_Environment ev;

    g_return_val_if_fail (dbg != NULL, 0);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 0);

    CORBA_exception_init (&ev);
    
    ret = GNOME_Development_Debugger_getState (dbg->objref, &ev);
    
    /* FIXME: Check env */

    CORBA_exception_free (&ev);   
    
    return ret;
}

/**
 * gdf_debugger_client_load_binary:
 * @dbg: The G#dfDebuggerClient object.
 * @file_name: The name of the binary to laod into the debugger.
 * 
 * Loads a binary file into the debugger.  If this function call is sucessful,
 * the %GNOME_Development_Debugger_BINARY_LOADED state flag will be set.
 *
 * Return value: A #GdfDebuggerClientResult indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_load_binary (GdfDebuggerClient *dbg, 
                                 char *file_name)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    char *path;

    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS); 
    g_return_val_if_fail (file_name != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    

    if (g_path_is_absolute (file_name)) {
        path = g_strdup (file_name);
    } else {
        char *dir = g_get_current_dir ();
        path = g_strdup_printf ("%s%c%s", dir, G_DIR_SEPARATOR, file_name);
        g_free (dir);
    }

    GNOME_Development_Debugger_loadBinary (dbg->objref,
                             path,
                             &ev);
    g_free (path);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_unload_binary:
 * @dbg: The $GdfDebuggerClient object.
 * 
 * Unloads the binary file in the debugger.  The %GNOME_Development_Debugger_BINARY_LOADED
 * flag must be set for this operation to succeed, and will be unset on 
 * success.
 * 
 * Return value: A $GdfDebuggerClientResult indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult 
gdf_debugger_client_unload_binary (GdfDebuggerClient *dbg)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    GNOME_Development_Debugger_unloadBinary (dbg->objref,
                                 &ev);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_get_sources:
 * @dbg: The $GdfDebuggerClient object.
 * @ret: An out parameter, through which a #GNOME_Development_SourceFileList will be
 *       returned.  The list will be NULL if the operation is unsuccessful.
 *       If successful it must be freed by the client using CORBA_free().
 * 
 * Gets a list of source files from the debugger.  
 * The %GNOME_Development_Debugger_BINARY_LOADED flag must be set for this operation to 
 * succeed.
 * 
 * Return value: A #GdfDebuggerClientResult indicating the success of the 
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_get_sources (GdfDebuggerClient *dbg, 
                                 GNOME_Development_SourceFileList **ret)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    *ret = GNOME_Development_Debugger_getSources (dbg->objref,
                                     &ev);
    
    res = exception_to_result (&ev);

    if (res != GDF_DEBUGGER_CLIENT_OK) {
        *ret = NULL;
    }
    
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_get_absolute_source_path:
 * @dbg: The #GdfDebuggerClient object.
 * @file_name: The file name to be made absolute.
 * @ret: An out parameter, through which the absolute source path will be 
 *       returned.  It will be NULL if the operation fails.  If successful 
 *       the path must be freed by the caller using g_free().
 * 
 * Converts a filename into an absolute source path, if possible locating the
 * source file from which the @file_name was compiled.
 * 
 * Return value: A #GdfDebuggerClientResult indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_get_absolute_source_path (GdfDebuggerClient *dbg,
                                              const char *file_name,
                                              char **ret)
{
    CORBA_char *corba_ret;
    CORBA_Environment ev;
    GdfDebuggerClientResult res;

    g_return_val_if_fail (file_name != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);

    corba_ret = GNOME_Development_Debugger_getAbsoluteSourcePath (dbg->objref,
                                                    file_name,
                                                    &ev);


    res = exception_to_result (&ev);
    
    if (res == GDF_DEBUGGER_CLIENT_OK) {
        /* duplicate the string so the user can g_free() */
        *ret = g_strdup (corba_ret);
    } else {
        *ret = NULL;
    }
    
    CORBA_free (corba_ret);
   
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_execute:
 * @dbg: The #GdfDebuggerClient object.
 * @args: The arguments to pass on the command line.
 * 
 * Begins execution of the program, passing @args on the command line.  This 
 * function will return immediately, and signals will be used to communicate 
 * state changes in the executing program.
 *
 * This function requires the %GNOME_Development_Debugger_BINARY_LOADED flag to be set,
 * and for the 5GNOME_Development_Debugger_TARGET_EXISTS and %GNOME_Development_Debugger_COREFILE flags
 * to be cleared.  This function will set the %GNOME_Development_Debugger_TARGET_EXISTS and
 * %GNOME_Development_Debugger_TARGET_EXECUTING flags.
 * 
 * Return value: A #GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_execute (GdfDebuggerClient *dbg,
                             const char *args)
{
    CORBA_Environment ev;    
    GdfDebuggerClientResult res;

    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);
    
    GNOME_Development_Debugger_execute (dbg->objref, args ? args : "", &ev);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_attach:
 * @dbg: The #GdfDebuggerClient object.
 * @pid: The process id to attach to.
 * 
 * Attaches the debugger to an existing process.  This function requires the
 * %GNOME_Development_Debugger_BINARY_LOADED flag to be set, and the 
 * %GNOME_Development_Debugger_TARGET_EXISTS and %GNOME_Development_Debugger_COREFILE flags to be cleared.
 * If successful, this function will set the %GNOME_Development_Debugger_TARGET_EXISTS and 
 * %GNOME_Development_Debugger_ATTACHED flags.
 * 
 * Return value: A #GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_attach (GdfDebuggerClient *dbg, int pid)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    GNOME_Development_Debugger_attach (dbg->objref, pid, &ev);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_detach:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Detaches from an external process.  This functino requires the 
 * %GNOME_Development_Debugger_ATTACHED flag to be set.  If successful, the 
 * %GNOME_Development_Debugger_ATTACHED and %GNOME_Development_Debugger_TARGET_EXISTS flags will be 
 * cleared.
 * 
 * Return value: A #GdfDebuggerClientResult value indicating the success of 
 *               the operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_detach (GdfDebuggerClient *dbg)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    GNOME_Development_Debugger_detach (dbg->objref, &ev);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_load_corefile:
 * @dbg: The #GdfDebuggerClient object.
 * @corefile_name: Filename of the corefile to load.
 * 
 * Loads a corefile into the debugger.  This function requires the 
 * %GNOME_Development_Debugger_BINARY_LOADED flag to be set, and the 
 * %GNOME_Development_Debugger_TARGET_EXISTS and %GNOME_Development_Debugger_COREFILE flags to be cleared.
 * Upon success, the %GNOME_Development_Debugger_COREFILE flag will be set.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_load_corefile (GdfDebuggerClient *dbg,
                                   const char *corefile_name)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    char *path = NULL;

    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (corefile_name != NULL, 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    if (g_path_is_absolute (corefile_name)) {
        path = g_strdup (corefile_name);
    } else {
        char *dir = g_get_current_dir ();
        path = g_concat_dir_and_file (dir, corefile_name);
        g_free (dir);
    }
    CORBA_exception_init (&ev);
    
    GNOME_Development_Debugger_loadCorefile (dbg->objref,
                               (CORBA_char*)path,
                               &ev);
    
    g_free (path);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_unload_corefile:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Unloads a corefile from the the debugger.  This function requires the
 * %GNOME_Development_Debugger_COREFILE flag to be set.  
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult 
gdf_debugger_client_unload_corefile (GdfDebuggerClient *dbg)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;

    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);

    GNOME_Development_Debugger_unloadCorefile (dbg->objref, &ev);
    
    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_get_breakpoints:
 * @dbg: The #GdfDebuggerClient object.
 * @ret: An out parameter through which a GNOME_Development_BreakpointList containing all 
 *       the breakpoints set will be returned.  If the operation is 
 *       unsuccessful this value will be NULL.  If successful, it must be 
 *       freed by the caller using CORBA_free().
 * 
 * Gets a list of all the breakpoints set in the debugger.  This function
 * requires the %GNOME_Development_Debugger_BINARY_LOADED flag to be set. 
 *
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_get_breakpoints (GdfDebuggerClient *dbg,
                                     GNOME_Development_BreakpointList **ret)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);
    *ret = GNOME_Development_Debugger_getBreakpoints (dbg->objref, &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_DEBUGGER_CLIENT_OK) {
        *ret = NULL;
    }

    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_set_breakpoint:
 * @dbg: The #GdfDebuggerClient object.
 * @file_name: The file to set the breakpoint in.  The path should not be
 *             absolute, the debugging information will be used to find
 *             the correct file.
 * @line_num: The line number to set the breakpoint on.
 * @condition: An expression in the target language that will be evaludated 
 *             by the debugger when the location is reached.  Execution will 
 *             only stop if the expression evaluates true.  NULL indicates an
 *             unconditional breakpoint.
 * @bp_num: An out parameter through which the breakpoint number will be 
 *          returned.  Will be -1 if the operation was unsuccessful.
 * 
 * Sets a breakpoint at the given location.  Execution will stop when
 * the location is reached, assuming @condition evaluates to %TRUE.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_set_breakpoint (GdfDebuggerClient *dbg,
                                    const char *file_name,
                                    long line_num,
                                    const char *condition,
                                    long *bp_num)
{
    CORBA_Environment ev;
    long num;
    GdfDebuggerClientResult res;

    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (file_name != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    num = 
        GNOME_Development_Debugger_setBreakpoint (dbg->objref,
                                    (CORBA_char*)(file_name ? file_name :""),
                                    line_num,
                                    (CORBA_char*)(condition ? condition :""),
                                    &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_DEBUGGER_CLIENT_OK) {
        num = -1;
    }

    if (bp_num) {
        *bp_num = num;
    }

    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_set_breakpoint_function:
 * @dbg: 
 * @file_name: The file to set the breakpoint in.  The path should not be
 *             absolute, the debugging information will be used to find the
 *             correct file.  This can be NULL, in which case only the function
 *             name will be used.
 * @function_name: The function to set the breakpoint on.
 * @condition: An expression in the target language that will be evaludated 
 *             by the debugger when the location is reached.  Execution will 
 *             only stop if the expression evaluates true. NULL indicates an
 *             unconditional breakpoint.
 * @bp_num: An out parameter through which the breakpoint number will be 
 *          returned.  Will be -1 if the operation was unsuccessful.
 * 
 * Sets a breakpoint on a given function.  Execution will stop when execution
 * enters the given function, assuming @condition evaluates to %TRUE.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_set_breakpoint_function (GdfDebuggerClient *dbg,
                                             const char *file_name,
                                             const char *function_name,
                                             const char *condition,
                                             long *bp_num)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    long num;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (function_name != NULL,
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    num = 
        GNOME_Development_Debugger_setBreakpointFunction 
        (dbg->objref,
         (CORBA_char*)(file_name ? file_name : ""),
         (CORBA_char*)(function_name ? function_name : ""),
         (CORBA_char*)(condition ? condition : ""),
         &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_DEBUGGER_CLIENT_OK) {
        num = -1;
    }

    if (bp_num) {
        *bp_num = num;
    }
    
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_set_watchpoint:
 * @dbg: The #GdfDebuggerClient object.
 * @expr: The expression to evaluate.
 * @condition: An expression in the target language that will be evaludated 
 *             by the debugger when the location is reached.  Execution will 
 *             only stop if the expression evaluates true. NULL indicates an
 *             unconditional breakpoint.
 * @bp_num: An out parameter through which the breakpoint number will be 
 *          returned.  Will be -1 if the operation was unsuccessful.
 * 
 * Sets a watchpoint (value-based breakpoint) on the given expression.  
 * Execution will stop when the value of the expression changes, assuming 
 * @condition evaluates to %TRUE.  The watchpoint can be manipulated using
 * the breakpoint manipulation functions.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_set_watchpoint (GdfDebuggerClient *dbg,
                                    const char *expr,
                                    const char *condition,
                                    long *bp_num)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    long num;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (expr != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    num = GNOME_Development_Debugger_setWatchpoint (dbg->objref, expr, 
                                       condition ? condition : "",
                                       &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_DEBUGGER_CLIENT_OK) {
        num = -1;
    }

    if (bp_num) {
        *bp_num = num;
    }
    
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_enable_breakpoint:
 * @dbg: The #GdfDebuggerClient object.
 * @bp_num: The breakpoint to enable.
 * 
 * Enables a breakpoint.  
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_enable_breakpoint (GdfDebuggerClient *dbg,
                                       long bp_num)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);
    
    GNOME_Development_Debugger_enableBreakpoint (dbg->objref,
                                     bp_num,
                                     &ev);

    /* FIXMR: Check env */
    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_disable_breakpoint:
 * @dbg: The #GdfDebuggerClient object.
 * @bp_num: The breakpoint to disable.
 * 
 * Disables a breakpoint.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_disable_breakpoint (GdfDebuggerClient *dbg,
                                        long bp_num)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    GNOME_Development_Debugger_disableBreakpoint (dbg->objref,
                                    bp_num,
                                    &ev);
    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_delete_breakpoint:
 * @dbg: The #GdfDebuggerClient object.
 * @bp_num: The breakpoint to delete.
 * 
 * Deletes a breakpoint.  When this call returns, @bp_num is no longer a valid
 * breakpoint identifier.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_delete_breakpoint (GdfDebuggerClient *dbg,
                                       long bp_num)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    GNOME_Development_Debugger_deleteBreakpoint (dbg->objref,
                                   bp_num,
                                   &ev);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_get_breakpoint_info:
 * @dbg: The #GdfDebuggerClient object.
 * @bp_num: The breakpoint to get information about.
 * @ret: An out parameter through which a GNOME_Development_Breakpoint structure will be
 *       returned.  This value will be NULL if the operation failed.  If
 *       successful the caller must free the structure using CORBA_free().
 * 
 * Retrieves information about a given breakpoint.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_get_breakpoint_info (GdfDebuggerClient *dbg,
                                         long bp_num,
                                         GNOME_Development_Breakpoint **ret)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    *ret = GNOME_Development_Debugger_getBreakpointInfo (dbg->objref,
                                           (CORBA_long)bp_num,
                                           &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_DEBUGGER_CLIENT_OK) {
        *ret = NULL;
    }

    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_get_registers:
 * @dbg: The #GdfDebuggerClient object.
 * @floating_pt: Whether floating point registers should be returned.
 * @ret: An out parameter through which a GNOME_Development_RegisterList value will be
 *       returned.  If the operation is unsuccessful this value will be NULL.
 *       If successful, the value must be freed using CORBA_free().
 * 
 * Gets the values of the registers.  This is returned as a GNOME_Development_RegisterList,
 * which is a CORBA sequence of GNOME_Development_Register values.  A GNOME_Development_Register is a
 * structure containing two strings, the register name and the register value.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_get_registers (GdfDebuggerClient *dbg,
                                   gboolean floating_pt,
                                   GNOME_Development_RegisterList **ret)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);
    *ret = GNOME_Development_Debugger_getRegisters (dbg->objref,
                                      floating_pt,
                                      &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_DEBUGGER_CLIENT_OK) {
        *ret = NULL;
    }

    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_get_register_value:
 * @dbg: The #GdfDebuggerClient object.
 * @register_name: Name of the register whose value should be returned.
 * @ret: An out parameter through which a string containing the value as
 *       reported by the backend is returned.  This will be NULL on failure.
 * 
 * Gets the value of a register.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_get_register_value (GdfDebuggerClient *dbg, 
                                        const char *register_name,
                                        char **ret)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    char *val = NULL;
  
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (register_name != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);   
  
    CORBA_exception_init (&ev);
    val = GNOME_Development_Debugger_getRegisterValue (dbg->objref, register_name, &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_DEBUGGER_CLIENT_OK) {
        *ret = NULL;
    } else {
        *ret = g_strdup (val);
        CORBA_free (val);
    }
    
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_set_register_value:
 * @dbg: The #GdfDebuggerClient object.
 * @register_name: Name of the register whose value should be changed.
 * @register_value: The value to which the register should be set.
 * 
 * Sets the value of a register.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_set_register_value (GdfDebuggerClient *dbg,
                                        const char *register_name,
                                        const char *register_value)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (register_name != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (register_value != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);   

    CORBA_exception_init (&ev);

    GNOME_Development_Debugger_setRegisterValue (dbg->objref, register_name, 
                                     register_value, &ev);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_current_frame:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Gets the number of the currently selected stack frame.
 * 
 * Return value: The number of the currently selected stack frame, or -1 if 
 *               there is no stack.
 **/
long
gdf_debugger_client_current_frame (GdfDebuggerClient *dbg)
{
    g_return_val_if_fail (dbg != NULL, -1);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), -1);

    return dbg->priv->cur_frame;
}

/**
 * gdf_debugger_client_up_frame:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Selects the stack frame above the currently selected frame.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_up_frame (GdfDebuggerClient *dbg)
{
    return gdf_debugger_client_change_frame (dbg, dbg->priv->cur_frame + 1);
}

/**
 * gdf_debugger_client_down_frame:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Selects the stack frame below the currently selected frame.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.
 **/
GdfDebuggerClientResult
gdf_debugger_client_down_frame (GdfDebuggerClient *dbg)
{
    return gdf_debugger_client_change_frame (dbg, dbg->priv->cur_frame - 1);
}

/**
 * gdf_debugger_client_change_frame:
 * @dbg: The #GdfDebuggerClient object.
 * @new_id: The frame to select.
 * 
 * Changes the currently selected stack frame.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation. 
 **/
GdfDebuggerClientResult
gdf_debugger_client_change_frame (GdfDebuggerClient *dbg, 
                                  long new_id)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);
    GNOME_Development_Debugger_changeFrame (dbg->objref, new_id, &ev);
    
    res = exception_to_result (&ev);

    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_get_frame:
 * @dbg: The #GdfDebuggerClient object.
 * @id: The stack frame to retrieve.
 * @ret: A GNOME_Development_StackFrame value describing the stack frame indicated 
 *       by @frame.  This value will be NULL if the operation is unsuccessful,
 *       and must be freed by CORBA_free() if successful.
 * 
 * Retrieves information about a given stack frame.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_get_frame (GdfDebuggerClient *dbg,
                               long id,
                               GNOME_Development_StackFrame **ret)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);
    *ret = GNOME_Development_Debugger_getFrame (dbg->objref,
                                  id,
                                  &ev);

    res = exception_to_result (&ev);
    if (res != GDF_DEBUGGER_CLIENT_OK) {
        *ret = NULL;
    }
  
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_get_backtrace:
 * @dbg: The #GdfDebuggerClient object.
 * @ret: A GNOME_Development_Stack value containing a sequnce of stack frames in the 
 *       currently executing program.  This value will be NULL if the 
 *       operation is unsuccessful, and must be freed by CORBA_free() if 
 *       successful.
 * 
 * Retrieves the stack as a CORBA sequence of GNOME_Development_StackFrame values.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_get_backtrace (GdfDebuggerClient *dbg,
                                   GNOME_Development_Stack **ret)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    *ret = GNOME_Development_Debugger_getBacktrace (dbg->objref, &ev);

    res = exception_to_result (&ev);
    if (res != GDF_DEBUGGER_CLIENT_OK) {
        *ret = NULL;
    }
    
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_cont:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Continue execution of a stopped program.  This operation requires the 
 * %GNOME_Development_Debugger_TARGET_EXISTS flag to be set, and the 
 * %GNOME_Development_Debugger_TARGET_EXECUTING flag to be cleared.  The 
 * %GNOME_Development_Debugger_TARGET_EXECUTING flag will be set upon returning from the 
 * function.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_cont (GdfDebuggerClient *dbg)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    GNOME_Development_Debugger_cont (dbg->objref,
                       &ev);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_stop:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Ends execution of a program.  This operation requires the 
 * %GNOME_Development_Debugger_TARGET_EXISTS flag to be set, and the 
 * %GNOME_Development_Debugger_TARGET_EXECUTING flag to be cleared.  The 
 * %GNOME_Development_Debugger_TARGET_EXISTS flag will be cleared up returning from the 
 * function.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_stop (GdfDebuggerClient *dbg)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    GNOME_Development_Debugger_stop (dbg->objref,
                        &ev);
    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_restart:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Restarts execution of the program. This operation requires the 
 * %GNOME_Development_Debugger_TARGET_EXISTS flag to be set, and the 
 * %GNOME_Development_Debugger_TARGET_EXECUTING flag to be cleared. The flags will remain
 * unchanged upon return.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_restart (GdfDebuggerClient *dbg)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    GNOME_Development_Debugger_restart (dbg->objref,
                           &ev);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_step_over:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Executes a single source line, not entering any function calls. 
 * Restarts execution of the program. This operation requires the 
 * %GNOME_Development_Debugger_TARGET_EXISTS flag to be set, and the 
 * %GNOME_Development_Debugger_TARGET_EXECUTING flag to be cleared. The 
 * %GNOME_Development_Debugger_TARGET_EXECUTING flag will be set upon return.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_step_over (GdfDebuggerClient *dbg)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    GNOME_Development_Debugger_stepOver (dbg->objref,
                             &ev);
 
    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_step_into:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Executes a single source line, entering any function calls. 
 * Restarts execution of the program. This operation requires the 
 * %GNOME_Development_Debugger_TARGET_EXISTS flag to be set, and the 
 * %GNOME_Development_Debugger_TARGET_EXECUTING flag to be cleared. The 
 * %GNOME_Development_Debugger_TARGET_EXECUTING flag will be set upon return. 
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_step_into (GdfDebuggerClient *dbg)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    GNOME_Development_Debugger_stepInto (dbg->objref,
                            &ev);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_step_out:
 * @dbg: The #GdfDebuggerClient object.
 * 
 * Executes the program until it returns from the current stack frame.
 * This operation requires the  %GNOME_Development_Debugger_TARGET_EXISTS flag to be set, 
 * and the %GNOME_Development_Debugger_TARGET_EXECUTING flag to be cleared. The 
 * %GNOME_Development_Debugger_TARGET_EXECUTING flag will be set upon return. 

 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_step_out (GdfDebuggerClient *dbg)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
   
    CORBA_exception_init (&ev);
    GNOME_Development_Debugger_stepOut (dbg->objref,
                           &ev);
    
    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_get_locals:
 * @dbg: The #GdfDebuggerClient object.
 * @ret: An out parameter through which a GdfSymbolSetClient will be returned.
 *       This value will be NULL if the operation is unsuccessful, and the
 *       object must be destroyed with gtk_object_unref() if successful.
 * 
 * Retrieves a #GdfSymbolSetClient object representing the local variables.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_get_locals (GdfDebuggerClient *dbg,
                                GNOME_Development_SymbolSet *ret)
{
    CORBA_Environment ev;
    GNOME_Development_SymbolSet set;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
   
    CORBA_exception_init (&ev);
    set = GNOME_Development_Debugger_getLocals (dbg->objref, &ev);

    res = exception_to_result (&ev);
    if (res == GDF_DEBUGGER_CLIENT_OK) {
        *ret = set;
    } else {
        CORBA_Object_release (set, &ev);
        *ret = CORBA_OBJECT_NIL;
    }

    CORBA_exception_free (&ev);
    return res;
}   

/**
 * gdf_debugger_client_allocate_symbol_set:
 * @dbg: The #GdfDebuggerClient object.
 * @ret: An out parameter through which a GdfSymbolSetClient will be returned.
 *       This value will be NULL if the operation is unsuccessful, and the
 *       object must be destroyed with gtk_object_unref() if successful. 
 * 
 * Retrieves a GdfSymbolSetClient object representing a newly allocated 
 * symbol set.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_allocate_symbol_set (GdfDebuggerClient *dbg,
                                         GNOME_Development_SymbolSet *ret)
{    
    CORBA_Environment ev;
    GNOME_Development_SymbolSet set;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
   
    CORBA_exception_init (&ev);
    set = GNOME_Development_Debugger_allocateSymbolSet (dbg->objref, &ev);

    res = exception_to_result (&ev);
    if (res == GDF_DEBUGGER_CLIENT_OK) {
        *ret = set;
    } else {
        CORBA_Object_release (set, &ev);
        *ret = CORBA_OBJECT_NIL
;
    }

    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_debugger_client_set_output_tty:
 * @dbg: The #GdfDebuggerClient object.
 * @tty_name: The terminal device that should be used for program interaction.
 * 
 * Sets the terminal device used for program interaction to @tty_name.
 * 
 * Return value: A GdfDebuggerClientResult value indicating the success of the
 *               operation.  
 **/
GdfDebuggerClientResult
gdf_debugger_client_set_output_tty (GdfDebuggerClient *dbg,
                                    const char *tty_name)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;
    
    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (tty_name != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg), 
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);

    GNOME_Development_Debugger_setOutputTTY (dbg->objref, (CORBA_char*)tty_name, &ev);
    
    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

GdfDebuggerClientResult 
gdf_debugger_client_set_working_directory (GdfDebuggerClient *dbg,
                                           const char *dirname)
{
    CORBA_Environment ev;
    GdfDebuggerClientResult res;

    g_return_val_if_fail (dbg != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (dirname != NULL, GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_DEBUGGER_CLIENT_BAD_PARAMS);
    CORBA_exception_init (&ev);
    GNOME_Development_Debugger_setWorkingDirectory (dbg->objref, (CORBA_char *)dirname,
                                        &ev);
    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}


/* private routines */
static void
debugger_client_class_init (GdfDebuggerClientClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *)klass;
    
    g_return_if_fail (klass != NULL);
    g_return_if_fail (GDF_IS_DEBUGGER_CLIENT_CLASS (klass));
    
    parent_class = gtk_type_class (gtk_object_get_type ());
    
    debugger_client_signals [UNKNOWN_EVENT_PUSHED] = 
        gtk_signal_new ("unknown_event_pushed",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           unknown_event_pushed),
                        gtk_marshal_NONE__POINTER_POINTER,
                        GTK_TYPE_NONE, 2,
                        GTK_TYPE_STRING, GTK_TYPE_POINTER);
    debugger_client_signals [PROGRAM_LOADED] = 
        gtk_signal_new ("program_loaded",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           program_loaded),
                        gtk_marshal_NONE__NONE,
                        GTK_TYPE_NONE, 0);
    debugger_client_signals [PROGRAM_UNLOADED] = 
        gtk_signal_new ("program_unloaded",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           program_unloaded),
                        gtk_marshal_NONE__NONE,
                        GTK_TYPE_NONE, 0);
    debugger_client_signals [BREAKPOINT_SET] = 
        gtk_signal_new ("breakpoint_set",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           breakpoint_set),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);
    debugger_client_signals [BREAKPOINT_ENABLED] = 
        gtk_signal_new ("breakpoint_enabled",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           breakpoint_enabled),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);
    debugger_client_signals [BREAKPOINT_DISABLED] = 
        gtk_signal_new ("breakpoint_disabled",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           breakpoint_disabled),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);
    debugger_client_signals [BREAKPOINT_DELETED] = 
        gtk_signal_new ("breakpoint_deleted",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           breakpoint_deleted),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);
    debugger_client_signals [ATTACHED] = 
        gtk_signal_new ("attached",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           attached),
                        gtk_marshal_NONE__NONE,
                        GTK_TYPE_NONE, 0);
    debugger_client_signals [DETACHED] = 
        gtk_signal_new ("detached",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           detached),
                        gtk_marshal_NONE__NONE,
                        GTK_TYPE_NONE, 0,
                        GTK_TYPE_NONE);
    debugger_client_signals [COREFILE_LOADED] = 
        gtk_signal_new ("corefile_loaded",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           corefile_loaded),
                        gtk_marshal_NONE__NONE,
                        GTK_TYPE_NONE, 0);
    debugger_client_signals [COREFILE_UNLOADED] = 
        gtk_signal_new ("corefile_unloaded",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           corefile_unloaded),
                        gtk_marshal_NONE__NONE,
                        GTK_TYPE_NONE, 0);
    debugger_client_signals [EXECUTION_STARTED] = 
        gtk_signal_new ("execution_started",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           execution_started),
                        gtk_marshal_NONE__NONE,
                        GTK_TYPE_NONE, 0);
    debugger_client_signals [EXECUTION_RUNNING] = 
        gtk_signal_new ("execution_running",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           execution_running),
                        gtk_marshal_NONE__NONE,
                        GTK_TYPE_NONE, 0);
    debugger_client_signals [EXECUTION_STOPPED] = 
        gtk_signal_new ("execution_stopped",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           execution_stopped),
                        gtk_marshal_NONE__NONE,
                        GTK_TYPE_NONE, 0);
    debugger_client_signals [EXECUTION_KILLED] = 
        gtk_signal_new ("execution_killed",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           execution_killed),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);
    debugger_client_signals [EXECUTION_EXITED] = 
        gtk_signal_new ("execution_exited",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           execution_exited),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);
    debugger_client_signals [EXECUTION_SOURCE_LINE] = 
        gtk_signal_new ("execution_source_line",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           execution_source_line),
                        gtk_marshal_NONE__POINTER_INT,
                        GTK_TYPE_NONE, 2,
                        GTK_TYPE_STRING,
                        GTK_TYPE_INT);
    debugger_client_signals [STACK_FRAME_CHANGED] = 
        gtk_signal_new ("stack_frame_changed",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           stack_frame_changed),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);
    debugger_client_signals [SIGNAL_RECEIVED] = 
        gtk_signal_new ("signal_received",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           signal_received),
                        gtk_marshal_NONE__STRING,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_STRING);
    debugger_client_signals [SIGNAL_TERMINATION] = 
        gtk_signal_new ("signal_termination",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfDebuggerClientClass,
                                           signal_termination),
                        gtk_marshal_NONE__STRING,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_STRING);

    gtk_object_class_add_signals (object_class,
                                  debugger_client_signals,
                                  LAST_SIGNAL);

    klass->program_loaded = NULL;
    klass->program_unloaded = NULL;
    klass->breakpoint_set = NULL;
    object_class->destroy = (GtkSignalFunc) debugger_client_destroy;
}

static void
debugger_client_init (GdfDebuggerClient *client)
{
    client->priv = NULL;
}

static void
debugger_client_destroy (GdfDebuggerClient *client)
{
    CORBA_Environment ev;
    CORBA_Object source;
 
    CORBA_exception_init (&ev);
 
    source = GNOME_Development_Debugger_getEventSource (client->objref,
                                                              &ev);
    
    Bonobo_EventSource_removeListener (source,
                                       bonobo_object_corba_objref (BONOBO_OBJECT (client->priv->listener)),
                                       &ev);
    bonobo_object_unref (BONOBO_OBJECT (client->priv->listener));
 
    if (client->priv) {
        g_free (client->priv);
        client->priv = NULL;
    }

    Bonobo_Unknown_unref (client->objref, &ev);   
    
    CORBA_exception_free (&ev);

    if (parent_class->destroy)
        parent_class->destroy (GTK_OBJECT (client));
    
}

static void
emit_event_signal (GdfDebuggerClient *dbg,
                   GdfEvent *event)
{
    /* FIXME: Some table logic would be much better here */

    if (strcmp (event->event_name, "program_loaded") == 0) {
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[PROGRAM_LOADED]);
    } else if (strcmp (event->event_name, "program_unloaded") == 0) {
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[PROGRAM_UNLOADED]);
    } else if (strcmp (event->event_name, "breakpoint_set") == 0) {
        g_assert (event->arg_type == GDF_EVENT_ARG_LONG);
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[BREAKPOINT_SET],
                         (gint)event->argument.long_arg);
    } else if (strcmp (event->event_name, "breakpoint_enabled") == 0) {
        g_assert (event->arg_type == GDF_EVENT_ARG_LONG);
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[BREAKPOINT_ENABLED],
                         (gint)event->argument.long_arg);
    } else if (strcmp (event->event_name, "breakpoint_disabled") == 0) {
        g_assert (event->arg_type == GDF_EVENT_ARG_LONG);
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[BREAKPOINT_DISABLED],
                         (gint)event->argument.long_arg);
    } else if (strcmp (event->event_name, "breakpoint_deleted") == 0) {
        g_assert (event->arg_type == GDF_EVENT_ARG_LONG);
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[BREAKPOINT_DELETED],
                         (gint)event->argument.long_arg);
    } else if (strcmp (event->event_name, "attached") == 0) {
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[ATTACHED]);
    } else if (strcmp (event->event_name, "detached") == 0) {
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[DETACHED]);
    } else if (strcmp (event->event_name, "corefile_loaded") == 0) {
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[COREFILE_LOADED]);
    } else if (strcmp (event->event_name, "corefile_unloaded") == 0) {
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[COREFILE_UNLOADED]);
    } else if (strcmp (event->event_name, "started") == 0) {
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[EXECUTION_STARTED]);
    } else if (strcmp (event->event_name, "running") == 0) {
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[EXECUTION_RUNNING]);
    } else if (strcmp (event->event_name, "stopped") == 0) {
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[EXECUTION_STOPPED]);
    } else if (strcmp (event->event_name, "killed") == 0) {
        g_assert (event->arg_type == GDF_EVENT_ARG_LONG);
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[EXECUTION_KILLED],
                         (gint)event->argument.long_arg);
    } else if (strcmp (event->event_name, "exited") == 0) {
        g_assert (event->arg_type == GDF_EVENT_ARG_LONG);
        dbg->priv->cur_frame = -1;
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[EXECUTION_EXITED],
                         (gint)event->argument.long_arg);
    } else if (strcmp (event->event_name, "source_line") == 0) {
        g_assert (event->arg_type == GDF_EVENT_ARG_SOURCE_LOCATION);
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[EXECUTION_SOURCE_LINE],
                         (gchar*)event->argument.loc_arg->file,
                         (gint)event->argument.loc_arg->line);
    } else if (strcmp (event->event_name, "frame_change") == 0) {
        g_assert (event->arg_type = GDF_EVENT_ARG_LONG);
        dbg->priv->cur_frame = event->argument.long_arg;
        gtk_signal_emit (GTK_OBJECT (dbg), 
                         debugger_client_signals[STACK_FRAME_CHANGED],
                         GPOINTER_TO_INT (event->argument.long_arg));
    } else if (strcmp (event->event_name, "signal") == 0) {
        g_assert (event->arg_type = GDF_EVENT_ARG_STRING);
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[SIGNAL_RECEIVED],
                         event->argument.string_arg);
    } else if (strcmp (event->event_name, "signal_termination") == 0) {
        g_assert (event->arg_type = GDF_EVENT_ARG_STRING);
        gtk_signal_emit (GTK_OBJECT (dbg),
                         debugger_client_signals[SIGNAL_TERMINATION],
                         event->argument.string_arg);
    }
}

void 
event_pushed_cb (BonoboListener *listener,
                 const char *event_name,
                 CORBA_any *val,
                 CORBA_Environment *ev,
                 GdfDebuggerClient *dbg)
{
    GdfEvent *event;
    event = gdf_demarshal_event (event_name, val);
    
    emit_event_signal (dbg, event);

    gdf_event_destroy (event);
}

static GdfDebuggerClientResult 
exception_to_result (CORBA_Environment *ev)
{
    GdfDebuggerClientResult res;
    static GHashTable *table = NULL;

    if (!table) {
        table = g_hash_table_new (g_str_hash, g_str_equal);      
        g_hash_table_insert (table, ex_GNOME_Development_Debugger_InvalidBinary,
                             GINT_TO_POINTER (GDF_DEBUGGER_CLIENT_INVALID_BINARY));
        g_hash_table_insert (table, ex_GNOME_Development_Debugger_NoSymbols,
                             GINT_TO_POINTER (GDF_DEBUGGER_CLIENT_NO_SYMBOLS));
        g_hash_table_insert (table, ex_GNOME_Development_Debugger_DoesntExist,
                             GINT_TO_POINTER (GDF_DEBUGGER_CLIENT_DOESNT_EXIST));
        g_hash_table_insert (table, ex_GNOME_Development_Debugger_NotExecutable,
                             GINT_TO_POINTER (GDF_DEBUGGER_CLIENT_NOT_EXECUTABLE));
        g_hash_table_insert (table, ex_GNOME_Development_Debugger_SecurityException,
                             GINT_TO_POINTER (GDF_DEBUGGER_CLIENT_SECURITY_EXCEPTION));
        g_hash_table_insert (table, ex_GNOME_Development_Debugger_InvalidState,
                             GINT_TO_POINTER (GDF_DEBUGGER_CLIENT_INVALID_STATE));
        g_hash_table_insert (table, ex_GNOME_Development_Debugger_UnknownError,
                             GINT_TO_POINTER (GDF_DEBUGGER_CLIENT_UNKNOWN_ERROR));
        g_hash_table_insert (table, ex_GNOME_Development_Debugger_InvalidCorefile,
                             GINT_TO_POINTER (GDF_DEBUGGER_CLIENT_INVALID_COREFILE));
    }

    res = GDF_DEBUGGER_CLIENT_OK;
    
    if (ev->_major == CORBA_USER_EXCEPTION) {
        res = GPOINTER_TO_INT (g_hash_table_lookup (table, ev->_repo_id));
    }

    return res;
}

