/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2001 Dave Camp <dave@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gdl/gdl-tools.h>
#include "gdf-breakpoint-set-client.h"

#include <bonobo/bonobo-listener.h>
#include "gdf-event-marshallers.h"

static void breakpoint_set_client_class_init (GdfBreakpointSetClientClass *class);
static void breakpoint_set_client_init (GdfBreakpointSetClient *prog);
static void breakpoint_set_client_destroy (GdfBreakpointSetClient *prog);

static void event_pushed_cb (BonoboListener *event_channel,
                             const char *event_name,
                             CORBA_any *val,
                             CORBA_Environment *ev,
                             GdfBreakpointSetClient *ss);
static GdfBreakpointSetClientResult exception_to_result (CORBA_Environment *ev);

static GtkObjectClass *parent_class;

struct _GdfBreakpointSetClientPrivate {
    BonoboListener *listener;
    
    Bonobo_EventSource_ListenerId listener_id;
};

enum {
    BREAKPOINT_SET,
    BREAKPOINT_ENABLED,
    BREAKPOINT_DISABLED,
    BREAKPOINT_DELETED,
    LAST_SIGNAL
};

static gint breakpoint_set_client_signals[LAST_SIGNAL];

static void 
gdf_breakpoint_set_client_construct (GdfBreakpointSetClient *bs,
                                     CORBA_Object corba_object)
{
    CORBA_Object source;
    CORBA_Environment ev;

    bs->objref = corba_object;
    bs->priv = g_new0 (GdfBreakpointSetClientPrivate, 1);
    
    CORBA_exception_init (&ev);

    bs->priv->listener = bonobo_listener_new (NULL, NULL);
    
    gtk_signal_connect (GTK_OBJECT (bs->priv->listener),
                        "event_notify", 
                        GTK_SIGNAL_FUNC (event_pushed_cb), 
                        bs);
    
    source = Bonobo_Unknown_queryInterface (bs->objref,
                                            "IDL:Bonobo/EventSource:1.0",
                                            &ev);
    
    
    bs->priv->listener_id = Bonobo_EventSource_addListener (source,
                                                            bonobo_object_corba_objref (BONOBO_OBJECT (bs->priv->listener)),
                                                            &ev);
    if (ev._major) {
        g_error ("could not connect to listener");
    }

    GDL_TRACE_EXTRA ("%p", bs);

    CORBA_Object_release (source, &ev);

    /* FIXME: Check env */

    CORBA_exception_free (&ev);
}

/**
 * gdf_breakpoint_set_client_new_from_objref:
 * @breakpoint_set: GDF::BreakpointSet CORBA object reference.
 *
 * Creates a GdfBreakpointSetClient GTK Object which wraps around the
 * GDF::BreakpointSet CORBA object.
 * * Returns: the initialized GdfBreakpointSetClient object.
 */
GdfBreakpointSetClient *
gdf_breakpoint_set_client_new_from_corba (GNOME_Development_BreakpointSet breakpoint_set) 
{
    GdfBreakpointSetClient *ret;
    CORBA_Environment ev;

    g_return_val_if_fail (breakpoint_set != CORBA_OBJECT_NIL, NULL);

    ret = gtk_type_new (gdf_breakpoint_set_client_get_type ());
    gdf_breakpoint_set_client_construct (ret, breakpoint_set);
   
    CORBA_exception_init (&ev);

    Bonobo_Unknown_ref (breakpoint_set, &ev);
    
    /* FIXME: Check env */
    
    CORBA_exception_free (&ev);

    GDL_TRACE_EXTRA ("%p", ret);

    return ret;
}

GdfBreakpointSetClient *
gdf_breakpoint_set_client_new (void)
{
    CORBA_Environment ev;
    CORBA_Object corba_object;

    CORBA_exception_init (&ev);
    
    corba_object = oaf_activate_from_id ((const OAF_ActivationID)"OAFIID:GNOME_Development_BreakpointSet", 
                                         OAF_FLAG_PRIVATE, 
                                         NULL, &ev);    
    
    if (CORBA_Object_is_nil (corba_object, &ev)) {
        CORBA_exception_free (&ev);
        return NULL;
    }
    CORBA_exception_free (&ev);

    return gdf_breakpoint_set_client_new_from_corba (corba_object);
    
}

GtkType
gdf_breakpoint_set_client_get_type (void)
{
    static GtkType type = 0;
    
    if (!type) {
        GtkTypeInfo info = {
            "GdfBreakpointSetClient",
            sizeof (GdfBreakpointSetClient),
            sizeof (GdfBreakpointSetClientClass),
            (GtkClassInitFunc) breakpoint_set_client_class_init,
            (GtkObjectInitFunc) breakpoint_set_client_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
	
        type = gtk_type_unique (gtk_object_get_type (), &info);
    }
    
    return type;
}


/**
 * gdf_breakpoint_set_client_get_breakpoints:
 * @bs: The #GdfBreakpointSetClient object.
 * @ret: An out parameter through which a GNOME_Development_BreakpointList containing all 
 *       the breakpoints set will be returned.  If the operation is 
 *       unsuccessful this value will be NULL.  If successful, it must be 
 *       freed by the caller using CORBA_free().
 * 
 * Gets a list of all the breakpoints set in the debugger.  This function
 * requires the %GNOME_Development_BreakpointSet_BINARY_LOADED flag to be set. 
 *
 * Return value: A GdfBreakpointSetClientResult value indicating the success of the
 *               operation.
 **/
GdfBreakpointSetClientResult
gdf_breakpoint_set_client_get_breakpoints (GdfBreakpointSetClient *bs,
                                     GNOME_Development_BreakpointList **ret)
{
    CORBA_Environment ev;
    GdfBreakpointSetClientResult res;
    
    g_return_val_if_fail (bs != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_BREAKPOINT_SET_CLIENT (bs), 
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);
    *ret = GNOME_Development_BreakpointSet_getBreakpoints (bs->objref, &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_BREAKPOINT_SET_CLIENT_OK) {
        *ret = NULL;
    }

    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_breakpoint_set_client_set_breakpoint:
 * @bs: The #GdfBreakpointSetClient object.
 * @file_name: The file to set the breakpoint in.  The path should not be
 *             absolute, the debugging information will be used to find
 *             the correct file.
 * @line_num: The line number to set the breakpoint on.
 * @condition: An expression in the target language that will be evaludated 
 *             by the debugger when the location is reached.  Execution will 
 *             only stop if the expression evaluates true.  NULL indicates an
 *             unconditional breakpoint.
 * @bp_num: An out parameter through which the breakpoint number will be 
 *          returned.  Will be -1 if the operation was unsuccessful.
 * 
 * Sets a breakpoint at the given location.  Execution will stop when
 * the location is reached, assuming @condition evaluates to %TRUE.
 * 
 * Return value: A GdfBreakpointSetClientResult value indicating the success of the
 *               operation.
 **/
GdfBreakpointSetClientResult
gdf_breakpoint_set_client_set_breakpoint (GdfBreakpointSetClient *bs,
                                          const char *file_name,
                                          long line_num,
                                          const char *condition,
                                          long *bp_num)
{
    CORBA_Environment ev;
    long num;
    GdfBreakpointSetClientResult res;

    g_return_val_if_fail (bs != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (file_name != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_BREAKPOINT_SET_CLIENT (bs),
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);

    fprintf (stderr, "about to set breakpoint\n");

    num = 
        GNOME_Development_BreakpointSet_setBreakpoint (bs->objref,
                                                             (CORBA_char*)(file_name ? file_name :""),
                                                             line_num,
                                                             (CORBA_char*)(condition ? condition :""),
                                                             &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_BREAKPOINT_SET_CLIENT_OK) {
        num = -1;
    }

    if (bp_num) {
        *bp_num = num;
    }

    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_breakpoint_set_client_set_breakpoint_function:
 * @bs: 
 * @file_name: The file to set the breakpoint in.  The path should not be
 *             absolute, the debugging information will be used to find the
 *             correct file.  This can be NULL, in which case only the function
 *             name will be used.
 * @function_name: The function to set the breakpoint on.
 * @condition: An expression in the target language that will be evaludated 
 *             by the debugger when the location is reached.  Execution will 
 *             only stop if the expression evaluates true. NULL indicates an
 *             unconditional breakpoint.
 * @bp_num: An out parameter through which the breakpoint number will be 
 *          returned.  Will be -1 if the operation was unsuccessful.
 * 
 * Sets a breakpoint on a given function.  Execution will stop when execution
 * enters the given function, assuming @condition evaluates to %TRUE.
 * 
 * Return value: A GdfBreakpointSetClientResult value indicating the success of the
 *               operation.
 **/
GdfBreakpointSetClientResult
gdf_breakpoint_set_client_set_breakpoint_function (GdfBreakpointSetClient *bs,
                                             const char *file_name,
                                             const char *function_name,
                                             const char *condition,
                                             long *bp_num)
{
    CORBA_Environment ev;
    GdfBreakpointSetClientResult res;
    long num;
    
    g_return_val_if_fail (bs != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_BREAKPOINT_SET_CLIENT (bs),
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (function_name != NULL,
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    num = 
        GNOME_Development_BreakpointSet_setBreakpointFunction 
        (bs->objref,
         (CORBA_char*)(file_name ? file_name : ""),
         (CORBA_char*)(function_name ? function_name : ""),
         (CORBA_char*)(condition ? condition : ""),
         &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_BREAKPOINT_SET_CLIENT_OK) {
        num = -1;
    }

    if (bp_num) {
        *bp_num = num;
    }
    
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_breakpoint_set_client_set_watchpoint:
 * @bs: The #GdfBreakpointSetClient object.
 * @expr: The expression to evaluate.
 * @condition: An expression in the target language that will be evaludated 
 *             by the debugger when the location is reached.  Execution will 
 *             only stop if the expression evaluates true. NULL indicates an
 *             unconditional breakpoint.
 * @bp_num: An out parameter through which the breakpoint number will be 
 *          returned.  Will be -1 if the operation was unsuccessful.
 * 
 * Sets a watchpoint (value-based breakpoint) on the given expression.  
 * Execution will stop when the value of the expression changes, assuming 
 * @condition evaluates to %TRUE.  The watchpoint can be manipulated using
 * the breakpoint manipulation functions.
 * 
 * Return value: A GdfBreakpointSetClientResult value indicating the success of the
 *               operation.
 **/
GdfBreakpointSetClientResult
gdf_breakpoint_set_client_set_watchpoint (GdfBreakpointSetClient *bs,
                                    const char *expr,
                                    const char *condition,
                                    long *bp_num)
{
    CORBA_Environment ev;
    GdfBreakpointSetClientResult res;
    long num;
    
    g_return_val_if_fail (bs != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_BREAKPOINT_SET_CLIENT (bs),
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (expr != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    num = GNOME_Development_BreakpointSet_setWatchpoint (bs->objref, expr, 
                                       condition ? condition : "",
                                       &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_BREAKPOINT_SET_CLIENT_OK) {
        num = -1;
    }

    if (bp_num) {
        *bp_num = num;
    }
    
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_breakpoint_set_client_enable_breakpoint:
 * @bs: The #GdfBreakpointSetClient object.
 * @bp_num: The breakpoint to enable.
 * 
 * Enables a breakpoint.  
 * 
 * Return value: A GdfBreakpointSetClientResult value indicating the success of the
 *               operation.
 **/
GdfBreakpointSetClientResult
gdf_breakpoint_set_client_enable_breakpoint (GdfBreakpointSetClient *bs,
                                       long bp_num)
{
    CORBA_Environment ev;
    GdfBreakpointSetClientResult res;
    
    g_return_val_if_fail (bs != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_BREAKPOINT_SET_CLIENT (bs),
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);
    
    GNOME_Development_BreakpointSet_enableBreakpoint (bs->objref,
                                     bp_num,
                                     &ev);

    /* FIXMR: Check env */
    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_breakpoint_set_client_disable_breakpoint:
 * @bs: The #GdfBreakpointSetClient object.
 * @bp_num: The breakpoint to disable.
 * 
 * Disables a breakpoint.
 * 
 * Return value: A GdfBreakpointSetClientResult value indicating the success of the
 *               operation.
 **/
GdfBreakpointSetClientResult
gdf_breakpoint_set_client_disable_breakpoint (GdfBreakpointSetClient *bs,
                                        long bp_num)
{
    CORBA_Environment ev;
    GdfBreakpointSetClientResult res;
    
    g_return_val_if_fail (bs != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_BREAKPOINT_SET_CLIENT (bs),
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    
    GNOME_Development_BreakpointSet_disableBreakpoint (bs->objref,
                                    bp_num,
                                    &ev);
    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_breakpoint_set_client_delete_breakpoint:
 * @bs: The #GdfBreakpointSetClient object.
 * @bp_num: The breakpoint to delete.
 * 
 * Deletes a breakpoint.  When this call returns, @bp_num is no longer a valid
 * breakpoint identifier.
 * 
 * Return value: A GdfBreakpointSetClientResult value indicating the success of the
 *               operation.
 **/
GdfBreakpointSetClientResult
gdf_breakpoint_set_client_delete_breakpoint (GdfBreakpointSetClient *bs,
                                       long bp_num)
{
    CORBA_Environment ev;
    GdfBreakpointSetClientResult res;
    
    g_return_val_if_fail (bs != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_BREAKPOINT_SET_CLIENT (bs),
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    GNOME_Development_BreakpointSet_deleteBreakpoint (bs->objref,
                                   bp_num,
                                   &ev);

    res = exception_to_result (&ev);
    CORBA_exception_free (&ev);
    return res;
}

/**
 * gdf_breakpoint_set_client_get_breakpoint_info:
 * @bs: The #GdfBreakpointSetClient object.
 * @bp_num: The breakpoint to get information about.
 * @ret: An out parameter through which a GNOME_Development_Breakpoint structure will be
 *       returned.  This value will be NULL if the operation failed.  If
 *       successful the caller must free the structure using CORBA_free().
 * 
 * Retrieves information about a given breakpoint.
 * 
 * Return value: A GdfBreakpointSetClientResult value indicating the success of the
 *               operation.
 **/
GdfBreakpointSetClientResult
gdf_breakpoint_set_client_get_breakpoint_info (GdfBreakpointSetClient *bs,
                                         long bp_num,
                                         GNOME_Development_Breakpoint **ret)
{
    CORBA_Environment ev;
    GdfBreakpointSetClientResult res;
    
    g_return_val_if_fail (bs != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_BREAKPOINT_SET_CLIENT (bs), 
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (ret != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    
    CORBA_exception_init (&ev);
    *ret = GNOME_Development_BreakpointSet_getBreakpointInfo (bs->objref,
                                           (CORBA_long)bp_num,
                                           &ev);
    
    res = exception_to_result (&ev);
    if (res != GDF_BREAKPOINT_SET_CLIENT_OK) {
        *ret = NULL;
    }

    CORBA_exception_free (&ev);
    return res;
}

GdfBreakpointSetClientResult
gdf_breakpoint_set_client_set_debugger (GdfBreakpointSetClient *bs,
                                        GdfDebuggerClient *dbg)
{
    CORBA_Environment ev;
    GdfBreakpointSetClientResult res;
    
    g_return_val_if_fail (bs != NULL, GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (GDF_IS_BREAKPOINT_SET_CLIENT (bs), 
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);
    g_return_val_if_fail (dbg == NULL || GDF_IS_DEBUGGER_CLIENT (dbg),
                          GDF_BREAKPOINT_SET_CLIENT_BAD_PARAMS);

    CORBA_exception_init (&ev);
    GNOME_Development_BreakpointSet_setDebugger (bs->objref,
                                                       dbg ? dbg->objref : CORBA_OBJECT_NIL,
                                                       &ev);
    res = exception_to_result (&ev);

    CORBA_exception_free (&ev);
    return res;
}


                                               

/* private routines */
static void
breakpoint_set_client_class_init (GdfBreakpointSetClientClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *)klass;
    
    g_return_if_fail (klass != NULL);
    g_return_if_fail (GDF_IS_BREAKPOINT_SET_CLIENT_CLASS (klass));
    
    parent_class = gtk_type_class (gtk_object_get_type ());
    
    breakpoint_set_client_signals [BREAKPOINT_SET] = 
        gtk_signal_new ("breakpoint_set",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfBreakpointSetClientClass,
                                           breakpoint_set),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);
    breakpoint_set_client_signals [BREAKPOINT_ENABLED] = 
        gtk_signal_new ("breakpoint_enabled",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfBreakpointSetClientClass,
                                           breakpoint_enabled),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);
    breakpoint_set_client_signals [BREAKPOINT_DISABLED] = 
        gtk_signal_new ("breakpoint_disabled",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfBreakpointSetClientClass,
                                           breakpoint_disabled),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);
    breakpoint_set_client_signals [BREAKPOINT_DELETED] = 
        gtk_signal_new ("breakpoint_deleted",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfBreakpointSetClientClass,
                                           breakpoint_deleted),
                        gtk_marshal_NONE__INT,
                        GTK_TYPE_NONE, 1,
                        GTK_TYPE_INT);

    gtk_object_class_add_signals (object_class,
                                  breakpoint_set_client_signals,
                                  LAST_SIGNAL);

    object_class->destroy = (GtkSignalFunc) breakpoint_set_client_destroy;
}

static void
breakpoint_set_client_init (GdfBreakpointSetClient *client)
{
    client->priv = NULL;
}

static void
breakpoint_set_client_destroy (GdfBreakpointSetClient *client)
{
    CORBA_Environment ev;
    CORBA_Object source;

    GDL_TRACE ();

    CORBA_exception_init (&ev);
 
    source = Bonobo_Unknown_queryInterface (client->objref, 
                                            "IDL:Bonobo/EventSource:1.0",&ev);
    
    Bonobo_EventSource_removeListener (source,
                                       client->priv->listener_id,
                                       &ev);
    bonobo_object_unref (BONOBO_OBJECT (client->priv->listener));
     

    if (client->priv) {
        g_free (client->priv);
        client->priv = NULL;
    }
    
    Bonobo_Unknown_unref (client->objref, &ev);
    
    /* FIXME: Check env */
    
    CORBA_exception_free (&ev);

    if (parent_class->destroy)
        parent_class->destroy (GTK_OBJECT (client));
    
}

static void
emit_event_signal (GdfBreakpointSetClient *bs,
                   GdfEvent *event)
{
    /* FIXME: Some table logic would be much better here */

    if (strcmp (event->event_name, "breakpoint_set") == 0) {
        GDL_TRACE_EXTRA ("%p", bs);
        g_assert (GDF_IS_BREAKPOINT_SET_CLIENT (bs));

        g_assert (event->arg_type == GDF_EVENT_ARG_LONG);
        gtk_signal_emit (GTK_OBJECT (bs),
                         breakpoint_set_client_signals[BREAKPOINT_SET],
                         (gint)event->argument.long_arg);
    } else if (strcmp (event->event_name, "breakpoint_enabled") == 0) {
        g_assert (event->arg_type == GDF_EVENT_ARG_LONG);
        gtk_signal_emit (GTK_OBJECT (bs),
                         breakpoint_set_client_signals[BREAKPOINT_ENABLED],
                         (gint)event->argument.long_arg);
    } else if (strcmp (event->event_name, "breakpoint_disabled") == 0) {
        g_assert (event->arg_type == GDF_EVENT_ARG_LONG);
        gtk_signal_emit (GTK_OBJECT (bs),
                         breakpoint_set_client_signals[BREAKPOINT_DISABLED],
                         (gint)event->argument.long_arg);
    } else if (strcmp (event->event_name, "breakpoint_deleted") == 0) {
        g_assert (event->arg_type == GDF_EVENT_ARG_LONG);
        gtk_signal_emit (GTK_OBJECT (bs),
                         breakpoint_set_client_signals[BREAKPOINT_DELETED],
                         (gint)event->argument.long_arg);
    }

}

void 
event_pushed_cb (BonoboListener *listener, 
                 const char *event_name,
                 CORBA_any *val,
                 CORBA_Environment *ev,
                 GdfBreakpointSetClient *bs)
{
    GdfEvent *event;
    event = gdf_demarshal_event (event_name, val);

    g_assert (GDF_IS_BREAKPOINT_SET_CLIENT (bs));

    emit_event_signal (bs, event);

    gdf_event_destroy (event);
}

GdfBreakpointSetClientResult 
exception_to_result (CORBA_Environment *ev)
{
    GdfBreakpointSetClientResult res;
    static GHashTable *table = NULL;

    if (!table) {
        table = g_hash_table_new (g_str_hash, g_str_equal);      
        g_hash_table_insert (table, ex_GNOME_Development_BreakpointSet_DoesntExist,
                             GINT_TO_POINTER (GDF_BREAKPOINT_SET_CLIENT_DOESNT_EXIST));
    }

    res = GDF_BREAKPOINT_SET_CLIENT_OK;
    
    if (ev->_major == CORBA_USER_EXCEPTION) {
        res = GPOINTER_TO_INT (g_hash_table_lookup (table, ev->_repo_id));
    }

    return res;
}
