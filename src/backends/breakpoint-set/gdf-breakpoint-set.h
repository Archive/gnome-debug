/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 2001 Dave Camp <dave@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */


#ifndef __GDF_BREAKPOINT_SET_H__
#define __GDF_BREAKPOINT_SET_H__

#include <bonobo/bonobo-xobject.h>
#include <bonobo/bonobo-event-source.h>
#include <src/lib/gdf.h>

BEGIN_GNOME_DECLS

#define GDF_BREAKPOINT_SET_TYPE                (gdf_breakpoint_set_get_gtk_type ())
#define GDF_BREAKPOINT_SET(o)                  (GTK_CHECK_CAST ((o), GDF_BREAKPOINT_SET_TYPE, GdfBreakpointSet))
#define GDF_BREAKPOINT_SET_CLASS(k)            (GTK_CHECK_CLASS_CAST((k), GDF_BREAKPOINT_SET_TYPE, GdfBreakpointSetClass))
#define GDF_IS_BREAKPOINT_SET(o)               (GTK_CHECK_TYPE ((o), GDF_BREAKPOINT_SET_TYPE))
#define GDF_IS_BREAKPOINT_SET_CLASS(k)         (GTK_CHECK_CLASS_TYPE ((k), GDF_BREAKPOINT_SET_TYPE))

typedef struct _GdfBreakpointSetPriv  GdfBreakpointSetPriv;
typedef struct _GdfBreakpointSet      GdfBreakpointSet;

struct _GdfBreakpointSet {
    BonoboXObject parent;
    GdfBreakpointSetPriv *priv;
    BonoboEventSource *event_source;
};

typedef struct {
	BonoboXObjectClass parent;
	POA_GNOME_Development_BreakpointSet__epv epv;
} GdfBreakpointSetClass;

GtkType           gdf_breakpoint_set_get_gtk_type (void);
GdfBreakpointSet *gdf_breakpoint_set_new          (void);
GdfBreakpointSet *gdf_breakpoint_set_construct    (GdfBreakpointSet *bs, 
                                                   BonoboEventSource *es);

END_GNOME_DECLS

#endif
