/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 2001 Dave Camp <dave@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include "gdf-breakpoint-set.h"
#include <liboaf/liboaf.h>

#define BREAKPOINT_SET_IID "OAFIID:GNOME_Development_BreakpointSet"

static CORBA_Object
make_object (PortableServer_POA poa,
	     const char *iid,
	     gpointer impl_ptr,
	     CORBA_Environment *ev)
{
	GdfBreakpointSet *bs;

	fprintf (stderr, "Creating breakpointset object\n");

	bs = gdf_breakpoint_set_new ();

	oaf_plugin_use (poa, impl_ptr);

	return BONOBO_OBJREF (bs);
}

static const OAFPluginObject plugin_list[] = 
{
	{
		BREAKPOINT_SET_IID,
		make_object
	},
	{
		NULL
	}
};

const OAFPlugin OAF_Plugin_info = {
	plugin_list,
	"GDF Breakpoint Set"
};
		
