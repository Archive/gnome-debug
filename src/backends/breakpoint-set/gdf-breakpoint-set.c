/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 2001 Dave Camp <dave@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gdf-breakpoint-set.h>

#include <gdl/gdl-tools.h>

#define PARENT_TYPE BONOBO_X_OBJECT_TYPE

static GtkObjectClass *parent_class = NULL;

struct _GdfBreakpointSetPriv {
    GdfDebuggerClient *dbg;
    long next_bp;
    GHashTable *breakpoints;  /* breakpoint number -> breakpoint */
    GList *breakpoint_list;

    GHashTable *dbg_to_bpset; /* debugger bp number -> breakpoint number */
    GHashTable *bpset_to_dbg; /* breakpoint number -> debugger bp number */
};

static void connect_debugger_signals (GdfBreakpointSet *bs);
static void disconnect_debugger_signals (GdfBreakpointSet *bs);

static void enable_breakpoint (GdfBreakpointSet *bs,
                               GNOME_Development_Breakpoint *bp);
static void disable_breakpoint (GdfBreakpointSet *bs,
                                GNOME_Development_Breakpoint *bp);
static void remove_breakpoint (GdfBreakpointSet *bs,
                               GNOME_Development_Breakpoint *bp);

static void
breakpoint_dup (GNOME_Development_Breakpoint *dest, 
                GNOME_Development_Breakpoint *src)
{
    if (src) {
        dest->num = src->num;
        dest->type = CORBA_string_dup (src->type);
        dest->enabled = src->enabled;
        dest->address = CORBA_string_dup (src->address);
        dest->file_name = CORBA_string_dup (src->file_name);
        dest->function = CORBA_string_dup (src->function);
        dest->line_num = (CORBA_long)src->line_num;
    } else {
        dest->num = -1;
        dest->type = CORBA_string_dup ("");
        dest->enabled = FALSE;
        dest->address = CORBA_string_dup ("");
        dest->file_name = CORBA_string_dup ("");
        dest->function = CORBA_string_dup ("");
        dest->line_num = 0;
    }
}

static void
breakpoint_clear (GNOME_Development_Breakpoint *bp)
{
    CORBA_free (bp->type);
    CORBA_free (bp->address);
    CORBA_free (bp->file_name);
    CORBA_free (bp->function);
}

static void 
map_bp_numbers (GdfBreakpointSet *bs, int bpset, int dbg)
{
    g_hash_table_insert (bs->priv->dbg_to_bpset, 
                         GINT_TO_POINTER (dbg), GINT_TO_POINTER (bpset));
    g_hash_table_insert (bs->priv->bpset_to_dbg, 
                         GINT_TO_POINTER (bpset), GINT_TO_POINTER (dbg));
}

#define BPSET_TO_DBG(n) (GPOINTER_TO_INT (g_hash_table_lookup (bs->priv->bpset_to_dbg, GINT_TO_POINTER(n))))
#define DBG_TO_BPSET(n) (GPOINTER_TO_INT (g_hash_table_lookup (bs->priv->dbg_to_bpset, GINT_TO_POINTER(n))))

static GNOME_Development_Breakpoint *
breakpoint_from_dbg (GdfBreakpointSet *bs, int dbg_bpnum)
{
    int num = GPOINTER_TO_INT (g_hash_table_lookup (bs->priv->dbg_to_bpset,
                                                    GINT_TO_POINTER (dbg_bpnum)));
    return g_hash_table_lookup (bs->priv->dbg_to_bpset,
                                GINT_TO_POINTER (num));
}

GdfBreakpointSet *
gdf_breakpoint_set_construct (GdfBreakpointSet *bs, BonoboEventSource *es)
{
    bs->event_source = es;

    bonobo_object_add_interface (BONOBO_OBJECT (bs), BONOBO_OBJECT (es));
    
    return bs;
}

GdfBreakpointSet *
gdf_breakpoint_set_new (void)
{
    GdfBreakpointSet *bs = gtk_type_new (GDF_BREAKPOINT_SET_TYPE);
    BonoboEventSource *es = bonobo_event_source_new ();
    
    return gdf_breakpoint_set_construct (bs, es);    
}

static void
send_event (BonoboEventSource *source, const char *name, CORBA_any *data)
{
    bonobo_event_source_notify_listeners (source, name, data, NULL);
    CORBA_free (data);
}

/* implementation functions */

static GNOME_Development_BreakpointList *
impl_get_breakpoints (PortableServer_Servant servant,
                      CORBA_Environment *ev)
{     
    GdfBreakpointSet *bs = GDF_BREAKPOINT_SET (bonobo_object_from_servant (servant));
    GList *breakpoints;
    GList *i;
    int n;
    GNOME_Development_BreakpointList *ret = GNOME_Development_BreakpointList__alloc ();
   
    breakpoints = bs->priv->breakpoint_list;
    
    n = g_list_length (breakpoints); 
    ret->_length = n;
    ret->_maximum = n;
    ret->_buffer = CORBA_sequence_GNOME_Development_Breakpoint_allocbuf (n);
    CORBA_sequence_set_release (ret, TRUE);
    
    n = 0;
    for (i = breakpoints; i != NULL; i = i->next) {
        GNOME_Development_Breakpoint *bp = i->data;
        GNOME_Development_Breakpoint *corba_bp = &ret->_buffer[n++];
        
        breakpoint_dup (corba_bp, bp);
        GDL_TRACE_EXTRA ("%s %d", corba_bp->file_name, corba_bp->line_num);
    }
    
    return ret;
}

static void
add_breakpoint (GdfBreakpointSet *bs, GNOME_Development_Breakpoint *bp)
{
    g_hash_table_insert (bs->priv->breakpoints, GINT_TO_POINTER (bp->num), bp);
    bs->priv->breakpoint_list = g_list_append (bs->priv->breakpoint_list, bp);
    send_event (bs->event_source, "breakpoint_set", 
                gdf_marshal_event_long ((long)bp->num));
}

static void
enable_breakpoint (GdfBreakpointSet *bs,
                   GNOME_Development_Breakpoint *bp)
{    
    bp->enabled = TRUE;
    send_event (bs->event_source, "breakpoint_enabled",
                gdf_marshal_event_long ((long)bp->num));   

}

static void
disable_breakpoint (GdfBreakpointSet *bs,
                    GNOME_Development_Breakpoint *bp)
{
    bp->enabled = FALSE;
    send_event (bs->event_source, "breakpoint_disabled",
                gdf_marshal_event_long ((long)bp->num));
}

static void
remove_breakpoint (GdfBreakpointSet *bs, 
                   GNOME_Development_Breakpoint *bp)
{
    send_event (bs->event_source, "breakpoint_deleted",
                gdf_marshal_event_long ((long)bp->num));

    g_hash_table_remove (bs->priv->breakpoints, GINT_TO_POINTER (bp->num));
    g_list_remove (bs->priv->breakpoint_list, bp);
}


static CORBA_long 
impl_set_breakpoint (PortableServer_Servant servant,
                     const CORBA_char *file_name,
                     CORBA_long line_num,
                     const CORBA_char *condition,
                     CORBA_Environment *ev)
{
    GdfBreakpointSet *bs = GDF_BREAKPOINT_SET (bonobo_object_from_servant (servant));
    
    GNOME_Development_Breakpoint *bp;
    int ret = -1;
    
    if (bs->priv->dbg) {
        GdfDebuggerClientResult res;
        long bp_num;
        res = gdf_debugger_client_set_breakpoint (bs->priv->dbg, file_name, 
                                                  line_num, condition,
                                                  &bp_num);
        if (res == GDF_DEBUGGER_CLIENT_OK && bp_num != -1) {
            GNOME_Development_Breakpoint *dbg_bp;
            
            res = gdf_debugger_client_get_breakpoint_info (bs->priv->dbg,
                                                           bp_num, &dbg_bp);
            if (res == GDF_DEBUGGER_CLIENT_OK) {
                bp = GNOME_Development_Breakpoint__alloc ();
                breakpoint_dup (bp, dbg_bp);
                CORBA_free (dbg_bp);
                ret = bp->num = bs->priv->next_bp++;
                map_bp_numbers (bs, ret, bp_num);
                add_breakpoint (bs, bp);
            }
        }
    } else {
        bp = GNOME_Development_Breakpoint__alloc ();
        bp->num = bs->priv->next_bp++;
        bp->type = CORBA_string_dup ("");
        bp->enabled = TRUE;
        bp->address = CORBA_string_dup ("???");
        bp->file_name = CORBA_string_dup (file_name);
        bp->function = CORBA_string_dup ("???");
        bp->line_num = line_num;

        add_breakpoint (bs, bp);

        ret = bp->num;
    }
    
    return ret;
}

static CORBA_long 
impl_set_breakpoint_function (PortableServer_Servant servant,
                              const CORBA_char *file_name,
                              const CORBA_char *function_name,
                              const CORBA_char *condition,
                              CORBA_Environment *ev)
{
    GdfBreakpointSet *bs = GDF_BREAKPOINT_SET (bonobo_object_from_servant (servant));
    GNOME_Development_Breakpoint *bp = NULL;
    int ret = -1;
    
    if (bs->priv->dbg) {
        GdfDebuggerClientResult res;
        long bp_num;
        res = gdf_debugger_client_set_breakpoint_function (bs->priv->dbg,
                                                           file_name, 
                                                           function_name,
                                                           condition,
                                                           &bp_num);
        if (res == GDF_DEBUGGER_CLIENT_OK && bp_num != -1) {
            GNOME_Development_Breakpoint *dbg_bp;
            
            res = gdf_debugger_client_get_breakpoint_info (bs->priv->dbg,
                                                           bp_num, &dbg_bp);
            if (res == GDF_DEBUGGER_CLIENT_OK) {
                bp = GNOME_Development_Breakpoint__alloc ();
                breakpoint_dup (bp, dbg_bp);
                CORBA_free (dbg_bp);
                ret = bp->num = bs->priv->next_bp++;
                map_bp_numbers (bs, ret, bp_num);
                add_breakpoint (bs, bp);
            }
        }
    } else {
        bp = GNOME_Development_Breakpoint__alloc ();
        bp->num = bs->priv->next_bp++;
        bp->type = CORBA_string_dup ("");
        bp->enabled = TRUE;
        bp->address = CORBA_string_dup ("???");
        bp->file_name = CORBA_string_dup (file_name);
        bp->function = CORBA_string_dup (function_name);
        bp->line_num = 0;
        
        add_breakpoint (bs, bp);
        ret = bp->num;
    }
    
    return ret;    
}

static CORBA_long 
impl_set_watchpoint (PortableServer_Servant servant,
                     const CORBA_char *expression,
                     const CORBA_char *condition,
                     CORBA_Environment *ev)
{
    
}

static void 
impl_enable_breakpoint (PortableServer_Servant servant,
                        CORBA_long bp_num,
                        CORBA_Environment *ev)
{
    GdfBreakpointSet *bs = GDF_BREAKPOINT_SET (bonobo_object_from_servant (servant));
    GNOME_Development_Breakpoint *bp;
    
    GDL_TRACE ();
    
    if (!bs->priv->dbg) {
        gdf_debugger_client_enable_breakpoint (bs->priv->dbg, BPSET_TO_DBG (bp_num));
    } else {
        bp = g_hash_table_lookup (bs->priv->breakpoints, GINT_TO_POINTER (bp_num));
        if (!bp) {
            return;
        }
        enable_breakpoint (bs, bp);
    }
     
}

static void
impl_disable_breakpoint (PortableServer_Servant servant,
                         CORBA_long bp_num,
                         CORBA_Environment *ev)
{
    GdfBreakpointSet *bs = GDF_BREAKPOINT_SET (bonobo_object_from_servant (servant));
    GNOME_Development_Breakpoint *bp;
    
    GDL_TRACE ();
    
    if (!bs->priv->dbg) {
        gdf_debugger_client_disable_breakpoint (bs->priv->dbg, BPSET_TO_DBG (bp_num));
    } else {
        bp = g_hash_table_lookup (bs->priv->breakpoints, GINT_TO_POINTER (bp_num));
        if (!bp) {
            return;
        }
        
        disable_breakpoint (bs, bp);
    }
}

static void 
impl_delete_breakpoint (PortableServer_Servant servant,
                        CORBA_long bp_num,
                        CORBA_Environment *ev)
{
    GdfBreakpointSet *bs = GDF_BREAKPOINT_SET (bonobo_object_from_servant (servant));
    GNOME_Development_Breakpoint *bp;
    
    if (!bs->priv->dbg) {
        gdf_debugger_client_delete_breakpoint (bs->priv->dbg, BPSET_TO_DBG (bp_num));
    } else {
        bp = g_hash_table_lookup (bs->priv->breakpoints, GINT_TO_POINTER (bp_num));
        if (!bp) {
            g_warning ("Asked to remove an invalid breakpoint.");
            return;
        }
        
        remove_breakpoint (bs, bp);
        CORBA_free (bp);
    }
}

static GNOME_Development_Breakpoint *
impl_get_breakpoint_info (PortableServer_Servant servant,
                          CORBA_long bp_num,
                          CORBA_Environment *ev)
{
    GdfBreakpointSet *bs = GDF_BREAKPOINT_SET (bonobo_object_from_servant (servant));
    GNOME_Development_Breakpoint *ret;
    GNOME_Development_Breakpoint *bp;

    GDL_TRACE_EXTRA ("%d", bp_num);

    bp = g_hash_table_lookup (bs->priv->breakpoints, GINT_TO_POINTER (bp_num));
    ret = GNOME_Development_Breakpoint__alloc ();
    breakpoint_dup (ret, bp);

    g_assert (ret->file_name);
    
    return ret;
}

static void
unload_debugger (GdfBreakpointSet *bs)
{
    disconnect_debugger_signals (bs);
    
    if (bs->priv->dbg) {
        gtk_object_unref (GTK_OBJECT (bs->priv->dbg));
        bs->priv->dbg = NULL;
    }
}

static void
breakpoint_enabled_cb (GdfDebuggerClient *dbg, int bp_num, 
                       GdfBreakpointSet *bs)
{
    GNOME_Development_Breakpoint *bp;
    
    bp = breakpoint_from_dbg (bs, bp_num);
    if (!bp) {
        return;
    }
     
    bp->enabled = TRUE;
    send_event (bs->event_source, "breakpoint_enabled",
                gdf_marshal_event_long ((long)bp->num));   

}

static void
breakpoint_disabled_cb (GdfDebuggerClient *dbg, int bp_num,
                        GdfBreakpointSet *bs)
{
    GNOME_Development_Breakpoint *bp;

    bp = breakpoint_from_dbg (bs, bp_num);
    if (!bp) {
        return;
    }
     
    bp->enabled = FALSE;
    send_event (bs->event_source, "breakpoint_disabled",
                gdf_marshal_event_long ((long)bp->num));
}

static void
breakpoint_deleted_cb (GdfDebuggerClient *dbg, int bp_num,
                       GdfBreakpointSet *bs)
{
    GNOME_Development_Breakpoint *bp;

    bp = breakpoint_from_dbg (bs, bp_num);
    if (!bp) {
        g_warning ("Asked to remove an invalid breakpoint.");
        return;
    }

    remove_breakpoint (bs, bp);
}

static void
connect_debugger_signals (GdfBreakpointSet *bs)
{
#if 0
    gtk_signal_connect (GTK_OBJECT (bs->priv->dbg), "breakpoint_set",
                        GTK_SIGNAL_FUNC (breakpoint_enabled_cb),
                        bs);
#endif
    gtk_signal_connect (GTK_OBJECT (bs->priv->dbg), "breakpoint_enabled",
                        GTK_SIGNAL_FUNC (breakpoint_enabled_cb),
                        bs);
    gtk_signal_connect (GTK_OBJECT (bs->priv->dbg), "breakpoint_disabled",
                        GTK_SIGNAL_FUNC (breakpoint_disabled_cb),
                        bs);
    gtk_signal_connect (GTK_OBJECT (bs->priv->dbg), "breakpoint_deleted",
                        GTK_SIGNAL_FUNC (breakpoint_deleted_cb),
                        bs);
}

static void
disconnect_debugger_signals (GdfBreakpointSet *bs)
{
    /* FIXME: Implement this */
}

static void
set_breakpoint_on_debugger (GdfBreakpointSet *bs, 
                            GNOME_Development_Breakpoint *bp)
{
    long bp_num;
    GdfDebuggerClientResult res;
    
    if (strcmp (bp->function, "???")) {
        /* FIXME: bp->condition doesn't exist, so we don't store it here.
         * but it should exist.  I don't know why it doesn't.  I must 
         * have been really stupid. So I'm using "" here.*/
        res = gdf_debugger_client_set_breakpoint_function (bs->priv->dbg,
                                                           bp->file_name,
                                                           bp->function,
                                                           NULL,
                                                           &bp_num);
    } else {
        /* FIXME: See above */
        GDL_TRACE_EXTRA ("filename: %s, line num: %d", 
                         bp->file_name, bp->line_num);
        res = gdf_debugger_client_set_breakpoint (bs->priv->dbg,
                                                  bp->file_name,
                                                  bp->line_num,
                                                  NULL,
                                                  &bp_num);
    }
    
    if (res == GDF_DEBUGGER_CLIENT_OK && bp_num != -1) {
        GNOME_Development_Breakpoint *dbg_bp;
        res = gdf_debugger_client_get_breakpoint_info (bs->priv->dbg,
                                                       bp_num,
                                                       &dbg_bp);
        if (res == GDF_DEBUGGER_CLIENT_OK) {
            long old_num = bp->num;
            gboolean was_enabled = bp->enabled;

            map_bp_numbers (bs, old_num, bp_num);

            breakpoint_clear (bp);
            breakpoint_dup (bp, dbg_bp);
            bp->num = old_num;
            CORBA_free (dbg_bp);

            if (!was_enabled) {
                gdf_debugger_client_disable_breakpoint (bs->priv->dbg,
                                                        bp_num);
            }
            
        }
    } else {
        disable_breakpoint (bs, bp);
    }
}

static void
set_breakpoints_on_debugger (GdfBreakpointSet *bs)
{
    GList *i;
    for (i = bs->priv->breakpoint_list; i != NULL; i = i->next) {
        set_breakpoint_on_debugger (bs, i->data);
    }
}

static void
impl_set_debugger (PortableServer_Servant servant,
                   GNOME_Development_Debugger dbg,
                   CORBA_Environment *ev)
{
    GdfBreakpointSet *bs = GDF_BREAKPOINT_SET (bonobo_object_from_servant (servant));
    
    unload_debugger (bs);

    if (!CORBA_Object_is_nil (dbg, ev)) {
        bs->priv->dbg = gdf_debugger_client_new_from_corba (dbg);
        gtk_object_ref (GTK_OBJECT (bs->priv->dbg));
        gtk_object_sink (GTK_OBJECT (bs->priv->dbg));
        
        connect_debugger_signals (bs);        
        set_breakpoints_on_debugger (bs);

    }
}

static void 
gdf_breakpoint_set_destroy (GtkObject *object)
{
    GdfBreakpointSet *bs = GDF_BREAKPOINT_SET (object);
    g_hash_table_destroy (bs->priv->breakpoints);
    g_list_free (bs->priv->breakpoint_list);
    g_free (bs->priv);
    parent_class->destroy (object);
}

static void
gdf_breakpoint_set_class_init (GdfBreakpointSetClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *)klass;
	POA_GNOME_Development_BreakpointSet__epv *epv = &klass->epv;
	parent_class = gtk_type_class (PARENT_TYPE);
	object_class->destroy = gdf_breakpoint_set_destroy;

    epv->getBreakpoints = impl_get_breakpoints;
    epv->setBreakpoint = impl_set_breakpoint;
    epv->setBreakpointFunction = impl_set_breakpoint_function;
    epv->setWatchpoint = impl_set_watchpoint;
    epv->enableBreakpoint = impl_enable_breakpoint;
    epv->disableBreakpoint = impl_disable_breakpoint;
    epv->deleteBreakpoint = impl_delete_breakpoint;
    epv->getBreakpointInfo = impl_get_breakpoint_info;
    epv->setDebugger = impl_set_debugger;
}

static void 
gdf_breakpoint_set_init (GdfBreakpointSet *bs)
{
	bs->priv = g_new0 (GdfBreakpointSetPriv, 1);
    bs->priv->breakpoints = g_hash_table_new (g_direct_hash, g_direct_equal);
    bs->priv->next_bp = 0;
    bs->priv->breakpoints = g_hash_table_new (g_direct_hash, g_direct_equal);
}

GtkType
gdf_breakpoint_set_get_gtk_type (void)
{
	static GtkType type = 0;
	
	if (!type) {
		GtkTypeInfo info = {
			"GdfBreakpointManager",
			sizeof (GdfBreakpointSet),
			sizeof (GdfBreakpointSetClass),
			(GtkClassInitFunc) gdf_breakpoint_set_class_init,
			(GtkObjectInitFunc) gdf_breakpoint_set_init,
			NULL, NULL, 
			(GtkClassInitFunc) NULL
		};
		
		type = bonobo_x_type_unique (PARENT_TYPE,
					     POA_GNOME_Development_BreakpointSet__init,
					     NULL, GTK_STRUCT_OFFSET (GdfBreakpointSetClass, epv), &info);
	}
	return type;
}

			

