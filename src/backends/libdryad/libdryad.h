/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * Main header file for libdryad, a library used to communicate with gdb 
 * 
 * Author : Dave Camp <campd@oit.edu>
 */

#ifndef __LIBDRYAD_H__
#define __LIBDRYAD_H__

/* define this to trace the communication with gdb on stderr */
#define TRACE_GDB

#include <glib.h>

/* Typedefs */
typedef struct _GdbInstance GdbInstance;
typedef struct _GdbCommand GdbCommand;
typedef struct _GdbSignal GdbSignal;
typedef struct _GdbBreakpoint GdbBreakpoint;
typedef struct _GdbValue GdbValue;
typedef struct _GdbDisplay GdbDisplay;
typedef struct _GdbRegister GdbRegister;
typedef gboolean (*GdbResponseHandler) (GdbInstance *inst, gpointer data);

/* Headers for the rest of libdryad */
#include "gdb-symbols.h"
#include "gdb-marshaller.h"
#include "gdb-instance.h"
#include "gdb-communication.h"
#include "gdb-signal.h"
#include "gdb-files.h"
#include "gdb-breakpoints.h"
#include "gdb-execution.h"
#include "gdb-info.h"
#include "gdb-stack.h"

#endif
