/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/* 
 * Creates processes and maps IO to files.
 * 
 * Author : Dave Camp <campd@oit.edu>
 */

#ifndef __PROCESS_H__
#define __PROCESS_H__

#include <glib.h>
#include <stdio.h>
#include <sys/types.h>

typedef struct {
    pid_t child_pid;

    int to_child[2];        /* pipes */
    int to_parent[2];
    int to_parent_error[2];

    FILE *inputfp;
    FILE *outputfp;
    FILE *errorfp;
} ChildProcess;

ChildProcess *
create_child_process (gchar * binfilename);

void
destroy_child_process (ChildProcess * proc);

#endif
