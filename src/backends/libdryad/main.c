/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

#include <config.h>
#include <gdl/gdl.h>
#include "gdf-libdryad-debugger.h"
#include <gnome.h>
#include <bonobo.h>
#include <liboaf/liboaf.h>

int
load_fn (BonoboPersistFile *pf, 
	 const CORBA_char *filename,
	 CORBA_Environment *ev,
	 void *closure)
{
    CORBA_Environment new_ev;
    GdfLibdryadDebugger *dbg = GDF_LIBDRYAD_DEBUGGER (closure);

    CORBA_exception_init (&new_ev);    
    GNOME_Development_Debugger_loadBinary (BONOBO_OBJREF (dbg), 
					   filename, &new_ev);
    
    CORBA_exception_free (&new_ev);

    return 0;
}

static BonoboObject *
debugger_factory (BonoboGenericFactory *fact,
		  void *closure)
{
    GdfLibdryadDebugger *dbg;
    BonoboPersistFile *pf;

    GDL_TRACE ();
   
    dbg = gdf_libdryad_debugger_new ();
    pf = bonobo_persist_file_new (load_fn, NULL, dbg);

    bonobo_object_add_interface (BONOBO_OBJECT (dbg), BONOBO_OBJECT (pf));

    return BONOBO_OBJECT (dbg);
}

BONOBO_OAF_FACTORY ("OAFIID:GNOME_Development_Debugger_ParseGdbFactory",
		    "gnome-debug GDB-based backend factory.",
		    VERSION, debugger_factory, NULL);
