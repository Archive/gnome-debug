/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

#ifndef __GDF_LIBDRYAD_DEBUGGER_H__
#define __GDF_LIBDRYAD_DEBUGGER_H__

typedef struct _GdfLibdryadDebugger GdfLibdryadDebugger;

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-event-source.h>
#include "../../lib/gdf.h"
#include "libdryad.h"
#include "gdf-libdryad-symbol-set.h"

BEGIN_GNOME_DECLS

#define GDF_LIBDRYAD_DEBUGGER_TYPE (gdf_libdryad_debugger_get_type ())
#define GDF_LIBDRYAD_DEBUGGER(o) (GTK_CHECK_CAST ((o), GDF_LIBDRYAD_DEBUGGER_TYPE, GdfLibdryadDebugger))
#define GDF_LIBDRYAD_DEBUGGER_CLASS(k) (GTK_CHECK_CLASS_CAST ((k), GDF_LIBDRYAD_DEBUGGER_CLASS_TYPE, GdfLibdryadDebuggerClass))
#define GNOME_Development_IS_LIBDRYAD_DEBUGGER(o) (GTK_CHECK_TYPE ((o), GDF_LIBDRYAD_DEBUGGER_TYPE))
#define GNOME_Development_IS_LIBDRYAD_DEBUGGER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_LIBDRYAD_DEBUGGER_TYPE))

struct _GdfLibdryadDebugger {
    BonoboObject base;

    GdbInstance *gdb_inst;
    BonoboEventSource *event_source;
    GNOME_Development_Debugger_StateFlag state;

    gchar *tty_name;
    gint pending_exit;
    gint error_condition;
    GList *event_queue;
    GList *stopped_handlers;

    GdfLibdryadSymbolSet *locals;
    GList *symsets;

    int frame_changed;
};

typedef struct {
    BonoboObjectClass parent_class;
} GdfLibdryadDebuggerClass;

GdfLibdryadDebugger *gdf_libdryad_debugger_new (void);
GtkType gdf_libdryad_debugger_get_type (void);

void gdf_libdryad_debugger_update_symsets (GdfLibdryadDebugger *dbg, 
					   gboolean include_locals);

void send_event (BonoboEventSource *source, const char *name, 
		 CORBA_any *data);

void queue_event (BonoboEventSource *source, const char *name, 
		  CORBA_any *data);

END_GNOME_DECLS

#endif

