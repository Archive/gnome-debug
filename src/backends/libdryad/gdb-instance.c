/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * Generic gdb stuff.
 *
 * Author : Dave Camp <campd@oit.edu>
 */

#include <glib.h>
#include <stdlib.h>
#include <string.h>
#include "process.h"
#include "libdryad.h"

static gboolean start_gdb (GdbInstance *inst);
static void get_gdb_ver (GdbInstance *inst);

/* copied from e-util */
char *
e_strdup_strip(char *string)
{
    int i;
    int length = 0;
    int initial = 0;
    for ( i = 0; string[i]; i++ ) {
	if (initial == i && isspace(string[i])) {
	    initial ++;
	}
	if (!isspace(string[i])) {
	    length = i - initial + 1;
	}
    }
    return g_strndup(string + initial, length);
}

/* Creates an instance of gdb. Returns a malloced GdbInstance struct. */
GdbInstance *
gdb_create_instance (void)
{
    GdbInstance *inst;

    inst = g_malloc (sizeof (GdbInstance));
        
    if (inst == NULL)
	return NULL;

    if (start_gdb (inst))
	return inst;
    else
	return NULL;
};

/* Destroys a gdb_instance. */
void
gdb_destroy_instance (GdbInstance *inst)
{
    gdb_signal_emit (inst,
		     "gdb-exited");

    /* Destroy the GDB instance */
    if (inst->is_attached) {
	gdb_detach (inst);
    }

    gdb_stop_listener (inst);
    gdb_clear_command_queue (inst);
    gdb_breakpoints_destroy (inst);
    gdb_wait_for_prompt (inst);
    destroy_child_process (inst->proc);

    g_free (inst);
}

/* Send an error message to the user. */
void
gdb_emit_error (GdbInstance *inst, gchar *error_msg)
{
    gdb_signal_emit (inst,
		     "error",
		     error_msg);
}

gboolean
gdb_set_working_directory (GdbInstance *inst, const char *dirname)
{
    char *cmd_str;
    char *line;
    
    cmd_str = g_strdup_printf ("cd %s", dirname);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    line = gdb_read_line (inst);
    while (line[0] == '\0') {
	line = gdb_read_line (inst);
    }
    if (!strcmp (line, "\32\32error-begin")) {
	return FALSE;
    } else {
	return TRUE;
    }
}

/* private */
gboolean
start_gdb (GdbInstance *inst)
{
    inst->proc = create_child_process ("gdb --annotate=2");

    if (inst->proc == NULL) {
	g_free (inst);
	return FALSE;
    }
    
    get_gdb_ver (inst);

    gdb_signal_init (inst);

    inst->first_command = NULL;
    inst->last_command = NULL;
    inst->running_command = NULL;

    inst->breakpoints = NULL;

    inst->display_array = NULL;
    inst->display_array_size = 0;
    inst->num_displays = 0;

    inst->file_loaded = FALSE;
    inst->is_running = FALSE;
    inst->is_executing = FALSE;
    inst->is_attached = FALSE;
    inst->corefile_loaded = FALSE;
    inst->at_prompt = FALSE;

    /* Setup the GDB environtment */

    if (inst->gdb_ver > 4.18) {
	fprintf (inst->proc->outputfp, "set prompt \\n\\32\\32pre-prompt\\n(gdb)\\n\\32\\32prompt\\n\n");
	fflush (inst->proc->outputfp);
    }
    
    gdb_wait_for_prompt (inst);

    gdb_start_listener (inst);
    gdb_execute_command (inst, "set height 0"); /* no "prompt-for-continue" */
    
    /* this drastically reduces the amount of worthless info that passes
     * through gdb :) */
    gdb_execute_command (inst, "set print elements 1");
    

    return TRUE;
}

void 
get_gdb_ver (GdbInstance *inst)
{
    char *line;
    
    line = gdb_read_line (inst);
    g_assert (!strncmp (line, "GNU gdb ", strlen ("GNU gdb ")));
    
    /* This isn't 100% perfect when dealing with cvs versions of gdb, but 
     * it will work in most cases */
    inst->gdb_ver = atof (line + strlen ("GNU gdb "));
}
