/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * Routines for handling communication with gdb
 *
 * Author : Dave Camp <campd@oit.edu>
 */

#ifndef __GDB_COMMUNICATION_H__
#define __GDB_COMMUNICATION_H__

#include <glib.h>

typedef void (*GdbDestroyNotifyFunc) (gpointer *data);

/* A gdb command */
struct _GdbCommand {
    gchar *command;
    GdbResponseHandler handler;
    GdbDestroyNotifyFunc destroy_notify;
    gpointer data;
    GdbCommand *next;
};

void gdb_start_listener (GdbInstance *inst);
void gdb_stop_listener (GdbInstance *inst);
void gdb_execute_command (GdbInstance *inst, char *command);
void gdb_post_command (GdbInstance *inst,
                       gchar *command,
                       GdbResponseHandler handler,
                       gpointer data);
void gdb_post_command_full (GdbInstance *inst,
                            gchar *command,
                            GdbResponseHandler handler,
                            gpointer data,
                            GdbDestroyNotifyFunc destroy_notify);
void gdb_clear_command_queue (GdbInstance *inst);
gchar *gdb_read_line (GdbInstance *inst);
void gdb_read_and_emit_error (GdbInstance *inst);
void gdb_wait_for_prompt (GdbInstance *inst);

#endif
