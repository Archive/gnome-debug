/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * Execution of the target. 
 *
 * Author : Dave Camp <campd@oit.edu>
 */

#include <glib.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include "libdryad.h"

static void interrupt_process (GdbInstance *inst);
static gboolean execution_handler (GdbInstance *inst, gpointer data);
static void execution_starting (GdbInstance *inst);
static void breakpoint_hit (GdbInstance *inst, gchar *line);
static void source_line (GdbInstance *inst, gchar *line);
static void exited (GdbInstance *inst, gchar *line);
static void signalled (GdbInstance *inst);
static void signal_handler (GdbInstance *inst);
static void stopped (GdbInstance *inst);
static void frame_begin (GdbInstance *inst, char *line);

/* Vocabulary for the day: 
 * /Running/ means "the program is between the beginning and the end"
 * /Executing/ means "the program is executing at this very moment" */
/* FIXME: Better names. */

/* public */

/* Starts program execution. */
void
gdb_run (GdbInstance *inst, const char *args)
{
    char *cmd_str;
    if (inst->is_running)
	return;

    cmd_str = g_strdup_printf ("run %s", args ? args : "");

    gdb_post_command (inst,
		      cmd_str,
		      (GdbResponseHandler) execution_handler,
		      NULL);
    g_free (cmd_str);
}

GdbAttachResult
gdb_attach (GdbInstance *inst, int pid)
{
    char *cmd_str;
    char *line;
    
    if (inst->is_running)
	return GDB_ATTACH_ALREADY_RUNNING;

    cmd_str = g_strdup_printf ("attach %d", pid);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    /* After executing an attach command, one of two annotations will come
     * down the pipe -- error-begin on error, and frames-invalid if the load
     * was successful.  If there was an error, read the error and return.
     * If frames-invalid is encountered, enter the execution loop (we can do
     * the execution loop here without using the event loop because an
     * attach happens relatively quickly). */

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32frames-invalid")) {
	if (!strcmp (line, "\32\32error-begin")) {
	    line = gdb_read_line (inst);
	    line += strlen ("ptrace :");
	    if (!strncmp (line, "No such", strlen ("No such"))) {
		return GDB_ATTACH_DOESNT_EXIST;
	    } else if (strncmp (line, "Operation not", strlen ("Operation not"))) {
		return GDB_ATTACH_ACCESS_DENIED;
	    }
	    return GDB_ATTACH_MISC;
	}
	line = gdb_read_line (inst);
    }  
    inst->is_attached = TRUE;
    execution_starting (inst);
    while (!execution_handler (inst, NULL))
	;
    return GDB_ATTACH_OK;
}

void
gdb_detach (GdbInstance *inst)
{
    gdb_execute_command (inst, "detach");
    inst->is_attached = FALSE;
}

GdbCoreResult 
gdb_load_core (GdbInstance *inst, const char *filename)
{
    char *cmd_str;
    char *line;
    
    if (inst->is_running) {
	return GDB_CORE_ALREADY_RUNNING;
    }

    cmd_str = g_strdup_printf ("core %s", filename);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    /* After executing a core command, one of two annotations will come
     * down the pipe -- error-begin on error, and frames-invalid if the load
     * was successful.  If there was an error, read the error and return.
     * If frames-invalid is encountered, enter the execution loop (we can do
     * the execution loop here without using the event loop because an
     * attach happens relatively quickly). */

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32frames-invalid")) {
	if (!strcmp (line, "\32\32error-begin")) {
	    line = gdb_read_line (inst);
	    if (strstr (line, "No such")) {
		return GDB_CORE_DOESNT_EXIST;
	    } else if (strstr (line, "is not")) {
		return GDB_CORE_INVALID_COREFILE;
	    } else if (strstr (line, "Permission denied")) {
		return GDB_CORE_ACCESS_DENIED;
	    }
	    return GDB_CORE_MISC;
	}
	line = gdb_read_line (inst);
    }  
    inst->corefile_loaded = TRUE;
    execution_starting (inst);
    while (!execution_handler (inst, NULL))
	;

    return GDB_CORE_OK;
}

void
gdb_unload_core (GdbInstance *inst) 
{
    g_assert (inst->corefile_loaded);
    
    gdb_execute_command (inst, "core");
}

/* Continues a *running*, but not *executing*, program. */
void
gdb_continue (GdbInstance *inst)
{
    if (!inst->is_running)
	return;
    if (inst->is_executing)
	return;
    gdb_post_command (inst,
		      "continue",
		      (GdbResponseHandler) execution_handler,
		      NULL);
}

/* Perform a "next" gdb command */
void
gdb_step_over (GdbInstance *inst)
{
    if (!inst->is_running)
	return;
    if (inst->is_executing)
	return;
    gdb_post_command (inst,
		      "next",
		      (GdbResponseHandler) execution_handler,
		      NULL);
}

/* Perform a gdb "step" command */
void
gdb_step_into (GdbInstance *inst)
{
    if (!inst->is_running)
	return;
    if (inst->is_executing)
	return;
    gdb_post_command (inst,
		      "step",
		      (GdbResponseHandler) execution_handler,
		      NULL);
}

/* perform a gdb "finish" command */
void
gdb_step_out (GdbInstance *inst)
{
    if (!inst->is_running)
	return;
    if (inst->is_executing)
	return;
    gdb_post_command (inst,
		      "finish",
		      (GdbResponseHandler) execution_handler,
		      NULL);
}

/* Stops execution and running of a currently running program. */
void
gdb_stop (GdbInstance *inst)
{
    if (!inst->is_running)
	return;
        
    if (inst->is_executing) {
	interrupt_process (inst);
	return;
    }
    gdb_execute_command (inst, "kill");

    inst->is_running = FALSE;
    inst->is_executing = FALSE;
    gdb_signal_emit (inst, "execution-exited", 0);
    gdb_signal_emit (inst, "execution-stopped");
}


/* Set the output tty to the given tty */
void
gdb_tty_set (GdbInstance *inst, const char *tty_name)
{
    gchar *cmd_str;
        
    cmd_str = g_strdup_printf ("tty %s", tty_name);
    gdb_post_command (inst, cmd_str, NULL, NULL);
    g_free (cmd_str);
}

/* private */

/* Send a SIGINT to an executing process */
/* FIXME: Make this *WORK* */
void
interrupt_process (GdbInstance *inst)
{
    g_assert (inst->is_executing);

    g_warning ("Interrupting processess is not yet implemented.");
}

gboolean
execution_handler (GdbInstance *inst, gpointer data)
{
    gboolean done_processing = FALSE;
    gchar *line = gdb_read_line (inst);

    if (line[0] == 0x1A && line[1] == 0x1A) {
	line += 2;      /* skip the two characters */
	if (strcmp (line, "starting") == 0) {
	    execution_starting (inst);
	} else if (strncmp (line, "breakpoint ",
			    strlen ("breakpoint ")) == 0) {
	    breakpoint_hit (inst, line);
	} else if (strncmp (line, "frame-begin",
			    strlen ("frame-begin")) == 0) {
	    frame_begin (inst, line);
	} else if (strncmp (line, "source", strlen ("source")) == 0) {
	    source_line (inst, line);
	} else if (strncmp (line, "exited", strlen ("exited")) == 0) {
	    exited (inst, line);
	} else if (strncmp (line, "signalled",
			    strlen ("signalled")) == 0) {
	    signalled (inst);
	} else if (strncmp (line, "signal", strlen ("signal")) == 0) {
	    signal_handler (inst);
	} else if (strncmp (line, "stopped",
			    strlen ("stopped")) == 0) {
	    stopped (inst);
	    done_processing = TRUE;
	} else if (strcmp (line, "pre-prompt") == 0) {
	    stopped (inst);
	    done_processing = TRUE;
	}
    }
    return done_processing;
}

void
execution_starting (GdbInstance *inst)
{
    if (!inst->is_running) {
	inst->is_running = TRUE;
	gdb_signal_emit (inst,
			 "execution-running");
    }
    inst->is_executing = TRUE;
    gdb_signal_emit (inst, "execution-starting");
}

void
breakpoint_hit (GdbInstance *inst, gchar *line)
{
    gint bp_num;

    inst->is_executing = FALSE;
    sscanf (line, "breakpoint %d", &bp_num);
    gdb_signal_emit (inst,
		     "execution-breakpoint-hit",
		     bp_num);
}

void
source_line (GdbInstance *inst, gchar *line)
{
    gchar *source_path;
    gchar *ptr_ch;
    gint line_num;

    source_path = line + strlen ("source ");
    ptr_ch = strchr (source_path, ':');
    sscanf (ptr_ch, ":%d:", &line_num);
    *ptr_ch = '\0';

    gdb_signal_emit (inst,
		     "execution-source-line",
		     source_path,
		     line_num);
}

void
exited (GdbInstance *inst, gchar *line)
{
    gint exit_code;

    inst->is_running = FALSE;
    inst->is_executing = FALSE;
    sscanf (line, "exited %d", &exit_code);
    gdb_signal_emit (inst,
		     "execution-exited",
		     exit_code);
}

/* Program terminated on signal. */
void
signalled (GdbInstance * inst)
{
    gchar *line;
    gchar *signal_buf = NULL;

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32signal-string-end") != 0) {
	if (strcmp (line, "\32\32signal-name") == 0) {
	    line = gdb_read_line (inst);
	    signal_buf = g_strdup_printf ("%s", line);
	}
	line = gdb_read_line (inst);
    }
    
    gdb_signal_emit (inst,
		     "execution-exited-signal",
		     signal_buf);
    g_free (signal_buf);
}

void
signal_handler (GdbInstance *inst)
{
    gchar *line;
    gchar *signal_buf = NULL;

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32signal-string-end") != 0) {
	if (strcmp (line, "\32\32signal-name") == 0) {
	    line = gdb_read_line (inst);
	    signal_buf = g_strdup_printf ("%s", line);
	}
	line = gdb_read_line (inst);
    }

    gdb_signal_emit (inst,
		     "execution-signal-received",
		     signal_buf);
    g_free (signal_buf);
}

static void
stopped (GdbInstance *inst)
{
    inst->is_executing = FALSE;
    gdb_signal_emit (inst,
		     "execution-stopped");
}

void
frame_begin (GdbInstance *inst, char *line)
{
    int id;
    
    sscanf (line + strlen ("frame-begin"), "%d", &id);
    
    gdb_signal_emit (inst,
		     "execution-frame-change",
		     id);
}
