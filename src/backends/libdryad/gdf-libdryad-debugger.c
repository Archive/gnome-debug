/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Implements a GDF Debugger based on Libdryad.
 * 
 * Copyright 1999-2000 Dave Camp <campd@oit.edu>, 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

/** 
 * IMPORTANT NOTE: This debugger is ugly, and doesn't really work well.  It
 * is not intended to be used for long; a more permanent solution will be 
 * coming at some point.
 **/

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include <bonobo/bonobo-event-source.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <gdl/gdl.h>

#include "gdf-libdryad-debugger.h"
#include "libdryad.h"
#include "../../lib/gnome-debug.h"


static CORBA_Object create_libdryad_debugger (BonoboObject *object);
static void gdf_libdryad_debugger_destroy (GtkObject *object);
static void gdf_libdryad_debugger_class_init (GdfLibdryadDebuggerClass *class);
static void gdf_libdryad_debugger_init (BonoboObject *object);
static void connect_signals (GdfLibdryadDebugger *dbg);
static void process_frame_change (GdfLibdryadDebugger *dbg);
static void new_locals (GdfLibdryadDebugger *dbg);

static BonoboObjectClass *parent_class;
static POA_GNOME_Development_Debugger__epv libdryad_debugger_epv;
static POA_GNOME_Development_Debugger__vepv libdryad_debugger_vepv;

#define CORBA_boolean__alloc() (CORBA_boolean*) CORBA_octet_allocbuf (sizeof (CORBA_boolean))

static inline GdfLibdryadDebugger*
libdryad_debugger_from_servant (PortableServer_Servant servant)
{
    return GDF_LIBDRYAD_DEBUGGER (bonobo_object_from_servant (servant));
}

/* public routines */

GdfLibdryadDebugger *
gdf_libdryad_debugger_new (void)
{
    GdfLibdryadDebugger *debugger;
    GNOME_Development_Debugger objref;

    debugger = 
	gtk_type_new (gdf_libdryad_debugger_get_type ());
    objref = 
	create_libdryad_debugger (BONOBO_OBJECT (debugger));
    if (objref == CORBA_OBJECT_NIL) {
	gtk_object_destroy (GTK_OBJECT (debugger));
	return NULL;
    }
    
    bonobo_object_construct (BONOBO_OBJECT (debugger),
			     objref);
    
    debugger->gdb_inst = NULL;
    debugger->state = 0;
    debugger->event_source = bonobo_event_source_new ();

    debugger->pending_exit = -1;

    debugger->stopped_handlers = NULL;
    debugger->locals = gdf_libdryad_symbol_set_new (debugger);
    debugger->symsets = NULL;
    
    debugger->frame_changed = -1;
    
    return debugger;
}

GtkType 
gdf_libdryad_debugger_get_type (void)
{
    static GtkType type = 0;
        
    if (!type) {
	GtkTypeInfo info = {
	    "IDL:GDF/Debugger:1.0",
	    sizeof (GdfLibdryadDebugger),
	    sizeof (GdfLibdryadDebuggerClass),
	    (GtkClassInitFunc) gdf_libdryad_debugger_class_init,
	    (GtkObjectInitFunc) gdf_libdryad_debugger_init,
	    NULL,
	    NULL,
	    (GtkClassInitFunc) NULL
	};

	type = gtk_type_unique (bonobo_object_get_type (), &info);
    }

    return type;
}

void 
send_event (BonoboEventSource *source, const char *name, CORBA_any *data)
{
    GDL_TRACE_EXTRA ("emitting %s", name);
    bonobo_event_source_notify_listeners (source, name, data, NULL);
    CORBA_free (data);
}
	
struct QueuedEvent
{
    char *name;
    CORBA_any *data;
};

void 
queue_event (BonoboEventSource *source, const char *name, CORBA_any *data)
{
    GList *event_queue;
    
    struct QueuedEvent *evt = g_new0 (struct QueuedEvent, 1);

    evt->name = g_strdup (name);
    evt->data = data;

    event_queue = gtk_object_get_data (GTK_OBJECT (source), "event_queue");
    event_queue = g_list_prepend (event_queue, evt);
    gtk_object_set_data (GTK_OBJECT (source), "event_queue", event_queue);
}


static void
process_event_queue (BonoboEventSource *source)
{
    GList *i;
    GList *event_queue;

    event_queue = gtk_object_get_data (GTK_OBJECT (source), "event_queue");
    event_queue = g_list_reverse (event_queue);
    i = event_queue;

    while (i) {
	struct QueuedEvent *evt = i->data;
	send_event (source, evt->name, evt->data);
	g_free (evt->name);
	i = g_list_next (i);
    }
    g_list_free (event_queue);
    gtk_object_set_data (GTK_OBJECT (source), "event_queue", NULL);

}


/* private routines */
CORBA_Object
create_libdryad_debugger (BonoboObject *object) 
{
    POA_GNOME_Development_Debugger *servant;
    CORBA_Environment ev;
    CORBA_exception_init (&ev);
    
    servant = 
        (POA_GNOME_Development_Debugger*)g_new0(BonoboObjectServant, 1);
    servant->vepv = &libdryad_debugger_vepv;
    
    POA_GNOME_Development_Debugger__init((PortableServer_Servant) servant, 
			    &ev);
    if (ev._major != CORBA_NO_EXCEPTION) {
        g_free (servant);
        CORBA_exception_free (&ev);
        return CORBA_OBJECT_NIL;
    }
    CORBA_exception_free (&ev);
    return bonobo_object_activate_servant (object, servant);
}

static void
gdf_libdryad_debugger_destroy (GtkObject *object) 
{
    GList *ss;
    GdfLibdryadDebugger *dbg = GDF_LIBDRYAD_DEBUGGER (object);

    if (dbg->gdb_inst)
	gdb_destroy_instance (dbg->gdb_inst);

    if (dbg->event_source) {
	bonobo_object_unref (BONOBO_OBJECT (dbg->event_source));
    }

    if (dbg->locals) {
	bonobo_object_unref (BONOBO_OBJECT (dbg->locals));
    }
    
    for (ss = dbg->symsets; ss != NULL; ss = g_list_next (ss)) {
	bonobo_object_unref (BONOBO_OBJECT (ss->data));
    }
}

static GNOME_Development_Debugger_StateFlag
impl_get_state (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    return dbg->state;
}

static void
impl_load_binary (PortableServer_Servant servant, 
		  const CORBA_char *file_name,
		  CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    gboolean success;
    
    if (dbg->gdb_inst) {
	if (dbg->gdb_inst->file_loaded) 
	    gdb_unload_file (dbg->gdb_inst);
	
	dbg->state &= ~(GNOME_Development_Debugger_BINARY_LOADED);
	
	send_event (dbg->event_source, "program_unloaded", 
		    gdf_marshal_event_none ());

	gdb_destroy_instance (dbg->gdb_inst);
    }
     
    dbg->gdb_inst = gdb_create_instance ();
    connect_signals (dbg);

    dbg->error_condition = 0;

    success = gdb_load_file (dbg->gdb_inst, file_name);

    if (success) {
	dbg->state |= GNOME_Development_Debugger_BINARY_LOADED;	

	send_event (dbg->event_source, "program_loaded", 
		    gdf_marshal_event_none ());
	if (dbg->tty_name) {
	    gdb_tty_set (dbg->gdb_inst, dbg->tty_name);
	}
    } else {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidBinary, NULL);
    }
}

static void
impl_unload_binary (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    
    if (dbg->gdb_inst) {
	if (dbg->gdb_inst->file_loaded) 
	    gdb_unload_file (dbg->gdb_inst);

	dbg->state &= ~(GNOME_Development_Debugger_BINARY_LOADED);
	
	send_event (dbg->event_source, "program_unloaded", 
		    gdf_marshal_event_none ());
	gdb_destroy_instance (dbg->gdb_inst);
    }
    dbg->gdb_inst = NULL;
}

static void
impl_attach (PortableServer_Servant servant, 
	     CORBA_short pid,
	     CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    GdbAttachResult res;

    res = gdb_attach (dbg->gdb_inst, (int)pid);

    switch (res) {
    case GDB_ATTACH_OK :
	dbg->state |= GNOME_Development_Debugger_ATTACHED;
	send_event (dbg->event_source, "attached", gdf_marshal_event_none ());
	break;
    case GDB_ATTACH_ALREADY_RUNNING :
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	break;
    case GDB_ATTACH_DOESNT_EXIST :
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_DoesntExist, NULL);
	break;
    case GDB_ATTACH_ACCESS_DENIED :
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_SecurityException, NULL);
	break;
    case GDB_ATTACH_MISC :
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_UnknownError, NULL);
	break;
    }
}

static void
impl_detach (PortableServer_Servant servant,
	     CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    if (!(dbg->state & GNOME_Development_Debugger_ATTACHED)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }
    dbg->state &= ~GNOME_Development_Debugger_ATTACHED;
    dbg->state &= ~GNOME_Development_Debugger_TARGET_EXISTS;
    
    gdb_detach (dbg->gdb_inst);
    new_locals (dbg);
    gdf_libdryad_debugger_update_symsets (dbg, TRUE);
    send_event (dbg->event_source, "detached", gdf_marshal_event_none ());
    send_event (dbg->event_source, "exited", gdf_marshal_event_long (-1));
}

static void
impl_execute (PortableServer_Servant servant, 
	      const CORBA_char *args,
	      CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    g_return_if_fail (dbg->gdb_inst != NULL);

    if ((dbg->state & GNOME_Development_Debugger_TARGET_EXISTS) 
	|| (dbg->state & GNOME_Development_Debugger_COREFILE)) {
	
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }
    
    gdb_run (dbg->gdb_inst, args);
}

static void
impl_load_corefile (PortableServer_Servant servant, 
		    const CORBA_char *corefile_name,
		    CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    GdbCoreResult res;

    if ((dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)
	|| (dbg->state & GNOME_Development_Debugger_COREFILE)) {
	
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }

    res = gdb_load_core (dbg->gdb_inst, corefile_name);

    switch (res) {
    case GDB_CORE_OK :
	dbg->state |= GNOME_Development_Debugger_COREFILE;
	send_event (dbg->event_source, "corefile_loaded", 
		    gdf_marshal_event_none ());
	break;
    case GDB_CORE_ALREADY_RUNNING :
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	break;
    case GDB_CORE_DOESNT_EXIST :
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_DoesntExist, NULL);
	break;
    case GDB_CORE_ACCESS_DENIED :
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_SecurityException, NULL);
	break;
    case GDB_CORE_INVALID_COREFILE :
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
			     ex_GNOME_Development_Debugger_InvalidCorefile, NULL);
	break;
    case GDB_CORE_MISC :
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_UnknownError, NULL);
	break;
    }
}

static void
impl_unload_corefile (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    if (!(dbg->state & GNOME_Development_Debugger_COREFILE)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }

    gdb_unload_core (dbg->gdb_inst);
    dbg->state &= ~GNOME_Development_Debugger_COREFILE;
    send_event (dbg->event_source, "corefile_unloaded", 
		gdf_marshal_event_none ());
}

static GNOME_Development_SourceFileList *
impl_get_sources (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    GNOME_Development_SourceFileList *list;
    GList *sources;
    GList *i;
    int num;

    if (!(dbg->state & GNOME_Development_Debugger_BINARY_LOADED)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return GNOME_Development_SourceFileList__alloc (); 
    }

    sources = gdb_list_sources (dbg->gdb_inst);    

    list = GNOME_Development_SourceFileList__alloc ();
    list->_length = list->_maximum = g_list_length (sources);
    list->_buffer = CORBA_sequence_CORBA_string_allocbuf (list->_length);
    CORBA_sequence_set_release (list, CORBA_TRUE);
    for (i = sources, num = 0; i != NULL; i = g_list_next (i), num++) {
	list->_buffer[num] = CORBA_string_dup (i->data);
	g_free (i->data);
    }
    g_list_free (sources);
    
    return list;
}

static CORBA_char *
impl_get_absolute_source_path (PortableServer_Servant servant, 
			       const CORBA_char *source_file,
			       CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    CORBA_char *retval;    

    if (!(dbg->state & GNOME_Development_Debugger_BINARY_LOADED)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return CORBA_string_dup (""); 
    }

    
    if (source_file[0] == '/')
	retval = CORBA_string_dup (source_file);
    else {
	gchar *abs_path;
	abs_path = gdb_get_full_source_path (dbg->gdb_inst, source_file);

	retval = CORBA_string_dup (abs_path);
    }

    return retval;
}

static void
breakpoint_to_corba (GNOME_Development_Breakpoint *dest,
		     const GdbBreakpoint *src) 
{
    dest->num = src->num;
    dest->type = CORBA_string_dup (src->type ? src->type : "");
    dest->enabled = (CORBA_boolean)src->enabled;
    dest->address = CORBA_string_dup (src->address ? src->address : "");
    dest->file_name =
	CORBA_string_dup (src->file_name ? src->file_name : "???");
    dest->function =
	CORBA_string_dup (src->function ? src->function : "???");
    dest->line_num = (CORBA_long)src->line_num;
}


static GNOME_Development_BreakpointList *
impl_get_breakpoints (PortableServer_Servant servant,
		      CORBA_Environment *ev)
{
     GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
     GList *breakpoints;
     GList *i;
     int n;
     GNOME_Development_BreakpointList *ret = GNOME_Development_BreakpointList__alloc ();

     if (!dbg->gdb_inst) {
	 return ret;
     }

     breakpoints = gdb_breakpoint_get_all (dbg->gdb_inst);
     n = g_list_length (breakpoints); 
     ret->_length = n;
     ret->_maximum = n;
     ret->_buffer = CORBA_sequence_GNOME_Development_Breakpoint_allocbuf (n);
     CORBA_sequence_set_release (ret, TRUE);

     n = 0;
     for (i = breakpoints; i != NULL; i = i->next) {
	 GdbBreakpoint *bp = i->data;
	 GNOME_Development_Breakpoint *corba_bp = &ret->_buffer[n++];

	 breakpoint_to_corba (corba_bp, bp);
     }
     
     g_list_free (breakpoints);

     return ret;
     
}

static CORBA_long
impl_set_breakpoint (PortableServer_Servant servant, 
		     const CORBA_char *file_name,
		     CORBA_long line_num,
		     const CORBA_char *condition,
		     CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    gchar *file;
    const GdbBreakpoint *bp;
    
    g_return_val_if_fail (dbg->gdb_inst != NULL, -1);

    if (strcmp (condition, "") == 0) 
	condition = NULL;
    
    file = strrchr (file_name, G_DIR_SEPARATOR);
    if (!file++)
	file = (char*)file_name;

    bp = gdb_breakpoint_set_linenum (dbg->gdb_inst,
				     file,
				     line_num,
				     condition);

    if (bp) {
	send_event (dbg->event_source, "breakpoint_set",
		    gdf_marshal_event_long ((long)bp->num));
	return bp->num;
    } else {
	/* FIXME: Set exception */
	return -1;
    }
}

static CORBA_long
impl_set_breakpoint_function (PortableServer_Servant servant,
			      const CORBA_char *file_name,
			      const CORBA_char *function_name,
			      const CORBA_char *condition,
			      CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    const GdbBreakpoint *bp;

    g_return_val_if_fail (dbg->gdb_inst != NULL, -1);

    if (strcmp (condition, "") == 0) 
	condition = NULL;
    if (strcmp (file_name, "") == 0) 
	file_name = NULL;

    bp = gdb_breakpoint_set_function (dbg->gdb_inst,
				      function_name,
				      condition);

    if (bp) {
	send_event (dbg->event_source, "breakpoint_set", 
		    gdf_marshal_event_long ((long)bp->num));
	return bp->num;
    } else {
	/* FIXME: Set exception */
	return -1;
    }
}

static CORBA_long
impl_set_watchpoint (PortableServer_Servant servant,
		     const CORBA_char *expr,
		     const CORBA_char *condition,
		     CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    const GdbBreakpoint *bp;

    g_return_val_if_fail (dbg->gdb_inst != NULL, -1);

    bp = gdb_breakpoint_set_watchpoint (dbg->gdb_inst, expr, 
					condition[0] ? condition : NULL);

    if (bp) {
	send_event (dbg->event_source, "breakpoint_set", 
		    gdf_marshal_event_long ((long)bp->num));
	return bp->num;
    } else {
	/* FIXME: Set exception */
	return -1;
    }
}

static void
impl_enable_breakpoint (PortableServer_Servant servant, 
			CORBA_long bp_num,
			CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    g_return_if_fail (dbg->gdb_inst != NULL);
    
    if (gdb_breakpoint_enable (dbg->gdb_inst, bp_num)) {
	send_event (dbg->event_source, "breakpoint_enabled", 
		    gdf_marshal_event_long ((long)bp_num));
    }
}

static void
impl_disable_breakpoint (PortableServer_Servant servant,
			 CORBA_long bp_num,
			 CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    
    g_return_if_fail (dbg->gdb_inst != NULL);

    if (gdb_breakpoint_disable (dbg->gdb_inst, bp_num)) {
	send_event (dbg->event_source, "breakpoint_disabled",
		    gdf_marshal_event_long ((long)bp_num));
    }
}

static void
impl_delete_breakpoint (PortableServer_Servant servant, 
			CORBA_long bp_num,
			CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    g_return_if_fail (dbg->gdb_inst != NULL);
    
    if (gdb_breakpoint_delete (dbg->gdb_inst, bp_num)) {
	send_event (dbg->event_source, "breakpoint_deleted",
		    gdf_marshal_event_long ((long)bp_num));
    }
}

static GNOME_Development_Breakpoint *
impl_get_breakpoint_info (PortableServer_Servant servant,
			  CORBA_long bp_num,
			  CORBA_Environment *ev)
{
    const GdbBreakpoint *bp;
    GNOME_Development_Breakpoint *ret;
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    bp = gdb_breakpoint_get_info (dbg->gdb_inst, bp_num);
    
    ret = GNOME_Development_Breakpoint__alloc ();
    
    if (bp == NULL) {
	/* FIXME: Set an exception */
	return ret;
    }
    

    breakpoint_to_corba (ret, bp);

    return ret;
}

static GNOME_Development_RegisterList *
impl_get_registers (PortableServer_Servant servant,
		    CORBA_boolean floating_pt,
		    CORBA_Environment *ev)
{
    GNOME_Development_RegisterList *ret;
    GList *registers;
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    int num_regs = 0;
    GList *r;
    int i = 0;

    ret = GNOME_Development_RegisterList__alloc ();
    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)
	&& !(dbg->state & GNOME_Development_Debugger_COREFILE)) {
	
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return ret;
    }

    CORBA_sequence_set_release (ret, CORBA_TRUE);

    if (dbg->gdb_inst) {
	registers = gdb_get_registers (dbg->gdb_inst, floating_pt);
	
	if (registers) {
	    num_regs = g_list_length (registers);
	    ret->_length = ret->_maximum = num_regs;
	    ret->_buffer = CORBA_sequence_GNOME_Development_Register_allocbuf (num_regs);
	    
	    i = 0;
	    for (r = registers; r != NULL; r = r->next) {
		ret->_buffer[i].name = 
		    CORBA_string_dup (((GdbRegister*)r->data)->name);
		ret->_buffer[i++].value = 
		    CORBA_string_dup (((GdbRegister*)r->data)->value);
	    }
	    
	    gdb_destroy_register_list (dbg->gdb_inst, registers);

	    return ret;
	}
    }
    
    /* Did not get registers; must not be running. */
    CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			 ex_GNOME_Development_Debugger_InvalidState, NULL);

    return ret;
}

static char *
impl_get_register_value (PortableServer_Servant servant,
			 const char *register_name,
			 CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    char *val;
    char *ret;
    ret = NULL;
    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)
	&& !(dbg->state & GNOME_Development_Debugger_COREFILE)) {
	
	ret = CORBA_string_dup ("");
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return ret;
    }
    
    val = gdb_get_register_value (dbg->gdb_inst, register_name);
    if (val) {
	ret = CORBA_string_dup (val);
	g_free (val);    
    } else {
	ret = CORBA_string_dup ("");
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
			     ex_GNOME_Development_Debugger_DoesntExist, NULL);
    }

    return ret;
}

static void
impl_set_register_value (PortableServer_Servant servant,
			 const char *register_name,
			 const char *register_value,
			 CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    
    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)
	&& !(dbg->state & GNOME_Development_Debugger_COREFILE)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }
    
    gdb_set_register_value (dbg->gdb_inst, register_name, register_value);
    return;
}

static void
impl_change_frame (PortableServer_Servant servant,
		   CORBA_long new_id,
		   CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)
	&& !(dbg->state & GNOME_Development_Debugger_COREFILE)) {
	
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }

    if (gdb_change_frame (dbg->gdb_inst, (int) new_id)) {
	process_frame_change (dbg);

	send_event (dbg->event_source, "frame_change", 
		    gdf_marshal_event_long (new_id));
    }
}

static GNOME_Development_StackFrame *
impl_get_frame (PortableServer_Servant servant, 
		CORBA_long id,
		CORBA_Environment *ev)
{
    GdbFrame *frame;
    GNOME_Development_StackFrame *gdf_frame;
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    gdf_frame = GNOME_Development_StackFrame__alloc ();

    if ((dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)
	|| (dbg->state & GNOME_Development_Debugger_COREFILE)) {
	frame = gdb_get_frame (dbg->gdb_inst, id);
	
	gdf_frame->id = frame->id;
	if (frame->function) 
	    gdf_frame->function = CORBA_string_dup (frame->function);
	else
	    gdf_frame->function = CORBA_string_dup ("");
	if (frame->file)
	    gdf_frame->location.file = CORBA_string_dup (frame->file);
	else
	    gdf_frame->location.file = CORBA_string_dup ("");
	gdf_frame->location.line = frame->line_num;
	gdf_frame->virtual_address = (GNOME_Development_MemoryAddress)frame->addr;   
	
	gdb_destroy_frame (dbg->gdb_inst, frame);
    } else {
	gdf_frame->id = -1;
	gdf_frame->function = CORBA_string_dup ("");
	gdf_frame->location.file = CORBA_string_dup ("");
	gdf_frame->location.line = -1;
	gdf_frame->virtual_address = 0;
	
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
    }
    
    return gdf_frame;
}

static GNOME_Development_Stack *
impl_get_backtrace (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    GNOME_Development_Stack *stack;
    GList *list;
    GList *i;
    int n;

    stack = GNOME_Development_Stack__alloc ();

    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)
	&& !(dbg->state & GNOME_Development_Debugger_COREFILE)) {
	
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return stack;
    }

    CORBA_sequence_set_release (stack, CORBA_TRUE);

    list = gdb_get_stack (dbg->gdb_inst);    

    stack->_length = stack->_maximum = g_list_length (list);
    stack->_buffer = CORBA_sequence_GNOME_Development_StackFrame_allocbuf (stack->_length);
    for (i = list, n = 0; i != NULL; i = g_list_next (i), n++) {	
	GdbFrame *frame = i->data;
	stack->_buffer[n].id = frame->id;
	stack->_buffer[n].function = 
	    CORBA_string_dup (frame->function ? frame->function : "");
	stack->_buffer[n].location.file = 
	    CORBA_string_dup (frame->file ? frame->file : "");
	stack->_buffer[n].location.line = frame->line_num;
	stack->_buffer[n].virtual_address = (GNOME_Development_MemoryAddress)frame->addr;
    }
    gdb_destroy_stack (dbg->gdb_inst, list);
    
    return stack;
}

static void
impl_cont (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    
    g_return_if_fail (dbg->gdb_inst != NULL);

    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }


    gdb_continue (dbg->gdb_inst);    
}

static void
impl_stop (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    g_return_if_fail (dbg->gdb_inst != NULL);

    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }

    gdb_stop (dbg->gdb_inst);
}

static void
impl_restart (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    g_return_if_fail (dbg->gdb_inst != NULL);

    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }

    gdb_stop (dbg->gdb_inst);
    gdb_run (dbg->gdb_inst, NULL);
}

static void
impl_step_over (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    g_return_if_fail (dbg->gdb_inst != NULL);

    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }

    gdb_step_over (dbg->gdb_inst);
}

static void
impl_step_into (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    g_return_if_fail (dbg->gdb_inst != NULL);

    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }

    gdb_step_into (dbg->gdb_inst);
}

static void
impl_step_out (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    g_return_if_fail (dbg->gdb_inst != NULL);

    if (!(dbg->state & GNOME_Development_Debugger_TARGET_EXISTS)) {
	CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
			     ex_GNOME_Development_Debugger_InvalidState, NULL);
	return;
    }

    gdb_step_out (dbg->gdb_inst);
}

/* Symbol watching */

void
gdf_libdryad_debugger_update_symsets (GdfLibdryadDebugger *dbg, 
				      gboolean include_locals)
{
    GList *i;
    
    if (include_locals) {
	gdf_libdryad_symbol_set_update (dbg->locals);
    }

    for (i = dbg->symsets; i != NULL; i = i->next) {
	gdf_libdryad_symbol_set_update (i->data);
    }
}

void
new_locals (GdfLibdryadDebugger *dbg)
{
    gdb_get_locals (dbg->gdb_inst, dbg->locals);
}

static GNOME_Development_SymbolSet
impl_get_locals (PortableServer_Servant servant, CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    return CORBA_Object_duplicate (bonobo_object_corba_objref (BONOBO_OBJECT (dbg->locals)), 
				   ev);
}

static GNOME_Development_SymbolSet
impl_allocate_symbol_set (PortableServer_Servant servant, 
			  CORBA_Environment *ev)
{    
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    GdfLibdryadSymbolSet *ss = gdf_libdryad_symbol_set_new (dbg);

    dbg->symsets = g_list_prepend (dbg->symsets, ss);

    return CORBA_Object_duplicate (bonobo_object_corba_objref (BONOBO_OBJECT (ss)), 
				   ev);
}

static void
impl_set_working_directory (PortableServer_Servant servant,
			    const char *dirname,
			    CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    int ret = chdir (dirname);
    if (ret != 0) {
	switch (errno) {
	case ENOENT :
	    CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
				 ex_GNOME_Development_Debugger_DoesntExist, NULL);
	    break;
	default :
	    CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				 ex_GNOME_Development_Debugger_UnknownError, NULL);
	    break;
	}
    }

    if (dbg->gdb_inst) {
	if (!gdb_set_working_directory (dbg->gdb_inst, dirname)) {
	    CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				 ex_GNOME_Development_Debugger_UnknownError, NULL);
	}
    }
}

static CORBA_Object
impl_get_event_source (PortableServer_Servant servant,
			CORBA_Environment *ev)
{
    CORBA_Object ret;
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);
    ret = bonobo_object_corba_objref (BONOBO_OBJECT (dbg->event_source));
    return CORBA_Object_duplicate (ret, ev);
}

static void
impl_set_output_tty (PortableServer_Servant servant,
		     const CORBA_char *tty_name,
		     CORBA_Environment *ev)
{
    GdfLibdryadDebugger *dbg = libdryad_debugger_from_servant (servant);

    if (dbg->tty_name)
	g_free (dbg->tty_name);
    
    dbg->tty_name = g_strdup (tty_name);
    
    if (dbg->gdb_inst)
	gdb_tty_set (dbg->gdb_inst, tty_name);
}

static void
init_libdryad_debugger_corba_class (void) 
{
    /* EPV */
    libdryad_debugger_epv.getState = impl_get_state;
    libdryad_debugger_epv.loadBinary = impl_load_binary;
    libdryad_debugger_epv.unloadBinary = impl_unload_binary;
    libdryad_debugger_epv.attach = impl_attach;
    libdryad_debugger_epv.detach = impl_detach;
    libdryad_debugger_epv.execute = impl_execute;
    libdryad_debugger_epv.loadCorefile = impl_load_corefile;
    libdryad_debugger_epv.unloadCorefile = impl_unload_corefile;
    libdryad_debugger_epv.getSources = impl_get_sources;
    libdryad_debugger_epv.getAbsoluteSourcePath = impl_get_absolute_source_path;
    libdryad_debugger_epv.getBreakpoints = impl_get_breakpoints;    
    libdryad_debugger_epv.setBreakpoint = impl_set_breakpoint;
    libdryad_debugger_epv.setBreakpointFunction = impl_set_breakpoint_function;
    libdryad_debugger_epv.setWatchpoint = impl_set_watchpoint;
    libdryad_debugger_epv.enableBreakpoint = impl_enable_breakpoint;
    libdryad_debugger_epv.disableBreakpoint = impl_disable_breakpoint;
    libdryad_debugger_epv.deleteBreakpoint = impl_delete_breakpoint;
    libdryad_debugger_epv.getBreakpointInfo = impl_get_breakpoint_info;
    libdryad_debugger_epv.getRegisters = impl_get_registers;
    libdryad_debugger_epv.getRegisterValue = impl_get_register_value;
    libdryad_debugger_epv.setRegisterValue = impl_set_register_value;
    libdryad_debugger_epv.changeFrame = impl_change_frame;    
    libdryad_debugger_epv.getFrame = impl_get_frame;
    libdryad_debugger_epv.getBacktrace = impl_get_backtrace;
    libdryad_debugger_epv.cont = impl_cont;    
    libdryad_debugger_epv.stop = impl_stop;
    libdryad_debugger_epv.restart = impl_restart;
    libdryad_debugger_epv.stepOver = impl_step_over;
    libdryad_debugger_epv.stepInto = impl_step_into;
    libdryad_debugger_epv.stepOut = impl_step_out;
    libdryad_debugger_epv.getLocals = impl_get_locals;
    libdryad_debugger_epv.allocateSymbolSet = impl_allocate_symbol_set;
    libdryad_debugger_epv.getEventSource = impl_get_event_source;
    libdryad_debugger_epv.setOutputTTY = impl_set_output_tty;
    libdryad_debugger_epv.setWorkingDirectory = impl_set_working_directory;
   
    /* VEPV */
    libdryad_debugger_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
    libdryad_debugger_vepv.GNOME_Development_Debugger_epv = &libdryad_debugger_epv;
}
        
static void
gdf_libdryad_debugger_class_init (GdfLibdryadDebuggerClass *class) 
{
    GtkObjectClass *object_class = (GtkObjectClass*) class;
    parent_class = gtk_type_class (bonobo_object_get_type ());
    
    object_class->destroy = gdf_libdryad_debugger_destroy;
    
    init_libdryad_debugger_corba_class ();
}

static void
gdf_libdryad_debugger_init (BonoboObject *object)
{
    GdfLibdryadDebugger *dbg = GDF_LIBDRYAD_DEBUGGER (object);
    
    dbg->tty_name = NULL;
}

/* Perform cleanup tasks associated with a frame change */
static void
process_frame_change (GdfLibdryadDebugger *dbg)
{
    new_locals (dbg);
    gdf_libdryad_debugger_update_symsets (dbg, FALSE);
}

static void
execution_starting_cb (GdfLibdryadDebugger *dbg)
{
    if (!dbg->gdb_inst->corefile_loaded) {
	dbg->state |= GNOME_Development_Debugger_TARGET_EXISTS;
	send_event (dbg->event_source, "started", gdf_marshal_event_none ());
    } else {
	dbg->state |= GNOME_Development_Debugger_COREFILE;
    }
}

static void
execution_running_cb (GdfLibdryadDebugger *dbg)
{
    if (!dbg->gdb_inst->corefile_loaded) {
	dbg->state |= GNOME_Development_Debugger_TARGET_EXECUTING;
	send_event (dbg->event_source, "running", gdf_marshal_event_none ());
    }
}

static void
execution_exited_cb (gint exit_code, GdfLibdryadDebugger *dbg)
{
    /* "exited" should be dealt with after the corresponding "stopped" */ 
    dbg->pending_exit = exit_code;
}

static void
execution_exited_signal_cb (char *sig_id, GdfLibdryadDebugger *dbg)
{
    /* "exited" should be dealt with after the corresponding "stopped" */ 
    dbg->pending_exit = 128;  /* FIXME: Map name to signal? */
    
    queue_event (dbg->event_source, "signal_termination", 
		 gdf_marshal_event_string (sig_id));
}

static void
execution_stopped_cb (GdfLibdryadDebugger *dbg)
{
    dbg->state &= ~GNOME_Development_Debugger_TARGET_EXECUTING;
    
    if (!dbg->gdb_inst->corefile_loaded) {
	queue_event (dbg->event_source, "stopped", gdf_marshal_event_none ());
    }

    if (dbg->pending_exit != -1) {
	dbg->state &= ~GNOME_Development_Debugger_TARGET_EXISTS;
	new_locals (dbg);
	gdf_libdryad_debugger_update_symsets (dbg, TRUE);
	queue_event (dbg->event_source, "exited", 
		     gdf_marshal_event_long (dbg->pending_exit));
	dbg->pending_exit = -1;
    } else {
	/* If the frame changed during the execution, this flag will be set.  
	 * We need to process this here rather than in 
	 * execution_frame_change_cb because any libdryad interaction in 
	 * execution_frame_change_cb will screw up the execution parsing. */
	if (dbg->frame_changed != -1) {
	    process_frame_change (dbg);
	    queue_event (dbg->event_source, "frame_change", 
			 gdf_marshal_event_long (dbg->frame_changed));
	    dbg->frame_changed = -1;
	} else {
	    gdf_libdryad_debugger_update_symsets (dbg, TRUE);
	}
    }
    
    /* This is the last signal in the sequence, so process the event queue */
    process_event_queue (dbg->event_source);
}

static void
execution_killed_cb (GdfLibdryadDebugger *dbg)
{
    dbg->state &= ~GNOME_Development_Debugger_TARGET_EXECUTING;

    g_warning ("libdryad debugger does not send the correct kill signal");

    send_event (dbg->event_source, "killed", gdf_marshal_event_long (0));
}


static void
execution_source_line_cb (gchar *source_file, gint line_num, 
			  GdfLibdryadDebugger *dbg)
{
    queue_event (dbg->event_source, "source_line", 
		 gdf_marshal_event_source_location (source_file, line_num));
}

static void
execution_frame_change_cb (int frame_id, GdfLibdryadDebugger *dbg)
{
    dbg->frame_changed = frame_id;
}

static void
execution_signal_received_cb (char *sig_id, GdfLibdryadDebugger *dbg)
{
    queue_event (dbg->event_source, "signal", 
		 gdf_marshal_event_string (sig_id));
}

static void
error_cb (gchar *error_msg, GdfLibdryadDebugger *dbg)
{
    dbg->error_condition = 1;
}

static void 
connect_signals (GdfLibdryadDebugger *dbg)
{
    gdb_signal_connect (dbg->gdb_inst, "execution-starting",
			execution_starting_cb, (gpointer) dbg);
    gdb_signal_connect (dbg->gdb_inst, "execution-running",
			execution_running_cb, (gpointer) dbg);
    gdb_signal_connect (dbg->gdb_inst, "execution-stopped",
			execution_stopped_cb, (gpointer) dbg);
    gdb_signal_connect (dbg->gdb_inst, "execution-killed",
			execution_killed_cb, (gpointer) dbg);
    gdb_signal_connect (dbg->gdb_inst, "execution-exited",
			execution_exited_cb, (gpointer) dbg);
    gdb_signal_connect (dbg->gdb_inst, "execution-exited-signal",
			execution_exited_signal_cb, (gpointer) dbg);
    gdb_signal_connect (dbg->gdb_inst, "execution-source-line",
			execution_source_line_cb, (gpointer) dbg);
    gdb_signal_connect (dbg->gdb_inst, "execution-frame-change",
			execution_frame_change_cb, (gpointer) dbg);
    gdb_signal_connect (dbg->gdb_inst, "execution-signal-received",
			execution_signal_received_cb, (gpointer) dbg);
    gdb_signal_connect (dbg->gdb_inst, "error", 
			error_cb, (gpointer)dbg);
}



