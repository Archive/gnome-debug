/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * Handles loading and unloading of targets.
 * 
 * Author : Dave Camp <campd@oit.edu>
 */

#include <glib.h>
#include <string.h>
#include "libdryad.h"

static gboolean check_load (GdbInstance *inst);

/* public routines */

/* Loads a file into the debugger.
 *  bin_filename - The name of the binary to load
 */
gboolean
gdb_load_file (GdbInstance *inst, const gchar *bin_filename)
{
    gchar *cmd_str;

    g_assert (!inst->file_loaded);

    cmd_str = g_strdup_printf ("file %s", bin_filename);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    /* Read gdb's response to determine if the load was successful */
    return check_load (inst);
}

/* Unloads a file from the debugger.  Actually, since there are no explicit
 * unload gdb functions, it just acts like it, and cleans up. */
void
gdb_unload_file (GdbInstance *inst)
{
    g_return_if_fail (inst->file_loaded);

    if (inst->is_running)
	gdb_stop (inst);

    gdb_execute_command (inst, "file");

    gdb_clear_command_queue (inst);

    inst->file_loaded = FALSE;
    inst->is_running = FALSE;
}

/* private rountines */
gboolean
check_load (GdbInstance *inst)
{
    gchar *line;
    gboolean successful = TRUE;

    line = gdb_read_line (inst);
    do {
	if (strcmp (line, "\32\32error-begin") == 0) {
	    successful = FALSE;
	}
	line = gdb_read_line (inst);
    } while (strcmp (line, "\32\32pre-prompt") != 0);

    inst->file_loaded = successful;
    return successful;
}
