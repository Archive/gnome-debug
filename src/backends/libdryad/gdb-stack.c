/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Stack browsing for libdryad.
 * 
 * Copyright (C) 2000 Dave Camp <campd@oit.edu>, 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include "libdryad.h"
#include <string.h>
#include <stdlib.h>

static GdbFrame *read_frame (GdbInstance *inst);

GList *
gdb_get_stack (GdbInstance *inst)
{
    GList *ret = NULL;
    GdbFrame *frame = NULL;

    if (!inst->is_running) {
	return NULL;
    }
    
    gdb_execute_command (inst, "backtrace");
    
    while ((frame = read_frame (inst))) {
	ret = g_list_prepend (ret, frame);
    }

    ret = g_list_reverse (ret);
    return ret;
}

void
gdb_destroy_stack (GdbInstance *inst, GList *stack)
{
    GList *i;
    
    for (i = stack; i != NULL; i = g_list_next (i)) {
	gdb_destroy_frame (inst, (GdbFrame*)i->data);
    }
    g_list_free (stack);
}

GdbFrame *
gdb_get_frame (GdbInstance *inst, int id)
{   
    GdbFrame *frame;
    char *cmd_str;

    if (!inst->is_running) {
	return NULL;
    }
    
    /* This pretty much sucks.  It would be nice if GDB would actually let
     * us look at a stack frame without using the crappy info frame 
     * command */

    cmd_str = g_strdup_printf ("bt %d", id + 1);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    while ((frame = read_frame (inst))) {
	if (frame->id != id) {
	    gdb_destroy_frame (inst, frame);
	} else {
	    return frame;
	}
    }

    return NULL;
}

void 
gdb_destroy_frame (GdbInstance *inst, GdbFrame *frame)
{
    g_return_if_fail (inst != NULL);
    g_return_if_fail (frame != NULL);

    if (frame->function) 
	g_free (frame->function);
    if (frame->function) 
	g_free (frame->file);
    g_free (frame);
}

int
gdb_count_stack_frames (GdbInstance *inst)
{
    char *line;
    int ret;

    if (!inst->is_running) {
	return FALSE;
    }

    gdb_execute_command (inst, "bt -1");

    line = gdb_read_line (inst);
    while (strncmp (line, "\32\32frame-begin", strlen ("\32\32frame-begin")))
	line = gdb_read_line (inst);
    line = gdb_read_line (inst);
    sscanf (line, "#%d", &ret);
    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32frame-end"))
	line = gdb_read_line (inst);
    return ret + 1;
}

gboolean
gdb_change_frame (GdbInstance *inst, int new_id)
{
    char *cmd_str;
    char *line;
    int num_frames;
    
    if (!inst->is_running) {
	return FALSE;
    }

    if (new_id < 0) {
	return FALSE;
    }

    num_frames = gdb_count_stack_frames (inst);

    if (new_id >= num_frames) {
	return FALSE;
    }

    cmd_str = g_strdup_printf ("frame %d", new_id);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);
    
    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32pre-prompt")) {
	if (!strcmp (line, "\32\32error-begin")) {
	    return FALSE;
	}
	line = gdb_read_line (inst);
    }
    return TRUE;
}

GdbFrame *
read_frame (GdbInstance *inst)
{
    GdbFrame *frame = NULL;
    char *line;
    
    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32frame-end") &&
	   strcmp (line, "\32\32pre-prompt")) {
	if (!strncmp (line,
		      "\32\32frame-begin", strlen ("\32\32frame-begin"))) {
	    frame = g_new0 (GdbFrame, 1);
	    frame->function = NULL;
	    frame->file = NULL;
	    frame->line_num = -1;
	    sscanf (line + strlen ("\32\32frame-begin"), "%d %lx", 
		    &frame->id, &frame->addr);
	} else if (!strcmp (line, "\32\32frame-function-name")) {
	    line = gdb_read_line (inst);
	    frame->function = g_strdup (line);
	} else if (!strcmp (line, "\32\32frame-source-file")) {
	    line = gdb_read_line (inst);
	    frame->file = g_strdup (line);
	} else if (!strcmp (line, "\32\32frame-source-line")) {
	    line = gdb_read_line (inst);
	    frame->line_num = atoi (line);
	}
	
	line = gdb_read_line (inst);
    }

    return frame;
}

