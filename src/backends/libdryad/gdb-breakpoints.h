/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * Breakpoint management functions.
 *
 * Author : Dave Camp <campd@oit.edu>
 */

#ifndef __GDB_BREAKPOINTS_H__
#define __GDB_BREAKPOINTS_H__

/* Breakpoint structure */
struct _GdbBreakpoint {
    gint num;
    gchar *type;
    gchar *disposition;
    gboolean enabled;
    gchar *address;
    gchar *file_name;
    gchar *function;
    gint line_num;

    GdbBreakpoint *next;
};

const GdbBreakpoint *gdb_breakpoint_set_linenum (GdbInstance *inst,
						 const gchar *filename,
						 gint linenum,
						 const gchar *condition);
const GdbBreakpoint *gdb_breakpoint_set_function (GdbInstance *inst,
						  const gchar *function_name,
						  const gchar *condition);

const GdbBreakpoint *gdb_breakpoint_set_watchpoint (GdbInstance *inst, 
						    const char *expression,
						    const char *condition);
GList *gdb_breakpoint_get_all (GdbInstance *inst);
const GdbBreakpoint *gdb_breakpoint_get_info (GdbInstance *inst, int bp_num);

gboolean gdb_breakpoint_disable (GdbInstance *inst,
				 gint bp_num);
gboolean gdb_breakpoint_enable (GdbInstance *inst,
				gint bp_num);
gboolean gdb_breakpoint_delete (GdbInstance *inst,
				gint bp_num);

/* Only used internally! */
void gdb_breakpoints_destroy (GdbInstance *inst);

#endif
