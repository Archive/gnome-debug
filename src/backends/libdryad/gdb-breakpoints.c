/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * Breakpoint management functions.
 *
 * Author : Dave Camp <campd@oit.edu>
 */

#include <glib.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>
#include "libdryad.h"
#include <gdl/gdl.h>

static void destroy_breakpoint (GdbBreakpoint *bp);
static void add_breakpoint (GdbInstance *inst, GdbBreakpoint *bp);
static GdbBreakpoint *find_breakpoint (GdbInstance *inst, int bp_num);
static const GdbBreakpoint *read_new_breakpoint (GdbInstance *inst);
static const GdbBreakpoint *read_new_watchpoint (GdbInstance *inst);
static GdbBreakpoint *read_bp_info (GdbInstance *inst, int bp_num);
static gboolean read_enable_response (GdbInstance *inst, int bp_num, 
				      gboolean enabled);

/* public functions */

/* Set a breakpoint at a given line 
 * file_name - The filename to set the breakpoint in.
 * line_num - The line number to set the breakpoint at.
 * condition - A freeform text condition to set, NULL if unconditional.
 */
const GdbBreakpoint *
gdb_breakpoint_set_linenum (GdbInstance *inst,
                            const char *file_name,
                            int line_num,
                            const char *condition)
{
    char *cmd_str = NULL;

    if (condition == NULL) {
	cmd_str = g_strdup_printf ("break %s:%d", file_name, line_num);
    } else {
	cmd_str = g_strdup_printf ("break %s:%d if %s",
				   file_name, line_num, condition);
    }

    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    return read_new_breakpoint (inst);
}

/* Set a breakpoint at a given line 
 * function_name - The function to break at.
 * condition - A freeform text condition to set, NULL if unconditional.
 */
const GdbBreakpoint *
gdb_breakpoint_set_function (GdbInstance *inst,
                             const char *function_name,
                             const char *condition)
{
    char *cmd_str = NULL;
        
    g_return_val_if_fail (function_name != NULL, NULL);
        
    if (condition == NULL) {
	cmd_str = g_strdup_printf ("break %s", function_name);
    } else {
	cmd_str = g_strdup_printf ("break %s if %s",
				   function_name, condition);
    }
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    return read_new_breakpoint (inst);
}

const GdbBreakpoint *
gdb_breakpoint_set_watchpoint (GdbInstance *inst, 
			       const char *expression,
			       const char *condition)
{
    char *cmd_str = NULL;
    
    g_return_val_if_fail (inst != NULL, NULL);
    g_return_val_if_fail (expression != NULL, NULL);

    if (condition) {
	cmd_str = g_strdup_printf ("watch %s if %s", expression, condition);
    } else {
	cmd_str = g_strdup_printf ("watch %s", expression);
    }
    
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    return read_new_watchpoint (inst);
}

static gint
all_breakpoint_traverse_func (gpointer key, gpointer value, gpointer data)
{
    GList **list = data;
    *list = g_list_prepend (*list, value);
    return FALSE;
}

GList *
gdb_breakpoint_get_all (GdbInstance *inst)
{
    GList *list = NULL;
    g_tree_traverse (inst->breakpoints, all_breakpoint_traverse_func,
		     G_IN_ORDER, &list);
    return g_list_reverse (list);
}

const GdbBreakpoint *
gdb_breakpoint_get_info (GdbInstance *inst, int bp_num)
{
    return find_breakpoint (inst, bp_num);
}

/* Enable a breakpoint */
gboolean
gdb_breakpoint_enable (GdbInstance *inst, int bp_num)
{
    char *cmd_str;
        
    cmd_str = g_strdup_printf ("enable breakpoints %d", bp_num);
        
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);
    return read_enable_response (inst, bp_num, TRUE);
}

/* Disable a breakpoint. */
gboolean
gdb_breakpoint_disable (GdbInstance *inst, int bp_num)
{
    char *cmd_str;
        
    cmd_str = g_strdup_printf ("disable breakpoints %d", bp_num);
        
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);
    return read_enable_response (inst, bp_num, FALSE); 
}

gboolean
gdb_breakpoint_delete (GdbInstance *inst,
                       int bp_num)
{
    char *cmd_str;
        
    /* Make sure the breakpoint exists */
    GdbBreakpoint *bp_work = find_breakpoint (inst, bp_num);
        
    if (!bp_work) {
	return FALSE;
    }

    g_tree_remove (inst->breakpoints, GINT_TO_POINTER (bp_num));
        
    /* Delete the breakpoint.  We know the breakpoint exists by this point, 
     * so there is pretty much no way this can fail. */
    cmd_str = g_strdup_printf ("delete %d", bp_num);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    destroy_breakpoint (bp_work);

    return TRUE;
}

static void
destroy_breakpoint (GdbBreakpoint *bp)
{
    g_free (bp->type);
    g_free (bp->disposition);
    if (bp->file_name) {
	g_free (bp->file_name);
    }
    
    if (bp->function) {
	g_free (bp->function);
    }

    g_free (bp);
}


static gint
free_func (gpointer key, GdbBreakpoint *bp, gpointer data)
{
    g_assert (bp != NULL);

    destroy_breakpoint (bp);

    return FALSE;
}

void
gdb_breakpoints_destroy (GdbInstance *inst)
{
    g_tree_traverse (inst->breakpoints, (GTraverseFunc)free_func, 
		     G_IN_ORDER, NULL);
    g_tree_destroy (inst->breakpoints);
    inst->breakpoints = NULL;
}

static int
int_cmp (gconstpointer a, gconstpointer b) 
{
    return (GPOINTER_TO_INT (a) < GPOINTER_TO_INT (b) 
	    ? -1 
	    : (GPOINTER_TO_INT (a) == GPOINTER_TO_INT (b) ? 0 : 1));
}

/* private functions */
void
add_breakpoint (GdbInstance *inst, GdbBreakpoint *bp)
{
    if (!inst->breakpoints) {
	inst->breakpoints = g_tree_new (int_cmp);
    }
    
    g_tree_insert (inst->breakpoints, GINT_TO_POINTER (bp->num), bp);
}

GdbBreakpoint *
find_breakpoint (GdbInstance *inst, int bp_num)
{
    return g_tree_lookup (inst->breakpoints, GINT_TO_POINTER (bp_num));
}

const GdbBreakpoint *
read_new_breakpoint (GdbInstance *inst)
{
    char *line;
    int bp_num;
    GdbBreakpoint *bp;
        
    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32pre-prompt") != 0) {
	if (strcmp (line, "\32\32error-begin") == 0) {
	    return NULL;
	} else if (strncmp (line, "Breakpoint ",
			    strlen ("Breakpoint ")) == 0) {
	    sscanf (line, "Breakpoint %d", &bp_num);
	    bp = read_bp_info (inst, bp_num);
	    if (bp) {
		add_breakpoint (inst, bp);
	    }
	    return bp;
	}
	line = gdb_read_line (inst);
    }
    return NULL;
}

const GdbBreakpoint *
read_new_watchpoint (GdbInstance *inst)
{
    char *line;
    int bp_num;
    GdbBreakpoint *bp = NULL;

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32pre-prompt")) {
	if (!strcmp (line, "\32\32error-begin")) {
	    return NULL;
	} else if (!strncmp (line, "Watchpoint", strlen ("Watchpoint"))) {
	    sscanf (line, "Watchpoint %d", &bp_num);
	    bp = read_bp_info (inst, bp_num);
	    add_breakpoint (inst, bp);
	} else if (!strncmp (line, "Hardware watchpoint",
			    strlen ("Hardware watchpoint"))) {
	    sscanf (line, "Hardware watchpoint %d", &bp_num);
	    bp = read_bp_info (inst, bp_num);
	    add_breakpoint (inst, bp);
	}
	line = gdb_read_line (inst);
    }
    
    return bp;
}

GdbBreakpoint *
read_bp_info (GdbInstance *inst, int bp_num)
{
    char *cmd_str;
    char *line = NULL;
    char *work;

    GdbBreakpoint *bp_work = NULL;

    cmd_str = g_strdup_printf ("info break %d", bp_num);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);
    
    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32record") != 0) {
	line = gdb_read_line (inst);
    }
        
    while (strcmp (line, "\32\32breakpoints-table-end") != 0) {
	if (line[0] == 0x1A && line[1] == 0x1A) {
	    if (strcmp (line + 2, "field 0") == 0) {  /* num */
		line = gdb_read_line (inst);
		sscanf (line, "%d", &bp_num);
		bp_work = g_new0 (GdbBreakpoint, 1);
		bp_work->num = bp_num;
	    } else if (strcmp (line + 2, "field 1") == 0) {  /* type */
		line = gdb_read_line (inst);
		work = line + strlen(line) - 1;
		while (isspace (*work--))
		    ;
		*(work + 2) = '\0';
		bp_work->type = g_strdup (line);
	    } else if (strcmp (line + 2, "field 2") == 0) {  /* disposition */
		line = gdb_read_line (inst);
		bp_work->disposition = g_strdup (line);
	    } else if (strcmp (line + 2, "field 3") == 0) {  /* enabled */
		line = gdb_read_line (inst);
		switch (line[0]) {
		case 'y':
		    bp_work->enabled = TRUE;
		    break;
		case 'n':
		    bp_work->enabled = FALSE;
		    break;
		}
	    } else if (strcmp (line + 2, "field 4") == 0) {  /* address */
		line = gdb_read_line (inst);
		bp_work->address = g_strdup (line);
	    } else if (strcmp (line + 2, "field 5") == 0) {  /* misc info */
		line = gdb_read_line (inst);
		if (!strcmp (bp_work->type, "breakpoint")) {
		    work = strrchr (line, ':');
		    if (work) {
			sscanf (work, ":%d", &bp_work->line_num);
			*work = '\0';
			work = strrchr (line, ' ') + 1;
			if (work == (char*)1)
			    work = line;
			bp_work->file_name = g_strdup (work);
		    }
		    if ((work = strstr (line, "in "))) {
			char *end;
			work += 3;
			end = strchr (work, ' ');
			if (end) {
			    *end = '\0';
			}
			
			bp_work->function = g_strdup (work);
		    }
		} else if (strstr (bp_work->type, "watchpoint")) {
		    bp_work->address = g_strdup (line);
		}
	    }
	}
	line = gdb_read_line (inst);
    }
    
    if (bp_work) {
	/* Expand the filename to the absolute path */
	if (bp_work->file_name) {
	    work = g_strdup (gdb_get_full_source_path (inst, 
						       bp_work->file_name));
	    g_free (bp_work->file_name);
	    bp_work->file_name = work;
	}
    }

    return bp_work;
}

gboolean
read_enable_response (GdbInstance *inst, int bp_num, gboolean enabled)
{
    char *line;
    GdbBreakpoint *bp_work;
    gboolean found_bp = FALSE;
        
    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32pre-prompt") != 0) {
	if (strcmp (line, "\32\32error-begin") == 0) {
	    break;
	} else if (strcmp (line, "\32\32breakpoints-invalid") == 0) {
	    found_bp = TRUE;
	    break;
	}
	line = gdb_read_line (inst);
    }
    if (found_bp) {
	bp_work = find_breakpoint (inst, bp_num);
	g_assert (bp_work != NULL);
	bp_work->enabled = enabled;
    }
    return found_bp;
}
