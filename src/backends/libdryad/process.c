/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/* 
 * Creates processes and maps IO to files.
 * 
 * Author : Dave Camp <campd@oit.edu>
 */

#include <glib.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <sys/wait.h>

#include "process.h"

#define READ 0
#define WRITE 1

static void setup_communication (ChildProcess *proc);
static void setup_child_communication (ChildProcess *proc);
static void setup_parent_communication (ChildProcess *proc);
static void execute_child (gchar *binfilename);

ChildProcess *
create_child_process (gchar *binfilename)
{
    ChildProcess *proc;

    proc = (ChildProcess *) g_malloc (sizeof (ChildProcess));
    if (proc == NULL)
	return NULL;

    setup_communication (proc);

    if ((proc->child_pid = fork ()) == 0) {
	setup_child_communication (proc);
	execute_child (binfilename);
    }
    if (proc->child_pid == -1) {
	g_error ("Could not fork child process");
    }
    setup_parent_communication (proc);
    return proc;
}

void
destroy_child_process (ChildProcess *proc)
{
    int status;

    kill (proc->child_pid, SIGKILL);
    waitpid (proc->child_pid, &status, WUNTRACED);
    fclose (proc->errorfp);
    fclose (proc->inputfp);
    fclose (proc->outputfp);
    g_free (proc);
}

void
setup_communication (ChildProcess *proc)
{
    if (pipe (proc->to_child) < 0)
	g_error ("Error creating stdin pipe");
    if (pipe (proc->to_parent) < 0)
	g_error ("Error creating stdout pipe");
    if (pipe (proc->to_parent_error) < 0)
	g_error ("Error creating stderr pipe");
}

void
setup_child_communication (ChildProcess *proc)
{
    close (proc->to_child[WRITE]);
    close (proc->to_parent[READ]);
    close (proc->to_parent_error[READ]);

    dup2 (proc->to_child[READ], STDIN_FILENO);
    close (proc->to_child[READ]);

    /* Map both stderr and stdout to the same pipe */
    dup2 (proc->to_parent[WRITE], STDOUT_FILENO);
    dup2 (proc->to_parent[WRITE], STDERR_FILENO);
    close (proc->to_parent[WRITE]);
    close (proc->to_parent_error[WRITE]);
}

void
setup_parent_communication (ChildProcess *proc)
{
    close (proc->to_child[READ]);
    close (proc->to_parent[WRITE]);
    close (proc->to_parent_error[WRITE]);

    proc->errorfp = fdopen (proc->to_parent_error[READ], "r");
    proc->inputfp = fdopen (proc->to_parent[READ], "r");
    proc->outputfp = fdopen (proc->to_child[WRITE], "w");
    if (proc->inputfp == NULL || proc->outputfp == NULL)
	g_error ("Error executing child process");

    /* Turn off buffering on the input from the process */
    setvbuf (proc->inputfp, NULL, _IONBF, 0);
}

void
execute_child (gchar *binfilename)
{
    gchar *exec_str;

    exec_str = g_strdup_printf ("exec %s", binfilename);

    if (exec_str == NULL)
	g_error ("Out of memory");

    execl ("/bin/sh", "sh", "-c", exec_str, (char *) 0);

    /* Could not find child */
    /* FIXME: Better error handling */
    g_free (exec_str);

    g_error ("Error executing /bin/sh - Cannot execute %s",
	     binfilename);
}
