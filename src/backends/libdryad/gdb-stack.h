/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Stack browsing for libdryad.
 * 
 * Copyright (C) 2000 Dave Camp <campd@oit.edu>, 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __GDB_STACK_H__
#define __GDB_STACK_H__

typedef struct _GdbFrame   GdbFrame;

struct _GdbFrame {
	int id;
	char *function;
	char *file;
	int line_num;
	long addr;
};

GList *gdb_get_stack (GdbInstance *inst);
void gdb_destroy_stack (GdbInstance *inst, GList *stack);
GdbFrame *gdb_get_frame (GdbInstance *inst, int frame_id);
void gdb_destroy_frame (GdbInstance *inst, GdbFrame *frame);

int gdb_count_stack_frames (GdbInstance *inst);
gboolean gdb_change_frame (GdbInstance *inst, int new_id);

#endif
