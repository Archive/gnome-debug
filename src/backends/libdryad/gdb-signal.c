/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * libdryad signals.
 *
 * Author : Dave Camp <campd@oit.edu>
 */

#include <glib.h>
#include <stdarg.h>
#include <string.h>
#include "libdryad.h"

/* FIXME: Need better documentation on these signals. */
GdbSignal signals[] =
{
    {"error", NULL, NULL, GDB_SIGNAL_MARSHALLER_GCHARPTR},
    {"info", NULL, NULL, GDB_SIGNAL_MARSHALLER_GCHARPTR},
    {"query", NULL, NULL, GDB_SIGNAL_MARSHALLER_GCHARPTR},
    {"gdb-exited", NULL, NULL, GDB_SIGNAL_MARSHALLER_VOID},
    {"execution-running", NULL, NULL, GDB_SIGNAL_MARSHALLER_VOID},
    {"execution-exited", NULL, NULL, GDB_SIGNAL_MARSHALLER_GINT},
    {"execution-exited-signal", NULL, NULL, GDB_SIGNAL_MARSHALLER_GCHARPTR},
    {"execution-starting", NULL, NULL, GDB_SIGNAL_MARSHALLER_VOID},
    {"execution-stopped", NULL, NULL, GDB_SIGNAL_MARSHALLER_VOID},
    {"execution-killed", NULL, NULL, GDB_SIGNAL_MARSHALLER_VOID},
    {"execution-source-line", NULL, NULL, GDB_SIGNAL_MARSHALLER_GCHARPTR_GINT},
    {"execution-frame-change", NULL, NULL, GDB_SIGNAL_MARSHALLER_GINT},
    {"execution-signal-received", NULL, NULL, GDB_SIGNAL_MARSHALLER_GCHARPTR},
    {"execution-breakpoint-hit", NULL, NULL, GDB_SIGNAL_MARSHALLER_GINT},
    {"display", NULL, NULL, GDB_SIGNAL_MARSHALLER_GINT_GPOINTER_GPOINTER},
    {"display-repeat", NULL, NULL, GDB_SIGNAL_MARSHALLER_GINT_GPOINTER_GPOINTER_GINT_GINT},
    {NULL}
};

void
gdb_signal_init (GdbInstance *inst)
{
    inst->signals = signals;
}

void
gdb_signal_connect (GdbInstance *inst,
                    gchar *signal_name,
                    void (*handler) (),
                    gpointer data)
{
    gint i;
        
    i = 0;
    while (inst->signals[i].signal_name != NULL) {
	if (strcmp (signal_name, inst->signals[i].signal_name) == 0) {
	    inst->signals[i].handler = handler;
	    inst->signals[i].data = data;
	    return;
	}
	i++;
    }
    g_warning ("Signal %s not found.", signal_name);
}

void
gdb_signal_emit (GdbInstance *inst,
                 gchar *signal_name,
                 ...)
{
    gint i;
    va_list list;

    va_start (list, signal_name);
    i = 0;
    while (inst->signals[i].signal_name != NULL) {
	if (strcmp (signal_name, inst->signals[i].signal_name) == 0) {
	    if (inst->signals[i].handler != NULL) {
		inst->signals[i].marshaller (inst->signals[i].handler,
					     inst->signals[i].data,
					     list);
	    }
	    return;
	}
	++i;
    }
    g_warning ("Signal %s not found.", signal_name);
}
