/* -*- Mode: C; tab-width: 4; indent-tabs-mode: n; c-basic-offset: 4 -*- */

#ifndef __GDF_LIBDRYAD_SYMBOL_SET_H__
#define __GDF_LIBDRYAD_SYMBOL_SET_H__

typedef struct _GdfLibdryadSymbolSet GdfLibdryadSymbolSet;

#include <bonobo/bonobo-object.h>
#include "../../lib/gdf.h"
#include "libdryad.h"
#include "gdf-libdryad-debugger.h"

BEGIN_GNOME_DECLS

#define GDF_LIBDRYAD_SYMBOL_SET_TYPE (gdf_libdryad_symbol_set_get_type ())
#define GDF_LIBDRYAD_SYMBOL_SET(o) (GTK_CHECK_CAST ((o), GDF_LIBDRYAD_SYMBOL_SET_TYPE, GdfLibdryadSymbolSet))
#define GDF_LIBDRYAD_SYMBOL_SET_CLASS(k) (GTK_CHECK_CLASS_CAST ((k), GDF_LIBDRYAD_SYMBOL_SET_CLASS_TYPE, GdfLibdryadSymbolSetClass))
#define GNOME_Development_IS_LIBDRYAD_SYMBOL_SET(o) (GTK_CHECK_TYPE ((o), GDF_LIBDRYAD_SYMBOL_SET_TYPE))
#define GNOME_Development_IS_LIBDRYAD_SYMBOL_SET_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_LIBDRYAD_SYMBOL_SET_TYPE))

struct _GdfLibdryadSymbolSet {
    BonoboObject base;
    
    GdfLibdryadDebugger *dbg;
	BonoboEventSource *event_source;
	GNOME_Development_SymbolSequence *symbols;
	int set_size;
};

typedef struct 
{
    BonoboObjectClass parent_class;
} GdfLibdryadSymbolSetClass;

GdfLibdryadSymbolSet *gdf_libdryad_symbol_set_new (GdfLibdryadDebugger *dbg);
GtkType gdf_libdryad_symbol_set_get_type (void);

void gdf_libdryad_symbol_set_changed (GdfLibdryadSymbolSet *ss);
void gdf_libdryad_symbol_set_update (GdfLibdryadSymbolSet *ss);

END_GNOME_DECLS

#endif

