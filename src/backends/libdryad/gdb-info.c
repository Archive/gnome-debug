/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/* 
 * Handles the gdb "info" commands
 * 
 * Author : Dave Camp <campd@oit.edu>
 */

#include <glib.h>
#include <gtk/gtk.h>
#include <string.h>
#include "libdryad.h"

/* public rountines */

/* Post an "info sources" command.  The source list will come to the user
 * through the use of singals. 
 * Before the first source, an info-sources-begin signal will be sent.
 * For each source file, an info-sources-file signal will be sent.
 * After all files have been processed, an info-sources-end will be sent.
 */
GList *
gdb_list_sources (GdbInstance *inst)
{
    GList *ret;
    gchar *line;
    gchar *filename;
    gchar *ptr_ch;

    gdb_execute_command (inst, "info sources");
    
    ret = NULL;
    
    line = gdb_read_line (inst);
    
    while (strcmp (line, "\32\32pre-prompt") != 0) {
	/*  skip status/blank/annotation lines */
	if (strchr (line, ':') == NULL  /* status line */
	    && line[0] != '\0'  /* blank line */
	    && (line[0] != 0x1A || line[0] != 0x1A)) {  /* annotation */
	    filename = line;
	    do {
		ptr_ch = strchr (filename, ',');
		if (ptr_ch != NULL)
		    *ptr_ch = '\0';
		
		ret = g_list_prepend (ret, e_strdup_strip (filename));
		
		if (ptr_ch != NULL && *(ptr_ch + 2) != '\0')
		    filename = ptr_ch + 2;
	    } while (ptr_ch != NULL && *(ptr_ch + 2) != '\0');
	}
	line = gdb_read_line (inst);
    }
    
    ret = g_list_reverse (ret);

    return ret;    
}

/* Returns the full source path of a given source file.  The returned string
 * is a static buffer, so you will have to strdup it if you want a permanant
 * copy of it.
 */
gchar *
gdb_get_full_source_path (GdbInstance *inst, const gchar *file_name)
{
    /* FIXME: Cache source paths */
    gchar *line;
    static gchar path[PATH_MAX];
    gchar *cmd_str;

    /* Make the file "current".  Wierd hack. */
    cmd_str = g_strdup_printf ("list %s:0", file_name);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    gdb_execute_command (inst, "info source");

    line = gdb_read_line (inst);
    while (strstr (line, "Located in") != line) {
	if (strcmp (line, "\32\32pre-prompt") == 0) {
	    strcpy (path, "NOTFOUND");
	    return path;
	}
	line = gdb_read_line (inst);
    }
    sscanf (line, "Located in %s", path);
    
    return path;
}

char *
gdb_get_register_value (GdbInstance *inst, const char *register_name)
{
    char *line;
    char *cmd;
    char *ret = NULL;
    char *t;
    
    if (!inst->is_running) {
	return NULL;
    }
    
    cmd = g_strdup_printf ("info register %s", register_name);
    gdb_execute_command (inst, cmd);
    g_free (cmd);

    line = gdb_read_line (inst);
    if (strncmp (line, register_name, strlen (register_name))) {
	return NULL;
    } else {
	t = strtok (line, " ");
	t = strtok (NULL, "\t");
	ret = e_strdup_strip (t);
    }

    return ret;
}    

void
gdb_set_register_value (GdbInstance *inst, 
			const char *name,
			const char *value)
{
    char *cmd = g_strdup_printf ("set $%s = %s", name, value);
    gdb_execute_command (inst, cmd);
    g_free (cmd);

    /* This is kinda minimal, some error checking/propagation would be
     * nice here, but not necessary. */
}

/* Returns a GList of register information */
GList *
gdb_get_registers (GdbInstance *inst, gboolean floating_pt)
{
    GList *list;
    gchar *line;
    GdbRegister *reg;

    if (!inst->is_running) {
	return NULL;
    }

    if (floating_pt) {
	gdb_execute_command (inst, "info all-registers");
    } else {
	gdb_execute_command (inst, "info registers");
    }
    
    list = NULL;

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32pre-prompt") != 0) {
	char *t;
	
	if (line[0] == '\0' || strcmp (line, "\32\32post-prompt") == 0) {
	    line = gdb_read_line (inst);
	    continue;
	}
	/* FIXME: use strtok rather than scanf */
	reg = g_new0 (GdbRegister, 1);
	t = strtok (line, " ");
	reg->name = e_strdup_strip (t);
	t = strtok (NULL, "\t");
	reg->value = e_strdup_strip (t);
	list = g_list_prepend (list, (gpointer)reg);
	
	line = gdb_read_line (inst);
    }
    
    list = g_list_reverse (list);
    
    return list;
}

void
gdb_destroy_register_list (GdbInstance *inst, GList *regs)
{
    GList *i1, *i2;
    
    for (i1 = regs; i1 != NULL; i1 = i2) {
	GdbRegister *r = i1->data;
	i2 = i1->next;
	g_free (r->name);
	g_free (r->value);
	g_free (r);
	g_list_free_1 (i1);
    }
}
