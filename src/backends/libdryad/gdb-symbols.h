/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Symbol viewing for libdryad.
 * 
 * Copyright (C) 2000 Dave Camp <campd@oit.edu>, 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __GDB_SYMBOLS_H__
#define __GDB_SYMBOLS_H__

#include "gdf-libdryad-symbol-set.h"

/* The Symbols API does direct manipulation of CORBA data structures, unlike
 * the rest of libdryad.  It isn't too pretty, but the alternative is much
 * worse. */

typedef enum {
    GDB_SYMBOL_OK,
    GDB_SYMBOL_INVALID_TYPE
} GdbSymbolResult;

void gdb_get_locals (GdbInstance *inst, GdfLibdryadSymbolSet *ss);
void gdb_get_expression (GdbInstance *inst, GNOME_Development_Symbol *sym, const char *expr);
void gdb_expand_symbol (GdbInstance *inst, GdfLibdryadSymbolSet *ss,
			GNOME_Development_Symbol *sym);
void gdb_update_symbols (GdbInstance *inst, GdfLibdryadSymbolSet *ss);
GNOME_Development_SymbolSequence *gdb_allocate_symbols (void);
void gdb_add_expression (GdbInstance *inst, GdfLibdryadSymbolSet *set, 
			 const char *expr);
GdbSymbolResult gdb_set_symbol_value (GdbInstance *inst, GNOME_Development_Symbol *sym,
				      const char *value);
GdbSymbolResult gdb_cast (GdbInstance *inst, GdfLibdryadSymbolSet *ss,
			  GNOME_Development_Symbol *sym, const char *type);

#endif
