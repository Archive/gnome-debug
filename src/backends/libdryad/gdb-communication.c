/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * Routines for handling communication with gdb
 *
 * Author : Dave Camp <campd@oit.edu>
 */

#include <gtk/gtk.h>
#include <string.h>
#include "libdryad.h"

#define READ 0
#define WRITE 1

#define TRACE_GDB
static FILE* trace_fp = NULL;

static void input_callback (GdbInstance *inst,
                            gint source,
                            GdkInputCondition condition);
static void execute_command (GdbInstance *inst);

/* public rountines */

/* Sends a command directly to gdb, and returns.  After calling this function,
 * the caller must either stop reading at the pre-prompt annotation or set the
 * inst->at_prompt flag accordingly.  */
void
gdb_execute_command (GdbInstance *inst, char *command) 
{
    char *line;
    
    if (!inst->at_prompt)
	gdb_wait_for_prompt (inst);
        
    inst->at_prompt = FALSE;

    fprintf (inst->proc->outputfp, "%s\n", command);
    fflush (inst->proc->outputfp);

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32post-prompt"))
	line = gdb_read_line (inst);
}

/* Places a command in the command queue, and if there is no executing command,
 * executes the command.  
 * command - The command string to send to gdb.
 * handler - A callback that will process the information returned by gdb.
 *           The handler should return TRUE when it is done processing, or
 *           FALSE when not done processing.
 * data    - User data to be sent to the handler.
 */
void
gdb_post_command (GdbInstance *inst,
                  gchar *command,
                  GdbResponseHandler handler,
                  gpointer data)
{
    gdb_post_command_full (inst,
			   command,
			   handler,
			   data,
			   NULL);
}

/* Places a command in the command queue, and if there is no executing command,
 * executes the command.  This one has destruction notification.
 * command - The command string to send to gdb.
 * handler       - A callback that will process the information returned by 
 *                 gdb. The handler should return TRUE when it is done 
 *                 processing, or FALSE when not done processing.
 * data           - User data to be sent to the handler.
 * destroy_notify - Function to call when done processing all handlers.
 */
void
gdb_post_command_full (GdbInstance *inst,
                       gchar *command,
                       GdbResponseHandler handler,
                       gpointer data,
                       GdbDestroyNotifyFunc destroy_notify)
{
    GdbCommand *new_cmd;
    gchar *command_cpy;
        
    g_return_if_fail (command != NULL);
        
    new_cmd = g_malloc (sizeof (GdbCommand));
    command_cpy = g_strdup (command);
        
    /* Fill the command structure */
    new_cmd->command = command_cpy;
    new_cmd->handler = handler;
    new_cmd->destroy_notify = destroy_notify;
    new_cmd->data = data;
    new_cmd->next = NULL;
        
    /* link it to the cmd queue */
    if (inst->last_command == NULL) {
	inst->last_command = new_cmd;
	inst->first_command = new_cmd;
    } else {
	inst->last_command->next = new_cmd;
	inst->last_command = new_cmd;
    }
        
    if (inst->at_prompt) {
	execute_command (inst);
    }
}

void
gdb_clear_command_queue (GdbInstance *inst)
{
    GdbCommand *cmd = inst->first_command;
    GdbCommand *cmd_prev;
        
    while (cmd != NULL) {
	cmd_prev = cmd;
	cmd = cmd->next;
	
	g_free (cmd_prev);
    }
        
    inst->first_command = NULL;
    inst->last_command = NULL;
}

/* Starts listening to gdb's output. Currently uses the GTK+ event loop.
 * In the future it should probably let the user provide a "start listener" 
 * callback. */
void
gdb_start_listener (GdbInstance *inst)
{
    inst->listener_id = gdk_input_add (inst->proc->to_parent[READ],
				       GDK_INPUT_READ,
				       (GdkInputFunction) input_callback,
				       (gpointer) inst);
}

/* Stop listening to gdb's output. */
void
gdb_stop_listener (GdbInstance *inst)
{
    gdk_input_remove (inst->listener_id);
}

/* Reads a line from gdb, and returns a pointer to a static buffer.  
 * NOTE - If you need to save the line for any reason, you'd better 
 * strdup it. */
gchar *
gdb_read_line (GdbInstance *inst)
{
    static gchar buf[512];  /* FIXME: Buffer length? */
        
    memset (buf, 0, sizeof (buf));
    fgets (buf, 512, inst->proc->inputfp);
        
    /* chop off the newline */
    buf[strlen (buf) - 1] = '\0';
  
#ifdef TRACE_GDB
    if (!trace_fp)
	trace_fp = fopen ("/home/dave/gdf.log", "w");
    
    fprintf (trace_fp, "%s\n", buf);
#endif
        
    return buf;
}

/* Get to a prompt. */
void
gdb_wait_for_prompt (GdbInstance *inst)
{
    gchar *line;
        
    if (inst->at_prompt)
	return;
        
    line = gdb_read_line (inst);
        
    while (strcmp (line, "\32\32prompt") != 0) {
	line = gdb_read_line (inst);
    }
    inst->at_prompt = TRUE;
}

/* private rountines */
static void
input_callback (GdbInstance *inst,
                gint source,
                GdkInputCondition condition)
{
    gboolean command_done;
    GdbCommand *cmd = inst->running_command;
    
    if (cmd == NULL) {
	gdb_wait_for_prompt (inst);
	return;
    }
    if (cmd->handler != NULL) {
	command_done = cmd->handler (inst, cmd->data);
    } else {
	command_done = TRUE;
    }

    if (command_done) {
	if (cmd->destroy_notify) {
	    (*cmd->destroy_notify) (cmd->data);
	}
	g_free (cmd->command);
	g_free (cmd);
	inst->running_command = NULL;
	gdb_wait_for_prompt (inst);
	execute_command (inst);
    }
}

static void
execute_command (GdbInstance *inst)
{
    GdbCommand *current_command;
    char *line;

    if (!inst->at_prompt) {
	gdb_wait_for_prompt (inst);
    }
    if (inst->first_command == NULL)
	return;

    inst->at_prompt = FALSE;

    current_command = inst->first_command;

    fprintf (inst->proc->outputfp, "%s\n", current_command->command);
    fflush (inst->proc->outputfp);

#ifdef TRACE_GDB
    if (!trace_fp)
	trace_fp = fopen ("/home/dave/gdf.log", "w");

    fprintf (trace_fp, "%s\n", inst->first_command->command);
#endif

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32post-prompt"))
	line = gdb_read_line (inst);

    if (current_command == inst->last_command) {
	inst->first_command = NULL;
	inst->last_command = NULL;
    } else {
	inst->first_command = inst->first_command->next;
    }

    inst->running_command = current_command;
}
