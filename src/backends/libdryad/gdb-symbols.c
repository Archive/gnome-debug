/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Symbol viewing for libdryad.
 * 
 * Copyright (C) 1999 Dave Camp <campd@oit.edu>, 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include "libdryad.h"
#include <string.h>
#include <ctype.h>

#include "gdf-libdryad-debugger.h"

#define MAX_ARRAY_SIZE 1000
#define NUM_SLICES 10

static void update_symbol (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
			   GNOME_Development_Symbol *sym);
static void read_type (GdbInstance *inst, GNOME_Development_Symbol *sym);
static void read_value (GdbInstance *inst, GNOME_Development_Symbol *sym);
static void expand_pointer (GdbInstance *inst, GdfLibdryadSymbolSet *ss,
			    GNOME_Development_Symbol *sym);
static void expand_structure (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
			      GNOME_Development_Symbol *sym);
static void expand_array (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
			  GNOME_Development_Symbol *sym);
static void expand_array_slice (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
				GNOME_Development_Symbol *sym);
static void add_slice (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
		       GNOME_Development_Symbol *sym, unsigned begin, unsigned size);
static void expand_slice (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
			  GNOME_Development_Symbol *sym, unsigned start, unsigned num);
static void destroy_children (GNOME_Development_Symbol *sym);
static GNOME_Development_SymbolSequence *init_seq (void);
static void grow_seq (GNOME_Development_SymbolSequence *seq, int amount);

void
gdb_get_locals (GdbInstance *inst, GdfLibdryadSymbolSet *ss)
{
    char *line;
    GList *names = NULL;
    GList *i;
    char *work;

    if (ss->symbols) {
	CORBA_free (ss->symbols);
    }
    ss->set_size = 0;
    ss->symbols = gdb_allocate_symbols ();

    gdb_execute_command (inst, "info scope *($pc)");

    /* Look for the scope header */
    line = gdb_read_line (inst);
    while (strncmp (line, "Scope for *($pc)", 16)
	   && strcmp (line, "\32\32error-begin"))
	line = gdb_read_line (inst);

    if (strcmp (line, "\32\32error-begin")) {
	/* read all the variables in the scope */
	line = gdb_read_line (inst);
	while (!isspace (line[0]) && line[0] != '\0') {
	    line += strlen ("Symbol ");
	    work = line;
	    while (!isspace (*work++))
		;
	    *(work - 1) = '\0';
	    names = g_list_prepend (names, g_strdup (line));
	    line = gdb_read_line (inst);
	}
	
	/* Add all the locals to the symbol set */
	names = g_list_reverse (names);
	
	for (i = names; i != NULL; i = i->next) {
	    gdb_add_expression (inst, ss, i->data);
	    g_free (i->data);
	}
	g_list_free (names);
    }

    /* Notify the symbol set that it has changed */
    gdf_libdryad_symbol_set_changed (ss);
}

void
gdb_get_expression (GdbInstance *inst, GNOME_Development_Symbol *sym, const char *expr)
{
    sym->name = CORBA_string_dup (expr);
    sym->expression = CORBA_string_dup (expr);

    read_type (inst, sym);
    read_value (inst, sym);
}

void
gdb_expand_symbol (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
		   GNOME_Development_Symbol *sym)
{
    g_return_if_fail (sym->expandable);

    if (!sym->expanded) {
	switch (sym->type) {
	case GNOME_Development_TYPE_PTR :
	    expand_pointer (inst, ss, sym);
	    break;
	case GNOME_Development_TYPE_STRUCT :
	    expand_structure (inst, ss, sym);
	    break;
	case GNOME_Development_TYPE_ARRAY :
	    expand_array (inst, ss, sym);
	    break;
	case GNOME_Development_TYPE_ARRAY_SLICE :
	    expand_array_slice (inst, ss, sym);
	    break;
	default:
	    return;
	}
    }

    sym->expanded = TRUE;
}

GdbSymbolResult
gdb_set_symbol_value (GdbInstance *inst, GNOME_Development_Symbol *sym, const char *value)
{
    char *cmd_str;
    char *line;

    cmd_str = g_strdup_printf ("set variable %s=%s", sym->expression, value);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32pre-prompt")) {
	if (!strcmp (line, "\32\32error-begin")) {
	    line = gdb_read_line (inst);
	    if (!strcmp (line, "Invalid cast.")) {
		return GDB_SYMBOL_INVALID_TYPE;
	    }
	}
	line = gdb_read_line (inst);
    }

    return GDB_SYMBOL_OK;
}

GdbSymbolResult
gdb_cast (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
	  GNOME_Development_Symbol *sym, const char *type)
{
    char *old;
    char *new;

    if (sym->cast_to[0] != '\0') {
	/* remove the cast from the expression */
	old = sym->expression;
	sym->expression = 
	    CORBA_string_dup (sym->expression + strlen (sym->cast_to) + 2);

	CORBA_free (old);
    }

    if (type[0] != '\0') {
	CORBA_free (sym->cast_to);
	sym->cast_to = CORBA_string_dup (type);
	new = g_strdup_printf ("(%s)%s", sym->cast_to, sym->expression);
	CORBA_free (sym->expression);
	sym->expression = CORBA_string_dup (new);
	g_free (new);
    }

    update_symbol (inst, ss, sym);

    return GDB_SYMBOL_OK;
}

static void
update_symbol (GdbInstance *inst, GdfLibdryadSymbolSet *ss, GNOME_Development_Symbol *sym)
{

    char *old_value;
    char *old_typename;
    CORBA_boolean was_expandable;
    
    old_value = sym->value;
    sym->value = NULL;
    old_typename = sym->typename;
    sym->typename = NULL;
    was_expandable = sym->expandable;
    
    read_type (inst, sym);
    read_value (inst, sym);
    
    if (strcmp (old_typename, sym->typename)) {
	destroy_children (sym);
	
	send_event (ss->event_source, "symbol_type_changed", 
		    gdf_marshal_event_long (sym->handle));

    }
    
    if (strcmp (old_value, sym->value) 
	|| was_expandable != sym->expandable) {
	send_event (ss->event_source, "symbol_changed", 
		    gdf_marshal_event_long (sym->handle));
    }
    
    CORBA_free (old_value);
    CORBA_free (old_typename);    
}

static void
update_seq (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
	    GNOME_Development_SymbolSequence *seq) 
{
    GNOME_Development_Symbol *sym;
    int i;

    for (i = 0; i < seq->_length; i++) {
	sym = &seq->_buffer[i];

	if (sym->type != GNOME_Development_TYPE_ARRAY_SLICE) {
	    update_symbol (inst, ss, sym);
	}
	
	update_seq (inst, ss, 
		    (GNOME_Development_SymbolSequence*)sym->children._value);
    }
}

void
gdb_update_symbols (GdbInstance *inst, GdfLibdryadSymbolSet *ss)
{
    update_seq (inst, ss, ss->symbols);
}

GNOME_Development_SymbolSequence *
gdb_allocate_symbols (void)
{
    return init_seq ();
}

static void
add_expression_to_seq  (GdbInstance *inst,
			GNOME_Development_SymbolSequence *seq, 
			GdfLibdryadSymbolSet *ss, 
			const char *expr)
{
    GNOME_Development_Symbol *sym;
    
    grow_seq (seq, 1);
    sym = &seq->_buffer[seq->_length - 1];
    gdb_get_expression (inst, sym, expr);
    
    sym->handle = ss->set_size++;
}

void
gdb_add_expression (GdbInstance *inst,
		    GdfLibdryadSymbolSet *ss, 
		    const char *expr)
{
    add_expression_to_seq (inst, ss->symbols, ss, expr);
}

void
read_type (GdbInstance *inst, GNOME_Development_Symbol *sym)
{
    char *line;
    char *p;
    char *cmd_str;

    g_assert (sym->expression);
    
    cmd_str = g_strdup_printf ("ptype %s", sym->expression);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    if (sym->typename) 
	CORBA_free (sym->typename);
    sym->typename = NULL;

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32pre-prompt")) {
	if (line[0] != '\0' && strcmp (line, "\32\32post-prompt")) {
	    char *type;
	    if (!strcmp (line, "\32\32error-begin"))
		line = gdb_read_line (inst);
	    if (!strncmp (line, "type = ", 7))
		line += 7;

	    type = g_strdup (line);

	    /* If the type contains a structure, skip over the structure
	     * definition and append anything after that. */
	    if (line[strlen (line) - 1] == '{') {
		int depth = 1;
		char *newtype;
		type[strlen (line) - 1] = '\0';
		while (depth) {
		    line = gdb_read_line (inst);
		    while (*line && depth) {
			if (*line == '{')
			    depth++;
			if (*line++ == '}')
			    depth--;
		    }
		}
		line++;
		newtype = g_strdup_printf ("%s%s", type, line);
		g_free (type);
		type = newtype;
	    }

	    /* End the type at a colon (C++ inheritance operator) */
	    p = strchr (type, ':');
	    if (p)
		*p = '\0';

	    /* Remove trailing whitespace */
	    p = type + strlen(type);
	    while (isspace (*p--))
		;
	    *(p + 1) = '\0';
	    
	    sym->typename = CORBA_string_dup (type);
	    g_free (type);
	    
	    break;
	}
	line = gdb_read_line (inst);
    }

    g_assert (sym->typename);
    
    sym->expandable = FALSE;
    sym->type = GNOME_Development_TYPE_NORMAL;
    if (!strncmp (sym->typename, "struct", strlen ("struct"))
	|| !strncmp (sym->typename, "class", strlen ("class"))) {
	sym->expandable = TRUE;
	sym->type = GNOME_Development_TYPE_STRUCT;
    }
    if (sym->typename[strlen (sym->typename) - 1] == ']') {
	sym->expandable = TRUE;
	sym->type = GNOME_Development_TYPE_ARRAY;
    }
    if (sym->typename[strlen (sym->typename) - 1] == '*') {
	sym->expandable = TRUE;
	sym->type = GNOME_Development_TYPE_PTR;
    }
}

void
read_value (GdbInstance *inst, GNOME_Development_Symbol *sym) 
{
    char *cmd_str;
    char *line;

    g_assert (!sym->value);
    sym->value = NULL;

    if (sym->type == GNOME_Development_TYPE_STRUCT) {
	sym->value = CORBA_string_dup ("{...}");
	return;
    } else if (sym->type == GNOME_Development_TYPE_ARRAY) {
	sym->value = CORBA_string_dup ("[...]");
	return;
    }

    cmd_str = g_strdup_printf ("output %s", sym->expression);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    line = gdb_read_line (inst);
    while (strcmp (line, "\32\32pre-prompt")) {
	if (!strncmp(line, "\32\32value-begin", strlen("\32\32value-begin"))) {
	    line = gdb_read_line (inst);
	    
	    while (line[0] == '\0' && strcmp (line, "\32\32\error-begin")) {
		line = gdb_read_line (inst);
	    }
	    
	    if (strcmp (line, "\32\32error-begin")) {
		sym->value = CORBA_string_dup (line);
	    } else {
		line = gdb_read_line (inst);
		while (strcmp (line, "\32\32error")) {
		    char *new;
		    new = g_strdup_printf ("%s%s",
					   sym->value ? sym->value : "", 
					   line);
		    if (sym->value) {
			CORBA_free (sym->value);
		    }
		    sym->value = CORBA_string_dup (new);
		    g_free (new);
		    line = gdb_read_line (inst);
		}
		break;
	    }
	    break;
	}
	if (!strcmp (line, "\32\32error-begin")) {
	    line = gdb_read_line (inst);
	    while (strcmp (line, "\32\32error")) {
		char *new;
		new = g_strdup_printf ("%s%s",
				       sym->value ? sym->value : "", 
				       line);
		if (sym->value) {
		    CORBA_free (sym->value);
		}
		sym->value = CORBA_string_dup (new);
		g_free (new);	
		line = gdb_read_line (inst);
	    }
	}
	line = gdb_read_line (inst);
    }
    g_assert (sym->value != NULL);
}

static void
add_child_expression (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
		      GNOME_Development_Symbol *sym, const char *name, const char *expr)
{
    GNOME_Development_Symbol *new_sym;
    GNOME_Development_SymbolSequence *children;

    children = sym->children._value;
    add_expression_to_seq (inst, children, ss, expr);
    new_sym = &(children->_buffer[children->_length - 1]);
    CORBA_free (new_sym->name);
    new_sym->name = CORBA_string_dup (name);
}

void
expand_pointer (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
		GNOME_Development_Symbol *sym)
{
    char *new_name;
    char *new_expr;
    new_name = g_strdup_printf ("*%s", sym->name);
    new_expr = g_strdup_printf ("*(%s)", sym->expression);
    add_child_expression (inst, ss, sym, new_name, new_expr);
    g_free (new_expr);
    g_free (new_name);
}

static char *
get_child_line (GdbInstance *inst)
{
    char *work;
    char *line = gdb_read_line (inst);

    while (1) {
	/* When debugging C++, gdb seperates data values and function values 
	 * with an empty line.  Look for that empty line to end processing.
	 * At some point we could put in support for function values, but I'm 
	 * to lazy to do that just yet */
	for (work = line; isspace (*work); work++)
	    ;
	if (*work == '\0')
	    return NULL;

	if (line[0] == '}') 
	    return NULL;

	/* Access specifiers begin in column 3, whereas variables begin in
	 * column 5.  If it is an access specifier we try the next line, 
	 * otherwise we return this line IF the line is not a static member
	 * of a C++ class. */
	g_assert (strlen (line) > 2);
	if (line[2] == ' ' 
	    && line[4] != ' '
	    && strncmp (line + 4, "static", strlen ("static"))) {
	    return line;
	}


	line = gdb_read_line (inst);
    }
}

static char *
read_child_from_ptype (GdbInstance *inst)
{
    char *name;
    char *work;
    char *line;

    line = get_child_line (inst);
    if (!line) {
	return NULL;
    }

    /* Clean out the trailing ;, and any array indices if necessary */
    work = line + (strlen (line) - 1);
    if (*work == '{') {
	do {
	    line = gdb_read_line (inst);
	    while (isspace (*line++))
		;    
	    line--;
	} while (*line != '}');
    } 

    work = strrchr (line, ';');
    if (work) {
	*work-- = '\0';
    } else {
	work = line + (strlen(line) - 1);
    }    

    while (*work == ']') {
	while (*work-- != '[') 
	    ;
	*(work + 1) = '\0';
    }

    while (*work != '*' && *work-- != ' ' && work != line)
	;
    name = work + 1;
    return g_strdup (name);
}

static GList *
get_ptype_names (GdbInstance *inst, char *type_line, 
		 GList *names, GTree *existing_names)
{
    static char *derive_keywords[] = { "public", "private", "protected",
				       "virtual" };
    char *line;
    char *type_line_copy = g_strdup (type_line + strlen ("type = "));
    char *p;

    char *name;
    
    name = read_child_from_ptype (inst);
    while (name) {
	if (!g_tree_lookup (existing_names, name)) {
	    names = g_list_prepend (names, name);
	    g_tree_insert (existing_names, name, name);
	}
	name = read_child_from_ptype (inst);
    }

    /* Find parent classes and get_ptype_names from them */
    p = strchr (type_line_copy, '{');
    if (p)
	*p = '\0';
    p = strchr (type_line_copy, ':');
    if (p)
	p += 2;
    while (p && *p) {
	char *type;
	int i;
	gboolean is_keyword = FALSE;
	
	while (*p && isspace (*p))
	    p++;
	type = p;
	
	while (*p && !isspace (*p) && *p != ',') 
	    p++;
	if (*p == '\0')
	    p = NULL;
	else 
	    *p = '\0';

	for (i = 0; i < sizeof (derive_keywords) / sizeof (derive_keywords[0]); i++) {
	    if (!strcmp (type, derive_keywords[i])) {
		is_keyword = TRUE;
		break;
	    }
	} 
	
	if (!is_keyword) {
	    char *cmd_str = g_strdup_printf ("ptype %s", type);
	    gdb_execute_command (inst, cmd_str);
	    g_free (cmd_str);
	    
	    do {
		line = gdb_read_line (inst);
	    } while (strncmp (line, "type = ", strlen ("type = ")));
	    
	    names = get_ptype_names (inst, line, names, existing_names);
	}
	if (p)
	    p++;
    }
    
    g_free (type_line_copy);
	    
    return names;
}

void
expand_structure (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
		  GNOME_Development_Symbol *sym)
{
    char *expr;
    char *cmd_str;
    char *line;
    GList *names = NULL;
    GTree *existing_names;
    GList *i;

    cmd_str = g_strdup_printf ("ptype %s", sym->expression);
    gdb_execute_command (inst, cmd_str);
    g_free (cmd_str);

    do {
	line = gdb_read_line (inst);
    } while (strncmp (line, "type = ", strlen ("type = ")));

    existing_names = g_tree_new ((GCompareFunc)strcmp);
    names = get_ptype_names (inst, line, names, existing_names);

    names = g_list_reverse (names);
    for (i = names; i != NULL; i = i->next) {
	g_assert (i != NULL);
	expr = g_strdup_printf ("(%s).%s", sym->expression, (char*)i->data);
	g_assert (i != NULL);
	add_child_expression (inst, ss, sym, i->data, expr);
	g_free (expr);
	g_free (i->data);
    }
    g_list_free (names); 
}

void
expand_array (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
	      GNOME_Development_Symbol *sym)
{
    char *typecpy;
    char *p;
    unsigned size;
    
    typecpy = g_strdup (sym->typename);

    /* isolate the first index size */
    p = strchr (typecpy, ']');
    g_assert (p);
    *p = '\0';
    
    p = strchr (typecpy, '[');
    g_assert (p);

    size = atoi (p + 1);
    
    g_free (typecpy);

    expand_slice (inst, ss, sym, 0, size);
}

void
expand_array_slice (GdbInstance *inst, GdfLibdryadSymbolSet *ss, 
		    GNOME_Development_Symbol *sym)
{
    char *namecpy;
    char *p, *p1;
    unsigned start, end;
    
    
    namecpy = g_strdup (sym->name);

    p = namecpy + 1;
    p1 = strchr (namecpy, '.');
    g_assert (p1);
    *p1 = '\0';
    start = atoi (p);

    p = p1 + 3;
    p1 = p + strlen (p) - 1;
    *p1 = '\0';
    end = atoi (p);

    expand_slice (inst, ss, sym, start, end - start);

    g_free (namecpy);
}

void
add_slice (GdbInstance *inst, GdfLibdryadSymbolSet *ss, GNOME_Development_Symbol *sym,
	   unsigned begin, unsigned size)
{
    char *name;
    GNOME_Development_Symbol *new_sym;
    GNOME_Development_SymbolSequence *seq;

    seq = sym->children._value;

    grow_seq (seq, 1);
    new_sym = &seq->_buffer[seq->_length - 1];   

    name = g_strdup_printf ("[%d...%d]", begin, size + begin - 1);
    new_sym->symbol_set = sym->symbol_set;
    new_sym->handle = ss->set_size++;
    new_sym->name = CORBA_string_dup (name);
    new_sym->expression = CORBA_string_dup (sym->expression);
    new_sym->typename = CORBA_string_dup ("");
    new_sym->cast_to = CORBA_string_dup ("");
    new_sym->type = GNOME_Development_TYPE_ARRAY_SLICE;
    new_sym->value = CORBA_string_dup("");
    new_sym->expandable = TRUE;
    new_sym->expanded = FALSE;
    
    g_free (name);
}

void
expand_slice (GdbInstance *inst, GdfLibdryadSymbolSet *ss, GNOME_Development_Symbol *sym,
	      unsigned start, unsigned num)
{
    int i;
    int slice_begin;
    int slice_size;
    if (num > MAX_ARRAY_SIZE) {
	slice_begin = start;
	
	if (num % NUM_SLICES)
	    slice_size = num / (NUM_SLICES - 1);
	else
	    slice_size = num / NUM_SLICES;
	
	for (i = 0; i < NUM_SLICES - 1; i++, slice_begin += slice_size)
	    add_slice (inst, ss, sym, slice_begin, slice_size);
	if (num - slice_begin)
	    add_slice (inst, ss, sym, slice_begin, num - slice_begin);
    } else {
	for (i = start; i < start + num; i++) {
	    char *expr;
	    char *name;
	    expr = g_strdup_printf ("(%s)[%d]", sym->expression, i);
	    name = g_strdup_printf ("[%d]", i);
	    add_child_expression (inst, ss, sym, name, expr);
	    g_free (expr);
	    g_free (name);
	}
    }
}

static void
destroy_children (GNOME_Development_Symbol *sym)
{
    CORBA_free (sym->children._value);
    sym->children._value = init_seq ();
    sym->expanded = CORBA_FALSE;
}

static GNOME_Development_SymbolSequence *
init_seq (void)
{
    GNOME_Development_SymbolSequence *seq = GNOME_Development_SymbolSequence__alloc ();
    seq->_length = 0;
    seq->_maximum = 0;
    seq->_buffer = NULL;
    /* We tell it not to release now, but will set release to TRUE when the
     * buffer is grown for the first time */
    CORBA_sequence_set_release (seq, CORBA_FALSE);
    return seq;
}

void
grow_seq (GNOME_Development_SymbolSequence *seq, int amount)
{
    int i;

    int old_max = seq->_maximum;
    GNOME_Development_Symbol *old_buf = seq->_buffer;

    seq->_maximum += amount;
    seq->_length += amount;
    seq->_buffer = CORBA_sequence_GNOME_Development_Symbol_allocbuf (seq->_maximum);

    if (old_buf) {
	ORBit_mem_info *block;

	for (i = 0; i < old_max; i++) {
	    GNOME_Development_Symbol *sym = &(seq->_buffer[i]);
	    GNOME_Development_Symbol *old_sym = &old_buf[i];
	    sym->symbol_set = old_sym->symbol_set;
	    sym->handle = old_sym->handle;
	    sym->name = old_sym->name;
	    sym->expression = old_sym->expression;
	    sym->type = old_sym->type;
	    sym->value = old_sym->value;
	    sym->typename = old_sym->typename;
	    sym->cast_to = old_sym->cast_to;
	    sym->expandable = old_sym->expandable;
	    sym->expanded = old_sym->expanded;
	    sym->children = old_sym->children;
	}

	/* This is somewhat not nice.  Basically, we want to free the previous
	 * buffer, but we don't want the deep free that is done by 
	 * CORBA_free.  To get around this we cheat and free the buffer
	 * ourselves */

	block = PTR_TO_MEMINFO (old_buf);
	g_free (block);
    } else {
	/* _release was previously set to FALSE (because there was no buffer).
	 * set it to true now that a buffer has been created. */
	CORBA_sequence_set_release (seq, CORBA_TRUE);
    }

    for (i = seq->_length - amount; i < seq->_length; i++) {
	GNOME_Development_Symbol *sym = &(seq->_buffer[i]);
	sym->symbol_set = -1;
	sym->handle = -1;
	sym->type = GNOME_Development_TYPE_NORMAL;
	sym->cast_to = CORBA_string_dup ("");
	sym->expandable = CORBA_FALSE;
	sym->expanded = CORBA_FALSE;
	sym->children._type = TC_CORBA_sequence_GNOME_Development_Symbol;
	sym->children._value = init_seq ();
	CORBA_any_set_release (&sym->children, CORBA_TRUE);
    }
}
