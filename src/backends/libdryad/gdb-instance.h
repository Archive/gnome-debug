/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * Generic gdb stuff.
 *
 * Author : Dave Camp <campd@oit.edu>
 */

#ifndef __GDB_INSTANCE_H__
#define __GDB_INSTANCE_H__

#include <glib.h>
#include "process.h"

#include "gdb-signal.h"

char *e_strdup_strip (char *string);

/* An instance of gdb */
struct _GdbInstance {
    ChildProcess *proc;

    GdbSignal *signals;

    GdbCommand *first_command;  /* Command Queue */
    GdbCommand *last_command;
    GdbCommand *running_command;

    GTree *breakpoints;

    GdbDisplay **display_array;  /* Displays */
    gint display_array_size;
    gint num_displays;

    gboolean file_loaded : 1;
    gboolean is_running : 1;    /* Loaded and ready to execute */
    gboolean is_executing : 1;  /* Currently executing */
    gboolean is_attached : 1;   /* Attached to an external process */
    gboolean corefile_loaded: 1;
    gboolean at_prompt : 1;
    gboolean at_query : 1;

    gint listener_id;
    double gdb_ver;
};

GdbInstance *gdb_create_instance (void);
void gdb_destroy_instance (GdbInstance *inst);
void gdb_emit_error (GdbInstance *inst, gchar *error_msg);
gboolean gdb_set_working_directory (GdbInstance *inst, const char *dirname);

#endif
