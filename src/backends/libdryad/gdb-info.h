/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/* 
 * Handles the gdb "info" commands
 * 
 * Author : Dave Camp <campd@oit.edu>
 */

#ifndef __GDB_INFO_H__
#define __GDB_INFO_H__

#include <glib.h>
#include "gdb-instance.h"

struct _GdbRegister
{
    char *name;
    char *value;
};


GList *gdb_list_sources (GdbInstance *inst);
gchar *gdb_get_full_source_path (GdbInstance *inst, const gchar *file_name);
char *gdb_get_register_value (GdbInstance *inst, const char *register_name);
void gdb_set_register_value (GdbInstance *inst, const char *register_name,
			     const char *value);
GList *gdb_get_registers (GdbInstance *inst, gboolean floating_pt);
void gdb_destroy_register_list (GdbInstance *inst, GList *regs);

#endif
