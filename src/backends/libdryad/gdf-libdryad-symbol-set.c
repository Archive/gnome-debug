/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: n; c-basic-offset: 4 -*-
 *
 * Implements a GDF Symbol_Set based on Libdryad.
 * 
 * Copyright (C) 1999 Dave Camp <campd@oit.edu>, 
 *                    Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

/** 
 * IMPORTANT NOTE: This symbol_set is ugly, and doesn't really work well.  It
 * is not intended to be used for long; a more permanent solution will be 
 * coming at some point.
 **/

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include <string.h>

#include "gdf-libdryad-symbol-set.h"
#include "libdryad.h"
#include "../../lib/gnome-debug.h"

static CORBA_Object create_libdryad_symbol_set (BonoboObject *object);
static void gdf_libdryad_symbol_set_destroy (GtkObject *object);
static void gdf_libdryad_symbol_set_class_init (GdfLibdryadSymbolSetClass *class);
static void gdf_libdryad_symbol_set_init (GtkObject *object);
static GNOME_Development_Symbol *find_sym_by_id (GdfLibdryadSymbolSet *ss, int sym_id);

static BonoboObjectClass *parent_class;
static POA_GNOME_Development_SymbolSet__epv libdryad_symbol_set_epv;
static POA_GNOME_Development_SymbolSet__vepv libdryad_symbol_set_vepv;

#define CORBA_boolean__alloc() (CORBA_boolean*) CORBA_octet_allocbuf (sizeof (CORBA_boolean))

static inline GdfLibdryadSymbolSet*
libdryad_symbol_set_from_servant (PortableServer_Servant servant)
{
    return GDF_LIBDRYAD_SYMBOL_SET (bonobo_object_from_servant (servant));
}

/* public routines */

GdfLibdryadSymbolSet *
gdf_libdryad_symbol_set_new (GdfLibdryadDebugger *dbg)
{
    GdfLibdryadSymbolSet *ss;
    GNOME_Development_SymbolSet objref;

    ss = gtk_type_new (gdf_libdryad_symbol_set_get_type ());
    objref = create_libdryad_symbol_set (BONOBO_OBJECT (ss));
    if (objref == CORBA_OBJECT_NIL) {
		gtk_object_destroy (GTK_OBJECT (ss));
		return NULL;
    }
    
    bonobo_object_construct (BONOBO_OBJECT (ss), objref);
    
    ss->dbg = dbg;
    ss->event_source = bonobo_event_source_new ();

	bonobo_object_add_interface (BONOBO_OBJECT (ss), 
								 BONOBO_OBJECT (ss->event_source));

    ss->symbols = gdb_allocate_symbols ();
    ss->set_size = 0;
    
	return ss;
}

GtkType 
gdf_libdryad_symbol_set_get_type (void)
{
    static GtkType type = 0;
        
    if (!type) {
		GtkTypeInfo info = {
			"IDL:GDF/Symbol:1.0",
			sizeof (GdfLibdryadSymbolSet),
			sizeof (GdfLibdryadSymbolSetClass),
			(GtkClassInitFunc) gdf_libdryad_symbol_set_class_init,
			(GtkObjectInitFunc) gdf_libdryad_symbol_set_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};
		
		type = gtk_type_unique (bonobo_object_get_type (), &info);
    }
	
    return type;
}

/**
 * gdf_libdryad_symbol_set_changed:
 * @ss: 
 * 
 * Used to notify the symbol set that ss->symbols and ss->size were changed.
 **/
void
gdf_libdryad_symbol_set_changed (GdfLibdryadSymbolSet *ss)
{
	send_event (ss->event_source, "symbol_set_changed", 
				gdf_marshal_event_none ());
}

void
gdf_libdryad_symbol_set_update (GdfLibdryadSymbolSet *ss) 
{
	gdb_update_symbols (ss->dbg->gdb_inst, ss);
}

/* private routines */
CORBA_Object
create_libdryad_symbol_set (BonoboObject *object) 
{
    POA_GNOME_Development_SymbolSet *servant;
    CORBA_Environment ev;
    CORBA_exception_init (&ev);
    
    servant = (POA_GNOME_Development_SymbolSet*)g_new0(BonoboObjectServant, 1);
    servant->vepv = &libdryad_symbol_set_vepv;
    
    POA_GNOME_Development_SymbolSet__init((PortableServer_Servant) servant, &ev);
    if (ev._major != CORBA_NO_EXCEPTION) {
        g_free (servant);
        CORBA_exception_free (&ev);
        return CORBA_OBJECT_NIL;
    }
    CORBA_exception_free (&ev);
    return bonobo_object_activate_servant (object, servant);
}

static void
gdf_libdryad_symbol_set_destroy (GtkObject *object) 
{
    GdfLibdryadSymbolSet *ss = GDF_LIBDRYAD_SYMBOL_SET (object);

    if (ss->event_source) {
		bonobo_object_unref (BONOBO_OBJECT (ss->event_source));
    }

	if (ss->symbols) {
		CORBA_free (ss->symbols);
	}
}

static GNOME_Development_SymbolSequence *
impl_get_symbols (PortableServer_Servant servant, 
				  CORBA_Environment *ev)
{
    GNOME_Development_SymbolSequence *ret;
    GdfLibdryadSymbolSet *ss = libdryad_symbol_set_from_servant (servant);
	
    ret = GNOME_Development_SymbolSequence__alloc ();

    if (ss->symbols != NULL) {
		ret->_length = ss->symbols->_length;
		ret->_maximum = ss->symbols->_maximum;
		ret->_buffer = ss->symbols->_buffer;
		CORBA_sequence_set_release (ret, CORBA_FALSE);
    } else {
		ret->_length = 0;
		ret->_maximum = 0;
		ret->_buffer = NULL;
    }
	
    return ret;
}

static CORBA_long
impl_add_expression (PortableServer_Servant servant,
					 const CORBA_char *expr,
					 CORBA_Environment *ev)
{
    GdfLibdryadSymbolSet *ss = libdryad_symbol_set_from_servant (servant);
    
    gdb_add_expression (ss->dbg->gdb_inst, ss, expr);
    
	send_event (ss->event_source, "symbol_set_changed",
				gdf_marshal_event_none ());

	return (ss->set_size - 1);
}

static GNOME_Development_Symbol *
impl_get_symbol (PortableServer_Servant servant,
				 CORBA_long id,
				 CORBA_Environment *ev)
{
    GNOME_Development_Symbol *ret;
    GNOME_Development_Symbol *sym;
    GdfLibdryadSymbolSet *ss = libdryad_symbol_set_from_servant (servant);
	
    sym = find_sym_by_id (ss, id);
    g_assert (sym);
    
    ret = GNOME_Development_Symbol__alloc ();
    ret->symbol_set = -1;
    ret->name = CORBA_string_dup (sym->name);
    ret->expression = CORBA_string_dup (sym->expression);
    ret->value = CORBA_string_dup (sym->value);
    ret->type = sym->type;
    ret->typename = CORBA_string_dup (sym->typename);
    ret->cast_to = CORBA_string_dup (sym->cast_to);
    ret->expandable = sym->expandable;
	
	ret->expanded = FALSE;
	ret->children._type = sym->children._type;
	ret->children._value = GNOME_Development_SymbolSequence__alloc ();
    
    CORBA_any_set_release (&ret->children, CORBA_FALSE);
    
    return ret;
}

static GNOME_Development_Symbol *
sym_search (GNOME_Development_SymbolSequence *seq, gint id)
{
    int i;
    GNOME_Development_Symbol *sym = NULL;
	
    for (i = 0; i < seq->_length; i++) {
		sym = &seq->_buffer[i];
		if (sym->handle == id) {
			break;
		}
		sym = sym_search (sym->children._value, id);
		if (sym) {
			break;
		}
    }
    return sym;
}

static GNOME_Development_Symbol *
find_sym_by_id (GdfLibdryadSymbolSet *ss, gint id)
{
	return sym_search (ss->symbols, id);
}

static GNOME_Development_SymbolSequence *
impl_get_symbol_children (PortableServer_Servant servant,
					 CORBA_long id,
					 CORBA_Environment *ev)
{
    GNOME_Development_Symbol *sym;
    GdfLibdryadSymbolSet *ss = libdryad_symbol_set_from_servant (servant);
	GNOME_Development_SymbolSequence *children;
	GNOME_Development_SymbolSequence *ret;
	
    ret = GNOME_Development_SymbolSequence__alloc ();

    sym = find_sym_by_id (ss, id);
	
    g_return_val_if_fail (sym->expandable, ret);
    
	if (!sym->expanded) {
		gdb_expand_symbol (ss->dbg->gdb_inst, ss, sym);
	}   

	children = sym->children._value;
	
	ret->_length = children->_length;
	ret->_maximum = children->_maximum;
	ret->_buffer = children->_buffer;
	CORBA_sequence_set_release (ret, CORBA_FALSE);
	
    return ret;
}

static void
impl_set_symbol_value (PortableServer_Servant servant,
					   CORBA_long id,
					   const CORBA_char *value,
					   CORBA_Environment *ev)
{
    GdfLibdryadSymbolSet *ss = libdryad_symbol_set_from_servant (servant);
	GdbSymbolResult res;
	GNOME_Development_Symbol *sym;

    sym = find_sym_by_id (ss, id);
	res = gdb_set_symbol_value (ss->dbg->gdb_inst, sym, value);
	if (res == GDB_SYMBOL_INVALID_TYPE) {
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
								 ex_GNOME_Development_Debugger_InvalidState, NULL);
	}
	
	gdf_libdryad_debugger_update_symsets (ss->dbg, TRUE);
}

static void
impl_cast (PortableServer_Servant servant,
		   CORBA_long id,
		   const CORBA_char *type,
		   CORBA_Environment *ev)
{
    GdfLibdryadSymbolSet *ss = libdryad_symbol_set_from_servant (servant);
	GdbSymbolResult res;
	GNOME_Development_Symbol *sym;

    sym = find_sym_by_id (ss, id);
	res = gdb_cast (ss->dbg->gdb_inst, ss, sym, type);
	if (res == GDB_SYMBOL_INVALID_TYPE) {
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
								 ex_GNOME_Development_Debugger_InvalidState, NULL);
	}
	
	gdf_libdryad_debugger_update_symsets (ss->dbg, TRUE);
}

static void
init_libdryad_symbol_set_corba_class (void) 
{
    /* EPV */
    libdryad_symbol_set_epv.getSymbols = impl_get_symbols;
    libdryad_symbol_set_epv.addExpression = impl_add_expression;
    libdryad_symbol_set_epv.getSymbol = impl_get_symbol;
    libdryad_symbol_set_epv.getSymbolChildren = impl_get_symbol_children;
	libdryad_symbol_set_epv.setSymbolValue = impl_set_symbol_value;
	libdryad_symbol_set_epv.cast = impl_cast;	
   
    /* VEPV */
    libdryad_symbol_set_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
    libdryad_symbol_set_vepv.GNOME_Development_SymbolSet_epv = &libdryad_symbol_set_epv;
}

static void
gdf_libdryad_symbol_set_class_init (GdfLibdryadSymbolSetClass *class) 
{
    GtkObjectClass *object_class = (GtkObjectClass*) class;
    parent_class = gtk_type_class (bonobo_object_get_type ());
    
    object_class->destroy = gdf_libdryad_symbol_set_destroy;
    
    init_libdryad_symbol_set_corba_class ();
}

static void
gdf_libdryad_symbol_set_init (GtkObject *object)
{
#if 0
    GdfLibdryadSymbolSet *ss = GDF_LIBDRYAD_SYMBOL_SET (object);
#endif
}
