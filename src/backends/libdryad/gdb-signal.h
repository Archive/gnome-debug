/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * libdryad signals.
 *
 * Author : Dave Camp <campd@oit.edu>
 */

#ifndef __GDB_SIGNAL_H__
#define __GDB_SIGNAL_H__

#include <glib.h>

struct _GdbSignal {
    gchar *signal_name;
    void (*handler) ();
    gpointer data;
    GdbSignalMarshaller marshaller;
};

void gdb_signal_init (GdbInstance *inst);
void gdb_signal_connect (GdbInstance *inst,
                         gchar *signal_name,
                         void (*handler) (),
                         gpointer data);

void gdb_signal_emit (GdbInstance *inst,
                      gchar *signal_name,
                      ...);

#endif
