/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * Handles loading and unloading of targets.
 * 
 * Author : Dave Camp <campd@oit.edu>
 */

#ifndef __GDB_FILES_H__
#define __GDB_FILES_H__

#include <glib.h>
#include "gdb-instance.h"

gboolean gdb_load_file (GdbInstance *inst, const gchar *bin_filename);
void gdb_unload_file (GdbInstance *inst);

#endif
