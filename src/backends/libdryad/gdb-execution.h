/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/* 
 * Execution of a target
 * 
 * Author : Dave Camp <campd@oit.edu>
 */

#ifndef __GDB_EXECUTION_H__
#define __GDB_EXECUTION_H__

typedef enum {
    GDB_ATTACH_OK,
    GDB_ATTACH_ALREADY_RUNNING,
    GDB_ATTACH_DOESNT_EXIST,
    GDB_ATTACH_ACCESS_DENIED,
    GDB_ATTACH_MISC
} GdbAttachResult;

typedef enum {
    GDB_CORE_OK,
    GDB_CORE_ALREADY_RUNNING,
    GDB_CORE_DOESNT_EXIST,
    GDB_CORE_ACCESS_DENIED,
    GDB_CORE_INVALID_COREFILE,
    GDB_CORE_MISC
} GdbCoreResult;

void gdb_run (GdbInstance *inst, const char *args);
GdbAttachResult gdb_attach (GdbInstance *inst, int pid);
void gdb_detach (GdbInstance *inst);
GdbCoreResult gdb_load_core (GdbInstance *inst, const char *filename);
void gdb_unload_core (GdbInstance *inst);
void gdb_continue (GdbInstance *inst);
void gdb_step_into (GdbInstance *inst);
void gdb_step_over (GdbInstance *inst);
void gdb_step_out (GdbInstance *inst);
void gdb_stop (GdbInstance *inst);

void gdb_tty_set (GdbInstance *inst, const char *tty_name);

#endif
