#include <stdio.h>

enum NumEnum {
    ZERO,
    ONE,
    TWO,
    THREE
};  

struct a 
{
    int x;
    char y;
    float z;
    char str;
    int f;
    int r;
    char t;

    struct a* child;
};

struct b 
{
    struct a i;
    int j, k;
    struct 
    {
	int q;
	int r;
	struct 
	{
	    int s;
	} m;
    } l;
    int z [15];
};

typedef struct 
{
    int a;
} C;

void no_locals_test ()
{
    printf ("testing...\n");
}

void function_call_test ()
{
    struct b var1;
    int b = 3;
    int j = 1;
    int c = 66;

    no_locals_test ();

    printf ("hello world, from inside a function!\n");
    printf ("I'm getting pretty sick of writing hello, world.\n");
}


int
main (int argc, char *argv[]) 
{
    int i;
    int j;

    enum NumEnum e = TWO;
    
    struct a var1;
    struct a *p1 = &var1;
    struct b var2;
    struct b* p2 = &var2;

    C c;

    int bigra [9999] = { 1, 2, 3, 4, 0 };
    int ra1[15] = { 0 };
    struct a ra2[5] = { 0 };
    struct a ra3[5][3][5][9] = { 0 };
    int *ip = &i;

    *ip = 3;
    
    i = 5;
    j = 10;
    i = j = 15;
    
    function_call_test ();
    
    printf ("hello, world\n");
    
    while (1) 
	;
    
    return 0;
}
