/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <gnome.h>
#include "gdf-locals-viewer.h"

static void locals_viewer_class_init (GdfLocalsViewerClass *klass);
static void locals_viewer_init (GdfLocalsViewer *lv);
static void locals_viewer_destroy (GtkObject *object);

static GdfVariableViewerClass *parent_class;

/* 
 * Public interface 
 */

GtkType
gdf_locals_viewer_get_type (void)
{
	static GtkType locals_viewer_type = 0;
	
	if (!locals_viewer_type) {
		static const GtkTypeInfo locals_viewer_info = {
			"GdfLocalsViewer",
			sizeof (GdfLocalsViewer),
			sizeof (GdfLocalsViewerClass),
			(GtkClassInitFunc) locals_viewer_class_init,
			(GtkObjectInitFunc) locals_viewer_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};
		
		locals_viewer_type = gtk_type_unique (gdf_variable_viewer_get_type (),
                                              &locals_viewer_info);
	}
	
	return locals_viewer_type;
}

GdfLocalsViewer *
gdf_locals_viewer_new (void)
{
	GdfLocalsViewer *lv;
	
	lv = gtk_type_new (gdf_locals_viewer_get_type ());
    
    gdf_variable_viewer_view_locals (GDF_VARIABLE_VIEWER (lv));
	
	return lv;
}

/*
 * Class/object functions.
 */

void
locals_viewer_class_init (GdfLocalsViewerClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *)klass;
	
	parent_class = gtk_type_class (gdf_variable_viewer_get_type ());
    
	object_class->destroy = locals_viewer_destroy;
}

void
locals_viewer_init (GdfLocalsViewer *lv)
{
    gtk_object_set (GTK_OBJECT (lv), "show_entry", (gboolean)FALSE, NULL);
}

void 
locals_viewer_destroy (GtkObject *obj)
{
	g_return_if_fail (obj != NULL);
	
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (obj);
}

