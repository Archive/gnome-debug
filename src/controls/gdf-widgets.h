#ifndef __GNOME_Development_WIDGETS_H__
#define __GNOME_Development_WIDGETS_H__

#include "gdf-breakpoint-manager.h"
#include "gdf-locals-viewer.h"
#include "gdf-output-terminal.h"
#include "gdf-register-viewer.h"
#include "gdf-source-viewer-manager.h"
#include "gdf-stack-browser.h"
#include "gdf-variable-viewer.h"

#endif
