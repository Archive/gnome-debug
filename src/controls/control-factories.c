/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gdl/gdl.h>
#include "control-factories.h"
#include "gdf-breakpoint-manager.h"
#include "gdf-register-viewer.h"
#include "gdf-locals-viewer.h"
#include "gdf-output-terminal.h"
#include "gdf-stack-browser.h"
#include "../lib/gdf.h"

#include <bonobo.h>
#include <bonobo/bonobo-shlib-factory.h>
#include <liboaf/liboaf.h>

enum {
    PROP_DEBUGGER_IOR = 1,
    PROP_SHOW_ENTRY
};

static BonoboObject *bpm_factory (BonoboGenericFactory *fact,
                                  void *closure);
static BonoboObject *sb_factory (BonoboGenericFactory *fact,
                                 void *closure);
static BonoboObject *rv_factory (BonoboGenericFactory *fact,
                                void *closure);
static BonoboObject *lv_factory (BonoboGenericFactory *fact, void *closure);
static BonoboObject *vv_factory (BonoboGenericFactory *fact, void *closure);
static BonoboObject *ot_factory (BonoboGenericFactory *fact, void *closure);   

static void set_prop (BonoboPropertyBag *bag,
                      const BonoboArg *arg,
                      guint arg_id,
                      CORBA_Environment *ev,
                      gpointer user_data);
static void get_prop (BonoboPropertyBag *bag,
                      BonoboArg *arg,
                      guint arg_id,
                      CORBA_Environment *ev,
                      gpointer user_data);
static GdfDebuggerClient *get_debugger (gchar *ior);
GdfBreakpointSetClient *get_breakpoint_set (gchar *ior);

static BonoboObject *
control_factory (BonoboGenericFactory *factory, 
                 const char *component_id,
                 gpointer closure)
{
    if (!strcmp (component_id, "OAFIID:GNOME_Development_BreakpointManager"))
        return bpm_factory (factory, closure);
    if (!strcmp (component_id, "OAFIID:GNOME_Development_StackBrowser"))
        return sb_factory (factory, closure);
    if (!strcmp (component_id, "OAFIID:GNOME_Development_RegisterViewer"))
        return rv_factory (factory, closure);
    if (!strcmp (component_id, "OAFIID:GNOME_Development_LocalsViewer"))
        return lv_factory (factory, closure);
    if (!strcmp (component_id, "OAFIID:GNOME_Development_VariableViewer"))
        return vv_factory (factory, closure);
    if (!strcmp (component_id, "OAFIID:GNOME_Development_OutputTerminal"))
        return ot_factory (factory, closure);
    return NULL;
}

static BonoboObject *
bpm_factory (BonoboGenericFactory *fact,
			 void *closure)
{
	BonoboPropertyBag *pb;
	BonoboControl *control;
	GdfBreakpointManager *bpm;

	/* Create the control */
	bpm = gdf_breakpoint_manager_new ();
	gtk_widget_show (GTK_WIDGET (bpm));
	
	control = bonobo_control_new (GTK_WIDGET (bpm));
	
	/* Create the properties */
	pb = bonobo_property_bag_new (get_prop, set_prop, bpm);
	bonobo_control_set_properties (control, pb);
	bonobo_property_bag_add (pb, "debugger-ior", PROP_DEBUGGER_IOR,
                             BONOBO_ARG_STRING, NULL, 
                             "Object reference of the debugging backend", 0);

	return BONOBO_OBJECT (control);
}

static BonoboObject *
sb_factory (BonoboGenericFactory *fact,
            void *closure)
{
	BonoboPropertyBag *pb;
	BonoboControl *control;
	GtkWidget *sb;

	/* Create the control */
	sb = gdf_stack_browser_new ();
	gtk_widget_show (GTK_WIDGET (sb));
	
	control = bonobo_control_new (GTK_WIDGET (sb));
	
	/* Create the properties */
	pb = bonobo_property_bag_new (get_prop, set_prop, sb);
	bonobo_control_set_properties (control, pb);
	bonobo_property_bag_add (pb, "debugger-ior", PROP_DEBUGGER_IOR,
                             BONOBO_ARG_STRING, NULL, 
                             "Object reference of the debugging backend", 0);
	
	return BONOBO_OBJECT (control);
}

static BonoboObject *
rv_factory (BonoboGenericFactory *fact,
            void *closure)
{
	BonoboPropertyBag *pb;
	BonoboControl *control;
	GtkWidget *rv;
    
	/* Create the control */
	rv = gdf_register_viewer_new ();
	gtk_widget_show (GTK_WIDGET (rv));
	
	control = bonobo_control_new (GTK_WIDGET (rv));
	
	/* Create the properties */
	pb = bonobo_property_bag_new (get_prop, set_prop, rv);
	bonobo_control_set_properties (control, pb);
	bonobo_property_bag_add (pb, "debugger-ior", PROP_DEBUGGER_IOR,
                             BONOBO_ARG_STRING, NULL, 
                             "Object reference of the debugging backend", 0);
	
	return BONOBO_OBJECT (control);
}

static BonoboObject *
lv_factory (BonoboGenericFactory *fact,
            void *closure)
{
	BonoboPropertyBag *pb;
	BonoboControl *control;
	GtkWidget *lv;

	/* Create the control */
	lv = GTK_WIDGET (gdf_locals_viewer_new ());
	gtk_widget_show (GTK_WIDGET (lv));
	
	control = bonobo_control_new (GTK_WIDGET (lv));
	
	/* Create the properties */
	pb = bonobo_property_bag_new (get_prop, set_prop, lv);
	bonobo_control_set_properties (control, pb);
	bonobo_property_bag_add (pb, "debugger-ior", PROP_DEBUGGER_IOR,
                             BONOBO_ARG_STRING, NULL, 
                             "Object reference of the debugging backend", 0);
	
	return BONOBO_OBJECT (control);
}

static BonoboObject *
vv_factory (BonoboGenericFactory *fact,
            void *closure)
{
	BonoboPropertyBag *pb;
	BonoboControl *control;
	GtkWidget *vv;
    BonoboArg *def;

	/* Create the control */
	vv = GTK_WIDGET (gdf_variable_viewer_new ());
	gtk_widget_show (GTK_WIDGET (vv));
	
	control = bonobo_control_new (GTK_WIDGET (vv));
	
	/* Create the properties */
	pb = bonobo_property_bag_new (get_prop, set_prop, vv);
	bonobo_control_set_properties (control, pb);
	bonobo_property_bag_add (pb, "debugger-ior", PROP_DEBUGGER_IOR,
                             BONOBO_ARG_STRING, NULL, 
                             "Object reference of the debugging backend", 
                             BONOBO_PROPERTY_READABLE 
                             | BONOBO_PROPERTY_WRITEABLE);
    

    def = bonobo_arg_new (BONOBO_ARG_BOOLEAN);
    BONOBO_ARG_SET_BOOLEAN (def, TRUE);
	bonobo_property_bag_add (pb, "show_entry", PROP_SHOW_ENTRY,
                             BONOBO_ARG_BOOLEAN, def,
                             "Show the \"Add symbol\" entry", 
                             BONOBO_PROPERTY_READABLE 
                             | BONOBO_PROPERTY_WRITEABLE);
    bonobo_arg_release (def);

	return BONOBO_OBJECT (control);
}

static void
set_prop (BonoboPropertyBag *bag,
          const BonoboArg *arg,
          guint arg_id,
          CORBA_Environment *ev,
          gpointer user_data)
{
    GtkObject *obj = user_data;

    switch (arg_id) {
    case PROP_DEBUGGER_IOR :
    {
        GdfDebuggerClient *dbg;
        char *s;
        s = BONOBO_ARG_GET_STRING (arg);
        
        if (strcmp (s, ""))
            dbg = get_debugger (s);
        else
            dbg = NULL;
        
        gtk_object_set (GTK_OBJECT (obj), "debugger", 
                        (GtkObject *)dbg, NULL);
        if (dbg)
            gtk_object_unref (GTK_OBJECT (dbg));
        break;
    }
    case PROP_SHOW_ENTRY :
    {
        gtk_object_set (GTK_OBJECT (obj), "show_entry",
                        BONOBO_ARG_GET_BOOLEAN (arg), NULL);
        break;
    }
    
    }
}

static BonoboObject *
ot_factory (BonoboGenericFactory *fact,
            void *closure)
{
    BonoboPropertyBag *pb;
    BonoboControl *control;
    GtkWidget *ot;

    /* Create the control */
    ot = GTK_WIDGET (gdf_output_terminal_new ());
    gtk_widget_show (GTK_WIDGET (ot));
    
    control = bonobo_control_new (GTK_WIDGET (ot));
    
    /* Create the properties */
    pb = bonobo_property_bag_new (get_prop, set_prop, ot);
    bonobo_control_set_properties (control, pb);
    bonobo_property_bag_add (pb, "debugger-ior", PROP_DEBUGGER_IOR,
                             BONOBO_ARG_STRING, NULL,
                             "Object reference of the debugging backend",
                             BONOBO_PROPERTY_READABLE
                             | BONOBO_PROPERTY_WRITEABLE);

    return BONOBO_OBJECT (control);
}

static void
get_prop (BonoboPropertyBag *bag,
          BonoboArg *arg,
          guint arg_id,
          CORBA_Environment *ev,
          gpointer user_data)
{
    GtkObject *obj = user_data;
    
    switch (arg_id) {
    case PROP_DEBUGGER_IOR :
    {
        GdfDebuggerClient *dbg;
        char *ior;
        GtkArg args[1];
        args[0].name = "debugger";
        gtk_object_getv (obj, 1, args);
        g_assert (args[0].type == GTK_TYPE_OBJECT);
        if (GTK_VALUE_OBJECT (args[0]))
            dbg = GDF_DEBUGGER_CLIENT (GTK_VALUE_OBJECT (args[0]));
        else 
            dbg = NULL;
        
        if (dbg) {
            CORBA_Environment ev;
            CORBA_exception_init (&ev);
            ior = CORBA_ORB_object_to_string (oaf_orb_get (), 
                                              dbg->objref, &ev);
            CORBA_exception_free (&ev);
        } else {
            ior = CORBA_string_dup ("");
        }
        
        BONOBO_ARG_SET_STRING (arg, ior);
        CORBA_free (ior);
        break;
    }
    case PROP_SHOW_ENTRY :
    {
        GtkArg args[1];
        args[0].name = "show_entry";
        gtk_object_getv (obj, 1, args);
        g_assert (args[0].type == GTK_TYPE_BOOL);
        BONOBO_ARG_SET_BOOLEAN (arg, GTK_VALUE_BOOL (args[0]));
        break;
    }
    }
}



static GdfDebuggerClient *
get_debugger (gchar *ior)
{
	CORBA_Object objref;
	GdfDebuggerClient *dbg;
	CORBA_Environment ev;

    static GHashTable *active_debuggers = NULL;
	
    if (ior[0] == '\0')
        return NULL;

	if (!active_debuggers) 
		active_debuggers = g_hash_table_new (g_str_hash, g_str_equal);

	dbg = (GdfDebuggerClient *)g_hash_table_lookup (active_debuggers, ior);
	
	if (!dbg) {
		CORBA_exception_init (&ev);
		objref = CORBA_ORB_string_to_object (oaf_orb_get (), ior, &ev);
		/* FIXME: Check for exceptions */
		CORBA_exception_free (&ev);
	
		dbg = gdf_debugger_client_new_from_corba (objref);
		g_hash_table_insert (active_debuggers, (gpointer)ior, (gpointer)dbg);
	} else {
        gtk_object_ref (GTK_OBJECT (dbg));
    }
	
	return dbg;
}

GdfBreakpointSetClient *
get_breakpoint_set (gchar *ior)
{
	CORBA_Object objref;
	GdfBreakpointSetClient *bs;
    CORBA_Environment ev;

    static GHashTable *active = NULL;
	
    if (ior[0] == '\0')
        return NULL;

	if (!active) 
		active = g_hash_table_new (g_str_hash, g_str_equal);

	bs = (GdfBreakpointSetClient *)g_hash_table_lookup (active, ior);
	
	if (!bs) {
		CORBA_exception_init (&ev);
		objref = CORBA_ORB_string_to_object (oaf_orb_get (), ior, &ev);
		/* FIXME: Check for exceptions */
		CORBA_exception_free (&ev);
	
		bs = gdf_breakpoint_set_client_new_from_corba (objref);
		g_hash_table_insert (active, (gpointer)ior, (gpointer)bs);
	} else {
        gtk_object_ref (GTK_OBJECT (bs));
    }
	
	return bs;
}
    
BONOBO_OAF_SHLIB_FACTORY_MULTI ("OAFIID:GNOME_Development_DebuggerControlFactory",
                                "Factory for the gnome-debug controls.",
                                control_factory, NULL);
