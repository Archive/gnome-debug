/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free softbreakpoitware; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <gnome.h>

#include "gdf-breakpoint-manager.h"

#include <gdl/gdl-tools.h>

#include <gal/widgets/e-canvas.h>
#include <gal/widgets/e-popup-menu.h>
#include <gal/e-table/e-table-header-item.h>
#include <gal/e-table/e-table-item.h>
#include <gal/util/e-util.h>
#include <gal/e-table/e-table.h>
#include <gal/e-table/e-table-header-item.h>
#include <gal/e-table/e-table-item.h>
#include <gal/e-table/e-table-scrolled.h>
#include <gal/e-table/e-table-simple.h>
#include <gal/e-table/e-cell-text.h>
#include <gal/e-table/e-cell-checkbox.h>

struct _GdfBreakpointManagerPriv {
    ETableModel *e_table_model;
	GtkWidget *e_table;
	
    GPtrArray *breakpoints;
    GHashTable *rows;

	/* signal identifiers */
	guint program_unloaded_sig;
	guint breakpoint_set_sig;
	guint breakpoint_disabled_sig;
	guint breakpoint_enabled_sig;
	guint breakpoint_deleted_sig;

	GdfDebuggerClient *dbg;
    GdfBreakpointSetClient *bs;
};

enum {
    ARG_0,
    ARG_DEBUGGER,
    ARG_BREAKPOINT_SET
};

enum {
    COL_NUM,
    COL_ENABLED,
    COL_ADDRESS,
    COL_LINE,
    COL_FILE,
    COL_FUNCTION,
    LAST_COL
};

static void breakpoint_manager_class_init (GdfBreakpointManagerClass *klass);
static void breakpoint_manager_init (GdfBreakpointManager *bpm);
static void breakpoint_manager_destroy (GtkObject *object);
static void get_arg (GtkObject *object, 
                     GtkArg *arg, 
                     guint arg_id);
static void set_arg (GtkObject *object, 
                     GtkArg *arg, 
                     guint arg_id);
static void create_children (GdfBreakpointManager *bpm);
static void init_breakpoint_model (GdfBreakpointManager *bpm);
static void destroy_breakpoint_model (GdfBreakpointManager *bpm);
static void clear_breakpoint_manager (GdfBreakpointManager *bpm);
static void get_debugger_breakpoints (GdfBreakpointManager *bpm);
static void connect_source_signals (GdfBreakpointManager *bpm);
static void disconnect_source_signals (GdfBreakpointManager *bpm);
static gint on_right_click_cb (ETable *table, gint row, gint col,
                               GdkEventButton *event,
                               GdfBreakpointManager *bpm);
static void delete_breakpoint_selected_cb (GtkWidget *menu,
                                           void *data);
static void breakpoint_set_cb (GtkObject *obj, gint bp_num, 
							   GdfBreakpointManager *bpm);
static void breakpoint_enabled_cb (GtkObject *obj, gint bp_num, 
								   GdfBreakpointManager *bpm);
static void breakpoint_disabled_cb (GtkObject *obj, gint bp_num, 
									GdfBreakpointManager *bpm);
static void breakpoint_deleted_cb (GtkObject *obj, gint bp_num, 
								   GdfBreakpointManager *bpm);

static GnomeUIInfo breakpoint_uiinfo[] = {
    { GNOME_APP_UI_ITEM, N_ ("Delete"),
      N_("Delete the breakpoint"), delete_breakpoint_selected_cb,
      NULL, NULL, 0, 0, 0, 0 },
    GNOMEUIINFO_END
};

typedef struct {
    GdfBreakpointManager *bpm;
    int row;
} BreakpointManagerMenuData;

static GtkFrameClass *parent_class;

const char *breakpoint_manager_spec = 
"<ETableSpecification click-to-add=\"false\" draw-grid=\"true\">"
"  <ETableColumn model_col=\"0\" _title=\"#\" expansion=\"0.0\" minimum_width=\"20\" resizable=\"false\" cell=\"string\" compare=\"string\"/>"
"  <ETableColumn model_col=\"1\" _title=\"Enbl\" expansion=\"0.0\" minimum_width=\"35\" resizable=\"false\" cell=\"checkbox\" compare=\"integer\"/>"
"  <ETableColumn model_col=\"2\" _title=\"Address\" expansion=\"0.25\" minimum_width=\"20\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableColumn model_col=\"3\" _title=\"Line\" expansion=\"0.25\" minimum_width=\"20\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableColumn model_col=\"4\" _title=\"File\" expansion=\"1.0\" minimum_width=\"20\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableColumn model_col=\"5\" _title=\"Function\" expansion=\"1.0\" minimum_width=\"20\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableState>"
"    <column source=\"0\"/>"
"    <column source=\"1\"/>"
"    <column source=\"3\"/>"
"    <column source=\"4\"/>"
"    <column source=\"5\"/>"
"    <grouping></grouping>"
"  </ETableState>"
"</ETableSpecification>";

/* 
 * Public interface 
 */

GtkType
gdf_breakpoint_manager_get_type (void)
{
	static GtkType breakpoint_manager_type = 0;
	
	if (!breakpoint_manager_type) {
		static const GtkTypeInfo breakpoint_manager_info = {
			"GdfBreakpointManager",
			sizeof (GdfBreakpointManager),
			sizeof (GdfBreakpointManagerClass),
			(GtkClassInitFunc) breakpoint_manager_class_init,
			(GtkObjectInitFunc) breakpoint_manager_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};
		
		breakpoint_manager_type = gtk_type_unique (gtk_frame_get_type (),
												   &breakpoint_manager_info);
	}
	
	return breakpoint_manager_type;
}

/**
 * gdf_breakpoint_manager_new:
 * 
 * Creates a new breakpoint manager.
 * 
 * Return value: An initialized breakpoint manager.
 **/
GdfBreakpointManager *
gdf_breakpoint_manager_new (void)
{
	GdfBreakpointManager *bpm;
	
	bpm = gtk_type_new (gdf_breakpoint_manager_get_type ());
	
	return bpm;
}

/**
 * gdf_breakpoint_manager_set_debugger:
 * @bpm: The breakpoint manager.
 * @dbg: The debugger to associate.
 * 
 * Sets the breakpoint manager's associated debugger.  The new debugger will
 * be ref'ed by the breakpoint manager.  Any previous debugger will be 
 * unref'ed.
 **/
void
gdf_breakpoint_manager_set_debugger (GdfBreakpointManager *bpm,
                                     GdfDebuggerClient *dbg)
{
	g_return_if_fail (bpm != NULL);
	g_return_if_fail (GNOME_Development_IS_BREAKPOINT_MANAGER (bpm));

    if (bpm->priv->bs) {
        g_warning ("Attempted to use a breakpoint manager to view both a BreakpointSet and a Debugger");
        return;
    }

	if (bpm->priv->dbg) {
		disconnect_source_signals (bpm);
		clear_breakpoint_manager (bpm);
		gtk_object_unref (GTK_OBJECT (bpm->priv->dbg));
	}
	
	bpm->priv->dbg = dbg;

    if (dbg) {
        gtk_object_ref (GTK_OBJECT (dbg));
        if (GTK_OBJECT_FLOATING (GTK_OBJECT (dbg))) 
            gtk_object_sink (GTK_OBJECT (dbg));
        
        connect_source_signals (bpm);
        get_debugger_breakpoints (bpm);
    }
}

void
gdf_breakpoint_manager_set_breakpoint_set (GdfBreakpointManager *bpm,
                                           GdfBreakpointSetClient *bs)
{
	g_return_if_fail (bpm != NULL);
	g_return_if_fail (GNOME_Development_IS_BREAKPOINT_MANAGER (bpm));

    GDL_TRACE ();

    if (bpm->priv->dbg) {
        g_warning ("Attempted to use a breakpoint manager to view both a BreakpointSet and a Debugger");
        return;
    }

	if (bpm->priv->bs) {
		disconnect_source_signals (bpm);
		clear_breakpoint_manager (bpm);
		gtk_object_unref (GTK_OBJECT (bpm->priv->bs));
	}
	
	bpm->priv->bs = bs;

    if (bs) {
        gtk_object_ref (GTK_OBJECT (bs));
        if (GTK_OBJECT_FLOATING (GTK_OBJECT (bs))) 
            gtk_object_sink (GTK_OBJECT (bs));
        
        connect_source_signals (bpm);
        get_debugger_breakpoints (bpm);
    }
}


/*
 * Class/object functions.
 */

void
breakpoint_manager_class_init (GdfBreakpointManagerClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *)klass;
	
    gtk_object_add_arg_type ("GdfBreakpointManager::debugger",
                             GTK_TYPE_OBJECT,
                             GTK_ARG_READWRITE,
                             ARG_DEBUGGER);
    gtk_object_add_arg_type ("GdfBreakpointManager::breakpoint_set",
                             GTK_TYPE_OBJECT,
                             GTK_ARG_READWRITE,
                             ARG_BREAKPOINT_SET);

	parent_class = gtk_type_class (gtk_frame_get_type ());

	object_class->destroy = breakpoint_manager_destroy;
    object_class->get_arg = get_arg;
    object_class->set_arg = set_arg;
}

void
breakpoint_manager_init (GdfBreakpointManager *bpm)
{
	bpm->priv = g_new0 (GdfBreakpointManagerPriv, 1);

	bpm->priv->dbg = NULL;
    bpm->priv->bs = NULL;
    
    init_breakpoint_model (bpm);

	create_children (bpm);
}

void 
breakpoint_manager_destroy (GtkObject *obj)
{
	GdfBreakpointManager *bpm = GNOME_Development_BREAKPOINT_MANAGER (obj);
	
	g_return_if_fail (obj != NULL);

	destroy_breakpoint_model (bpm);

	if (bpm->priv->dbg)
		gtk_object_unref (GTK_OBJECT (bpm->priv->dbg));
    if (bpm->priv->bs)
        gtk_object_unref (GTK_OBJECT (bpm->priv->dbg));

	g_free (bpm->priv);
	
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (obj);
}

void 
get_arg (GtkObject *object,
         GtkArg *arg, 
         guint arg_id)
{
    GdfBreakpointManager *bpm = GNOME_Development_BREAKPOINT_MANAGER (object);
    
    switch (arg_id) {
    case ARG_DEBUGGER :
        GTK_VALUE_OBJECT (*arg) = GTK_OBJECT (bpm->priv->dbg);
        break;
    case ARG_BREAKPOINT_SET :
        GTK_VALUE_OBJECT (*arg) = GTK_OBJECT (bpm->priv->bs);
    default :
        arg->type = GTK_TYPE_INVALID;
    }   
}

void 
set_arg (GtkObject *object, 
         GtkArg *arg, 
         guint arg_id)
{
    GdfBreakpointManager *bpm = GNOME_Development_BREAKPOINT_MANAGER (object);
    
    GDL_TRACE ();

    switch (arg_id) {
    case ARG_DEBUGGER :
        gdf_breakpoint_manager_set_debugger (bpm,
                                             GTK_VALUE_OBJECT (*arg) ? GDF_DEBUGGER_CLIENT (GTK_VALUE_OBJECT (*arg)) : NULL);
        break;
    case ARG_BREAKPOINT_SET :
        gdf_breakpoint_manager_set_breakpoint_set (bpm, 
                                                   GTK_VALUE_OBJECT (*arg) ? GDF_BREAKPOINT_SET_CLIENT (GTK_VALUE_OBJECT (*arg)) : NULL);
        break;
    default :
        break;
    }   
}

/* 
 * ETable Model Funcs 
 */

static int
col_count (ETableModel *etm, void *data)
{
    return LAST_COL;
}

static int
row_count (ETableModel *etm, void *data)
{
    GdfBreakpointManager *bpm = data;
    return bpm->priv->breakpoints->len;
}

static void *
value_at (ETableModel *etm, int col, int row, void *data)
{
    GdfBreakpointManager *bpm = data;
    GNOME_Development_Breakpoint *bp;
    void *ret = NULL;
    
    static char *stored_val = NULL;
    if (stored_val) {
        g_free (stored_val);
        stored_val = 0;
    }
 
    bp = g_ptr_array_index (bpm->priv->breakpoints, row);
    if (bp) {
        switch (col) {
        case COL_NUM :
            ret = stored_val = g_strdup_printf ("%d", bp->num);
            break;
        case COL_ENABLED :
            ret = GINT_TO_POINTER (bp->enabled ? TRUE : FALSE);
            break;
        case COL_ADDRESS :
            ret = stored_val = g_strdup (bp->address);
            break;
        case COL_LINE :
            ret = stored_val = g_strdup_printf ("%d", bp->line_num);
            break;
        case COL_FILE :
            ret = stored_val = g_strdup (bp->file_name);
            break;
        case COL_FUNCTION :
            ret = stored_val = g_strdup (bp->function);
            break;
        default :
            g_assert_not_reached ();
        }
    } else {
        g_warning ("Could not get breakpoint info.");
        ret = NULL;
    }

    return ret;
}

static void
set_value_at (ETableModel *etm, int col, int row, const void *val, void *data)
{
    GdfBreakpointManager *bpm = data;
    if (col == COL_ENABLED) {
        GNOME_Development_Breakpoint *bp = g_ptr_array_index (bpm->priv->breakpoints, row);
        if (bp->enabled) {
            if (bpm->priv->dbg) {
                gdf_debugger_client_disable_breakpoint (bpm->priv->dbg,
                                                        bp->num);
            } else if (bpm->priv->bs) {
                gdf_breakpoint_set_client_disable_breakpoint (bpm->priv->bs,
                                                              bp->num);
            }
        } else {
            if (bpm->priv->dbg) {
                gdf_debugger_client_enable_breakpoint (bpm->priv->dbg,
                                                       bp->num);
            } else if (bpm->priv->bs) {
                gdf_breakpoint_set_client_enable_breakpoint (bpm->priv->bs,
                                                             bp->num);
            }
        }    
    } else {
        g_warning ("Tried to set the value of an uneditable column");
    }   

}

static gboolean 
is_cell_editable (ETableModel *etm, int col, int row, void *data)
{
    return (col == COL_ENABLED);
}

static gboolean 
value_is_empty (ETableModel *etm, int col, const void *value, void *data)
{
    return !(value && *(char*)value);
}

static char *
value_to_string (ETableModel *etm, int col, const void *val, void *data)
{
    if (col == COL_ENABLED) {
        return g_strdup (val ? _("Y") : _("N"));
    } else {
        return g_strdup (val);
    }
}

void
create_children (GdfBreakpointManager *bpm)
{   
    bpm->priv->e_table_model
        = e_table_simple_new (col_count, row_count, NULL, value_at, set_value_at, 
                              is_cell_editable, NULL, NULL, NULL, NULL, NULL, 
                              value_is_empty, value_to_string, bpm);
                             
    bpm->priv->e_table = e_table_scrolled_new (bpm->priv->e_table_model, 
                                               NULL, breakpoint_manager_spec, 
                                               NULL);

    gtk_signal_connect (GTK_OBJECT (e_table_scrolled_get_table (bpm->priv->e_table)), "right_click",
                        GTK_SIGNAL_FUNC (on_right_click_cb), bpm);

    gtk_widget_show (bpm->priv->e_table);
    gtk_container_add (GTK_CONTAINER (bpm), bpm->priv->e_table);
}


void
init_breakpoint_model (GdfBreakpointManager *bpm)
{
    bpm->priv->breakpoints = g_ptr_array_new ();
    bpm->priv->rows = g_hash_table_new (g_direct_hash, g_direct_equal);
}

void
destroy_breakpoint_model (GdfBreakpointManager *bpm)
{
    int i;
    for (i = 0; i < bpm->priv->breakpoints->len; i++) {
        CORBA_free (g_ptr_array_index (bpm->priv->breakpoints, i));
    }
    g_ptr_array_free (bpm->priv->breakpoints, TRUE);
    g_hash_table_destroy (bpm->priv->rows);
}

void 
clear_breakpoint_manager (GdfBreakpointManager *bpm)
{
    GDL_TRACE ();

    e_table_model_pre_change (bpm->priv->e_table_model);

    destroy_breakpoint_model (bpm);
    init_breakpoint_model (bpm);

    e_table_model_changed (bpm->priv->e_table_model);
}

void
get_debugger_breakpoints (GdfBreakpointManager *bpm)
{
    int i;
    GNOME_Development_BreakpointList *breakpoints;
    GdfDebuggerClientResult res = -1;

    e_table_model_pre_change (bpm->priv->e_table_model);

    if (bpm->priv->dbg) {
        res = gdf_debugger_client_get_breakpoints (bpm->priv->dbg,
                                                   &breakpoints);
    } else if (bpm->priv->bs) {
        res = gdf_breakpoint_set_client_get_breakpoints (bpm->priv->bs,
                                                         &breakpoints);
    }
    
    if (res == GDF_DEBUGGER_CLIENT_OK) {
        for (i = 0; i < breakpoints->_length; i++) {   
            GNOME_Development_Breakpoint *bp = &breakpoints->_buffer[i];
            g_ptr_array_add (bpm->priv->breakpoints, bp);
            g_hash_table_insert (bpm->priv->rows,
                                 GINT_TO_POINTER (bp->num), 
                                 GINT_TO_POINTER (bpm->priv->breakpoints->len - 1));
        } 
    }
    
    if (breakpoints)
        CORBA_free (breakpoints);
    
    e_table_model_changed (bpm->priv->e_table_model);
}

void
connect_source_signals (GdfBreakpointManager *bpm)
{
    GtkObject *obj;

    if (bpm->priv->dbg) {
        GDL_TRACE ();
        obj = GTK_OBJECT (bpm->priv->dbg);
    } else {
        obj = GTK_OBJECT (bpm->priv->bs);
        GDL_TRACE_EXTRA ("%p", obj);
    }
        
#if 0
    bpm->priv->program_unloaded_sig = 
		gtk_signal_connect_object (GTK_OBJECT (obj), 
								   "program_unloaded",
								   GTK_SIGNAL_FUNC (clear_breakpoint_manager),
								   (gpointer)bpm);
#endif

	bpm->priv->breakpoint_set_sig = 
		gtk_signal_connect (GTK_OBJECT (obj), 
							"breakpoint_set",
							GTK_SIGNAL_FUNC (breakpoint_set_cb),
							(gpointer)bpm);

	bpm->priv->breakpoint_enabled_sig = 
		gtk_signal_connect (GTK_OBJECT (obj), 
							"breakpoint_enabled",
							GTK_SIGNAL_FUNC (breakpoint_enabled_cb),
							(gpointer)bpm);

	bpm->priv->breakpoint_disabled_sig = 
		gtk_signal_connect (GTK_OBJECT (obj), 
							"breakpoint_disabled",
							GTK_SIGNAL_FUNC (breakpoint_disabled_cb),
							(gpointer)bpm);

	bpm->priv->breakpoint_deleted_sig = 
		gtk_signal_connect (GTK_OBJECT (obj), 
							"breakpoint_deleted",
							GTK_SIGNAL_FUNC (breakpoint_deleted_cb),
							(gpointer)bpm);
}

void
disconnect_source_signals (GdfBreakpointManager *bpm)
{
    GtkObject *obj;

    GDL_TRACE ();

    obj = bpm->priv->dbg ? GTK_OBJECT (bpm->priv->dbg) : GTK_OBJECT (bpm->priv->bs);

	gtk_signal_disconnect (GTK_OBJECT (obj), 
						   bpm->priv->program_unloaded_sig);
	gtk_signal_disconnect (GTK_OBJECT (obj),
						   bpm->priv->breakpoint_set_sig);
	gtk_signal_disconnect (GTK_OBJECT (obj),
						   bpm->priv->breakpoint_enabled_sig);
	gtk_signal_disconnect (GTK_OBJECT (obj),
						   bpm->priv->breakpoint_disabled_sig);
	gtk_signal_disconnect (GTK_OBJECT (obj),
						   bpm->priv->breakpoint_deleted_sig);
}

static gint 
on_right_click_cb (ETable *table, gint row, gint col,
                   GdkEventButton *event,
                   GdfBreakpointManager *bpm)
{
    BreakpointManagerMenuData menu_data;
    GtkWidget *popup_menu;
    
    menu_data.bpm = bpm;
    menu_data.row = row;

    popup_menu = gnome_popup_menu_new (breakpoint_uiinfo);
    gnome_popup_menu_do_popup_modal (popup_menu, NULL, NULL,
                                     event, &menu_data);
    
    gtk_widget_unref (popup_menu);
    return TRUE;
}

static void 
delete_breakpoint_selected_cb (GtkWidget *menu,
                               void *data)
{
    BreakpointManagerMenuData *menu_data = data;
    GNOME_Development_Breakpoint *bp;
    
    bp = g_ptr_array_index (menu_data->bpm->priv->breakpoints, 
                            menu_data->row);

    if (menu_data->bpm->priv->dbg) {
        gdf_debugger_client_delete_breakpoint (menu_data->bpm->priv->dbg,
                                               bp->num);
    } else {
        gdf_breakpoint_set_client_delete_breakpoint (menu_data->bpm->priv->bs,
                                                     bp->num);
    }
}

/* 
 * Debugger Callbacks 
 */					
					  
void
breakpoint_set_cb (GtkObject *obj, gint bp_num, 
				   GdfBreakpointManager *bpm)
{
    GNOME_Development_Breakpoint *bp;
    GdfDebuggerClientResult res = -1;

    GDL_TRACE ();

    if (bpm->priv->dbg) {
        res = gdf_debugger_client_get_breakpoint_info (bpm->priv->dbg,
                                                       bp_num,
                                                       &bp);
    } else if (bpm->priv->bs) {
        res = gdf_breakpoint_set_client_get_breakpoint_info (bpm->priv->bs,
                                                             bp_num,
                                                             &bp);
    }

    if (res == GDF_DEBUGGER_CLIENT_OK) {
        g_ptr_array_add (bpm->priv->breakpoints, bp);
        g_hash_table_insert (bpm->priv->rows,
                             GINT_TO_POINTER (bp_num), 
                             GINT_TO_POINTER (bpm->priv->breakpoints->len - 1));
        
        e_table_model_row_inserted (bpm->priv->e_table_model,
                                    bpm->priv->breakpoints->len - 1);
    } else {
        g_warning ("Could not get breakpoint info.");
    }
}

void
breakpoint_enabled_cb (GtkObject *obj, int bp_num, 
					   GdfBreakpointManager *bpm)
{
    int row;    
    GNOME_Development_Breakpoint *bp;
    e_table_model_pre_change (bpm->priv->e_table_model);
    row = GPOINTER_TO_INT (g_hash_table_lookup (bpm->priv->rows, 
                                                GINT_TO_POINTER (bp_num)));
    bp = g_ptr_array_index (bpm->priv->breakpoints, row);
    bp->enabled = TRUE;
    e_table_model_row_changed (bpm->priv->e_table_model, row);
}

void
breakpoint_disabled_cb (GtkObject *obj, gint bp_num, 
						GdfBreakpointManager *bpm)
{    
    int row;
    GNOME_Development_Breakpoint *bp;
    e_table_model_pre_change (bpm->priv->e_table_model);
    row = GPOINTER_TO_INT (g_hash_table_lookup (bpm->priv->rows, 
                                                GINT_TO_POINTER (bp_num)));
    bp = g_ptr_array_index (bpm->priv->breakpoints, row);
    bp->enabled = FALSE;
    e_table_model_row_changed (bpm->priv->e_table_model, row);
}

void
breakpoint_deleted_cb (GtkObject *obj, int bp_num, 
					   GdfBreakpointManager *bpm)
{
    int row;
    int i;
    GNOME_Development_Breakpoint *removed_bp;
    row = GPOINTER_TO_INT (g_hash_table_lookup (bpm->priv->rows,
                                                GINT_TO_POINTER (bp_num)));
        
    removed_bp = g_ptr_array_index (bpm->priv->breakpoints, row);
    e_table_model_pre_change (bpm->priv->e_table_model);
    
    g_ptr_array_remove_index (bpm->priv->breakpoints, row);
    g_hash_table_remove (bpm->priv->rows, GINT_TO_POINTER (bp_num));
    for (i = row; i < bpm->priv->breakpoints->len; i++) {
        GNOME_Development_Breakpoint *bp = g_ptr_array_index (bpm->priv->breakpoints, i);
        g_hash_table_insert (bpm->priv->rows,
                             GINT_TO_POINTER (bp->num), 
                             GINT_TO_POINTER (i));
    }
    
    e_table_model_row_deleted (bpm->priv->e_table_model, row);   
    CORBA_free (removed_bp);
}
