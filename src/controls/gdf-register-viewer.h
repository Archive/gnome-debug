/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __GNOME_Development_REGISTER_VIEWER_H__
#define __GNOME_Development_REGISTER_VIEWER_H__

#include <gnome.h>
#include <gdf.h>

BEGIN_GNOME_DECLS

typedef struct _GdfRegisterViewer      GdfRegisterViewer;
typedef struct _GdfRegisterViewerClass GdfRegisterViewerClass;
typedef struct _GdfRegisterViewerPriv  GdfRegisterViewerPriv;

#define GNOME_Development_REGISTER_VIEWER_TYPE        (gdf_register_viewer_get_type ())
#define GNOME_Development_REGISTER_VIEWER(o)          (GTK_CHECK_CAST ((o), GNOME_Development_REGISTER_VIEWER_TYPE, GdfRegisterViewer))
#define GNOME_Development_REGISTER_VIEWER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GNOME_Development_REGISTER_VIEWER_TYPE, GdfRegisterViewerClass))
#define GNOME_Development_IS_REGISTER_VIEWER(o)       (GTK_CHECK_TYPE ((o), GNOME_Development_REGISTER_VIEWER_TYPE))
#define GNOME_Development_IS_REGISTER_VIEWER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_Development_REGISTER_VIEWER_TYPE))

struct _GdfRegisterViewer {
    GtkFrame frame;

    GdfRegisterViewerPriv *priv;
};

struct _GdfRegisterViewerClass {
    GtkFrameClass parent_class;
};

GtkType gdf_register_viewer_get_type (void);
GtkWidget *gdf_register_viewer_new (void);
void gdf_register_viewer_set_debugger (GdfRegisterViewer *rv, 
                                        GdfDebuggerClient *dbg);

END_GNOME_DECLS
#endif
