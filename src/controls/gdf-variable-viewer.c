/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <gnome.h>
#include "gdf-variable-viewer.h"
#include <gdf.h>
#include <gdl/gdl.h>

struct _GdfVariableViewerPriv {
	GtkWidget *ctree;
    GtkWidget *entry;
    GtkWidget *add_btn;
	GtkWidget *scrolled;
    GtkWidget *entry_hbox;

    GTree *nodes;

	/* signal identifiers */
    Bonobo_EventSource_ListenerId ss_listener_id;
	guint program_unloaded_sig;

	GdfDebuggerClient *dbg;
    GNOME_Development_SymbolSet ss;
    gboolean view_locals;
};

enum {
    DEBUGGER_SET,
    LAST_SIGNAL
};

enum {
    ARG_0,
    ARG_DEBUGGER,
    ARG_SHOW_ENTRY
};

static gint variable_viewer_signals[LAST_SIGNAL];

static void variable_viewer_class_init (GdfVariableViewerClass *klass);
static void variable_viewer_init (GdfVariableViewer *vv);
static void variable_viewer_destroy (GtkObject *object);
static void get_arg (GtkObject *object, 
                     GtkArg *arg, 
                     guint arg_id);
static void set_arg (GtkObject *object, 
                     GtkArg *arg, 
                     guint arg_id);
static void create_children (GdfVariableViewer *vv);
static void add_node (GdfVariableViewer *vv, GtkCTreeNode *parent, 
                      GNOME_Development_Symbol *sym);
static void add_dummy_child (GdfVariableViewer *vv, GtkCTreeNode *parent);
static void clear_variable_viewer (GdfVariableViewer *vv);
static void connect_debugger_signals (GdfVariableViewer *vv);
static void disconnect_debugger_signals (GdfVariableViewer *vv);
static void connect_symbol_set_signals (GdfVariableViewer *vv);
static void disconnect_symbol_set_signals (GdfVariableViewer *vv);
static void add_btn_clicked_cb (GtkWidget *btn, gpointer data);
static void entry_activate_cb (GtkWidget *btn, gpointer data);
static void tree_expand_cb (GtkWidget *ctree, GtkCTreeNode *node, 
                            gpointer data);
static gint int_cmp (gconstpointer a, gconstpointer b);
static void do_popup (GdfVariableViewer *vv, int symbol_id);
static gint button_press_cb (GtkWidget *ctree, GdkEventButton *event, 
							 GdfVariableViewer *vv);
static void set_value_selected_cb (GtkWidget *widget, 
                                   GdfVariableViewer *vv);
static void cast_selected_cb (GtkWidget *widget, 
                              GdfVariableViewer *vv);
static void watchpoint_selected_cb (GtkWidget *widget, 
                                    GdfVariableViewer *vv);

static GnomeUIInfo popup_menu[] =
{
    {
        GNOME_APP_UI_ITEM,
        N_("Set value"),
        N_("Set the value of the expression."),
        set_value_selected_cb,
        NULL,
        NULL,
        GNOME_APP_PIXMAP_STOCK,
        GNOME_STOCK_MENU_INDEX,
        0, 0, NULL
    },
    {
        GNOME_APP_UI_ITEM,
        N_("Cast"),
        N_("Cast the expression to a different type"),
        cast_selected_cb,
        NULL,
        NULL,
        GNOME_APP_PIXMAP_STOCK,
        GNOME_STOCK_MENU_CONVERT,
        0, 0, NULL
    }, 
    {
        GNOME_APP_UI_ITEM,
        N_("Set watchpoint"),
        N_("Set a watchpoint on the expression"),
        watchpoint_selected_cb,
        NULL,
        NULL,
        GNOME_APP_PIXMAP_STOCK,
        GNOME_STOCK_MENU_SEARCH,
        0, 0, NULL
    },
    GNOMEUIINFO_END
};

static GtkFrameClass *parent_class;

/* 
 * Public interface 
 */

GtkType
gdf_variable_viewer_get_type (void)
{
	static GtkType variable_viewer_type = 0;
	
	if (!variable_viewer_type) {
		static const GtkTypeInfo variable_viewer_info = {
			"GdfVariableViewer",
			sizeof (GdfVariableViewer),
			sizeof (GdfVariableViewerClass),
			(GtkClassInitFunc) variable_viewer_class_init,
			(GtkObjectInitFunc) variable_viewer_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};
	
		variable_viewer_type = gtk_type_unique (gtk_frame_get_type (),
                                                &variable_viewer_info);
	}
	
	return variable_viewer_type;
}

GdfVariableViewer *
gdf_variable_viewer_new (void)
{
	GdfVariableViewer *vv;
	
	vv = gtk_type_new (gdf_variable_viewer_get_type ());
	
	return vv;
}

void 
gdf_variable_viewer_view_symbol_set (GdfVariableViewer *vv,
                                     GNOME_Development_SymbolSet ss)
{
    g_return_if_fail (vv != NULL);
	g_return_if_fail (GDF_IS_VARIABLE_VIEWER (vv));

    vv->priv->view_locals = FALSE;

    if (vv->priv->dbg) {
        if (!ss) {
            gdf_debugger_client_allocate_symbol_set (vv->priv->dbg, 
                                                     &vv->priv->ss);
        } else {
            vv->priv->ss = ss;
        }
    }
    
    /* FIXME: Get the current symbol set */
}

void 
gdf_variable_viewer_view_locals (GdfVariableViewer *vv)
{
    g_return_if_fail (vv != NULL);
	g_return_if_fail (GDF_IS_VARIABLE_VIEWER (vv));

    vv->priv->view_locals = TRUE;

    if (vv->priv->dbg) {
        gdf_debugger_client_get_locals (vv->priv->dbg, &vv->priv->ss);
    }
    
    /* FIXME: Get the current symbol set */
}

void 
gdf_variable_viewer_view_symbol (GdfVariableViewer *vv,
                                 GNOME_Development_Symbol *sym)
{
	g_return_if_fail (vv != NULL);
	g_return_if_fail (sym != NULL);
	g_return_if_fail (GDF_IS_VARIABLE_VIEWER (vv));

    add_node (vv, NULL, sym);
}

void
gdf_variable_viewer_clear (GdfVariableViewer *vv)
{
	g_return_if_fail (vv != NULL);
	g_return_if_fail (GDF_IS_VARIABLE_VIEWER (vv));

    clear_variable_viewer (vv);
}

void
gdf_variable_viewer_set_debugger (GdfVariableViewer *vv,
                                  GdfDebuggerClient *dbg)
{
	g_return_if_fail (vv != NULL);
	g_return_if_fail (GDF_IS_VARIABLE_VIEWER (vv));

    gdf_variable_viewer_clear (vv);

	if (vv->priv->dbg) {
		disconnect_debugger_signals (vv);
		gtk_object_unref (GTK_OBJECT (vv->priv->dbg));
	} 

    if (vv->priv->ss) {
        disconnect_symbol_set_signals (vv);
        gtk_object_unref (GTK_OBJECT (vv->priv->ss));
        vv->priv->ss = NULL;
    }
	
	vv->priv->dbg = dbg;

    if (dbg) {
        gtk_object_ref (GTK_OBJECT (dbg));
        if (GTK_OBJECT_FLOATING (GTK_OBJECT (dbg))) 
            gtk_object_sink (GTK_OBJECT (dbg));
        if (vv->priv->view_locals) {
            gdf_debugger_client_get_locals (vv->priv->dbg, 
                                            &vv->priv->ss);
        } else {
            gdf_debugger_client_allocate_symbol_set (vv->priv->dbg,
                                                     &vv->priv->ss);
        }

        connect_debugger_signals (vv);
        connect_symbol_set_signals (vv);
    }

    /* FIXME: View the symbol set */

    gtk_signal_emit (GTK_OBJECT (vv), variable_viewer_signals[DEBUGGER_SET],
                     dbg);
}

/*
 * Class/object functions.
 */

void
variable_viewer_class_init (GdfVariableViewerClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *)klass;

    gtk_object_add_arg_type ("GdfVariableViewer::debugger",
                             GTK_TYPE_OBJECT,
                             GTK_ARG_READWRITE, 
                             ARG_DEBUGGER);
    gtk_object_add_arg_type ("GdfVariableViewer::show_entry",
                             GTK_TYPE_BOOL,
                             GTK_ARG_READWRITE, 
                             ARG_SHOW_ENTRY);
    
	parent_class = gtk_type_class (gtk_frame_get_type ());
    
    variable_viewer_signals [DEBUGGER_SET] =
        gtk_signal_new ("debugger_set",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GdfVariableViewerClass,
                                           debugger_set),
                        gtk_marshal_NONE__POINTER,
                        GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
    
    gtk_object_class_add_signals (object_class, 
                                  variable_viewer_signals,
                                  LAST_SIGNAL);

    klass->debugger_set = NULL;
	object_class->destroy = variable_viewer_destroy;
    object_class->get_arg = get_arg;
    object_class->set_arg = set_arg;
}

void
variable_viewer_init (GdfVariableViewer *vv)
{

    vv->priv = g_new0 (GdfVariableViewerPriv, 1);

	vv->priv->dbg = NULL;
    vv->priv->ss = NULL;
    vv->priv->nodes = NULL;
    vv->priv->view_locals = FALSE;

	create_children (vv);
}

void 
variable_viewer_destroy (GtkObject *obj)
{
	GdfVariableViewer *vv = GDF_VARIABLE_VIEWER (obj);
	
	g_return_if_fail (obj != NULL);
	
	if (vv->priv->dbg)
		gtk_object_unref (GTK_OBJECT (vv->priv->dbg));
    if (vv->priv->ss)
        gtk_object_unref (GTK_OBJECT (vv->priv->ss));

    if (vv->priv->nodes)
        g_tree_destroy (vv->priv->nodes);

	g_free (vv->priv);
	
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (obj);
}

void 
get_arg (GtkObject *object, 
         GtkArg *arg, 
         guint arg_id)
{
    GdfVariableViewer *vv = GDF_VARIABLE_VIEWER (object);
    
    switch (arg_id) {
    case ARG_DEBUGGER :
        GTK_VALUE_OBJECT (*arg) = GTK_OBJECT (vv->priv->dbg);
        break;
    case ARG_SHOW_ENTRY :
        GTK_VALUE_BOOL (*arg) = GTK_WIDGET_VISIBLE (vv->priv->entry_hbox);
        break;
    default :
        arg->type = GTK_TYPE_INVALID;
    }   
}

void 
set_arg (GtkObject *object, 
         GtkArg *arg, 
         guint arg_id)
{
    GdfVariableViewer *vv = GDF_VARIABLE_VIEWER (object);
    
    switch (arg_id) {
    case ARG_DEBUGGER :
        gdf_variable_viewer_set_debugger (vv, 
                                          GTK_VALUE_OBJECT (*arg) ? GDF_DEBUGGER_CLIENT (GTK_VALUE_OBJECT (*arg)) : NULL);
        break;
    case ARG_SHOW_ENTRY :
        if (GTK_VALUE_BOOL (*arg)) {
            gtk_widget_show (vv->priv->entry_hbox);
        } else {
            gtk_widget_hide (vv->priv->entry_hbox);
        }
        break;
    default :
        break;
    }   
}

/* 
 * Helper functions
 */
void
create_children (GdfVariableViewer *vv)
{
	static gchar *titles[] = {
		N_("Name"),
		N_("Value")
	};
    GtkWidget *vbox;

	vv->priv->ctree = gtk_ctree_new_with_titles (2, 0, titles);
	
    gtk_signal_connect (GTK_OBJECT (vv->priv->ctree), "tree_expand",
                        GTK_SIGNAL_FUNC (tree_expand_cb), vv);
	gtk_signal_connect (GTK_OBJECT (vv->priv->ctree), "button_press_event",
						GTK_SIGNAL_FUNC (button_press_cb), vv);

    gtk_clist_set_column_width (GTK_CLIST (vv->priv->ctree), 0, 150);

	vv->priv->scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (vv->priv->scrolled),
									GTK_POLICY_AUTOMATIC,
									GTK_POLICY_AUTOMATIC);	

    vv->priv->entry = gnome_entry_new ("variable-viewer");
    gtk_signal_connect (GTK_OBJECT (gnome_entry_gtk_entry (GNOME_ENTRY (vv->priv->entry))), 
                        "activate", GTK_SIGNAL_FUNC (entry_activate_cb), vv);
    vv->priv->add_btn = gtk_button_new_with_label (_("Add"));
    gtk_signal_connect (GTK_OBJECT (vv->priv->add_btn), "clicked", 
                        GTK_SIGNAL_FUNC (add_btn_clicked_cb), vv);

    vbox = gtk_vbox_new (FALSE, 0);
    vv->priv->entry_hbox = gtk_hbox_new (FALSE, 0);

    gtk_box_pack_start (GTK_BOX (vv->priv->entry_hbox), 
                        vv->priv->entry, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vv->priv->entry_hbox), 
                        vv->priv->add_btn, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (vv->priv->scrolled), vv->priv->ctree);
    gtk_box_pack_start (GTK_BOX (vbox), vv->priv->entry_hbox, FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), vv->priv->scrolled, TRUE, TRUE, 0); 
	gtk_container_add (GTK_CONTAINER (vv), vbox);
    
    gtk_widget_show (vv->priv->entry);
    gtk_widget_show (vv->priv->add_btn);
    gtk_widget_show (vv->priv->entry_hbox);
    gtk_widget_show (vbox);
	gtk_widget_show (vv->priv->ctree);
	gtk_widget_show (vv->priv->scrolled);
}

void
add_dummy_child (GdfVariableViewer *vv, GtkCTreeNode *parent)
{       
    GtkCTreeNode *node;
    char *text[2];
    text[0] = "";
    text[1] = "";
    
    node = gtk_ctree_insert_node (GTK_CTREE (vv->priv->ctree), parent, 
                                  NULL, text, 0, NULL, NULL, NULL, NULL, 
                                  FALSE, FALSE);
    gtk_ctree_node_set_row_data (GTK_CTREE (vv->priv->ctree), node, 
                                 GINT_TO_POINTER (-1));
}

void
add_node (GdfVariableViewer *vv, GtkCTreeNode *parent, GNOME_Development_Symbol *sym)
{
    GtkCTreeNode *node;
    char *text[2];

    text[0] = sym->name;
    text[1] = sym->value;
    
    node = gtk_ctree_insert_node (GTK_CTREE (vv->priv->ctree), parent, NULL, 
                                  text, 0, NULL, NULL, NULL, NULL, 
                                  FALSE, 
                                  FALSE);
    g_tree_insert (vv->priv->nodes, GINT_TO_POINTER (sym->handle), node);
    gtk_ctree_node_set_row_data (GTK_CTREE (vv->priv->ctree), node, 
                                 GINT_TO_POINTER (sym->handle));
    if (sym->expandable) {
        add_dummy_child (vv, node);
    }
}

void 
clear_variable_viewer (GdfVariableViewer *vv)
{
	gtk_clist_clear (GTK_CLIST (vv->priv->ctree));
    if (vv->priv->nodes) {
        g_tree_destroy (vv->priv->nodes);
        vv->priv->nodes = NULL;
    }
}

static void
symbol_set_changed_cb (GNOME_Development_SymbolSet ss, GdfVariableViewer *vv)
{
    CORBA_Environment ev;
    GNOME_Development_SymbolSequence *syms;
    int i;

    gdf_variable_viewer_clear (vv);

    CORBA_exception_init (&ev);
    syms = GNOME_Development_SymbolSet_getSymbols (ss, &ev);
    CORBA_exception_free (&ev);

    if (syms->_length > 0)
        vv->priv->nodes = g_tree_new (int_cmp);
    else 
        vv->priv->nodes = NULL;

    for (i = 0; i < syms->_length; i++)
        gdf_variable_viewer_view_symbol (vv, &syms->_buffer[i]);

    CORBA_free (syms);
}

static void
symbol_changed_cb (GNOME_Development_SymbolSet ss, int handle, 
                   GdfVariableViewer *vv)
{
    CORBA_Environment ev;
    GNOME_Development_Symbol *sym;
    GtkCTreeNode *node;
    GtkCTreeNode *child;
    GtkCTreeRow *row;

    CORBA_exception_init (&ev);
    sym = GNOME_Development_SymbolSet_getSymbol (ss, handle, &ev);
    CORBA_exception_free (&ev);

    node = g_tree_lookup (vv->priv->nodes, GINT_TO_POINTER (handle));
    row = GTK_CTREE_ROW (node);
    gtk_ctree_node_set_text (GTK_CTREE (vv->priv->ctree),
                             node, 1, sym->value);

    child = GTK_CTREE_ROW (node)->children;
    if (sym->expandable && !child) {
        add_dummy_child (vv, node);
    }

    CORBA_free (sym);
}

static void
symbol_type_changed_cb (GNOME_Development_SymbolSet ss, int handle,
                        GdfVariableViewer *vv)
{
    CORBA_Environment ev;
    GNOME_Development_Symbol *sym;
    GtkCTreeNode *node;
    GtkCTreeNode *child;
    GtkCTreeRow *row;

    CORBA_exception_init (&ev);
    sym = GNOME_Development_SymbolSet_getSymbol (ss, handle, &ev);   
    CORBA_exception_free (&ev);

    node = g_tree_lookup (vv->priv->nodes, GINT_TO_POINTER (handle));
    row = GTK_CTREE_ROW (node);
    
    /* Remove the old/dummy children */
    child = GTK_CTREE_ROW (node)->children;
    while (child) {
        GtkCTreeNode *next;
        next = GTK_CTREE_ROW (child)->sibling;
        gtk_ctree_remove_node (GTK_CTREE (vv->priv->ctree), child);
        child = next;
    }

    if (sym->expandable) {
        add_dummy_child (vv, node);
    }
}

void
connect_debugger_signals (GdfVariableViewer *vv)
{
	vv->priv->program_unloaded_sig = 
		gtk_signal_connect_object (GTK_OBJECT (vv->priv->dbg), 
								   "program_unloaded",
								   GTK_SIGNAL_FUNC (clear_variable_viewer),
								   (gpointer)vv);

}

void
disconnect_debugger_signals (GdfVariableViewer *vv)
{
	gtk_signal_disconnect (GTK_OBJECT (vv->priv->dbg), 
						   vv->priv->program_unloaded_sig);
}

static void
symbol_set_event_cb (BonoboListener *listener, char *event_name,
                     BonoboArg *arg, CORBA_Environment *ev,
                     gpointer user_data)
{
    GdfVariableViewer *vv = GDF_VARIABLE_VIEWER (user_data);
    
    GDL_TRACE_EXTRA ("%s", event_name);

    if (!strcmp (event_name, "symbol_changed")) {
        symbol_changed_cb (vv->priv->ss, BONOBO_ARG_GET_LONG(arg), vv);
    } else if (!strcmp (event_name, "symbol_type_changed")) {
        symbol_type_changed_cb (vv->priv->ss, BONOBO_ARG_GET_LONG(arg), vv);
    } else if (!strcmp (event_name, "symbol_set_changed")) {
        symbol_set_changed_cb (vv->priv->ss, vv);
    }
}

void 
connect_symbol_set_signals (GdfVariableViewer *vv)
{
    GDL_TRACE ();

    vv->priv->ss_listener_id =
        bonobo_event_source_client_add_listener (vv->priv->ss,
                                                 symbol_set_event_cb,
                                                 NULL, NULL, (gpointer)vv);
}

void
disconnect_symbol_set_signals (GdfVariableViewer *vv)
{
    bonobo_event_source_client_remove_listener (vv->priv->ss,
                                                vv->priv->ss_listener_id,
                                                NULL);
}

static void
add_current_expression (GdfVariableViewer *vv)
{
    CORBA_Environment ev; 
    char *text;
    
    text = 
        gtk_entry_get_text (GTK_ENTRY (gnome_entry_gtk_entry (GNOME_ENTRY (vv->priv->entry))));
    
    CORBA_exception_init (&ev);
    if (strcmp (text, "")) {
        GNOME_Development_SymbolSet_addExpression (vv->priv->ss,
                                                   text,
                                                   &ev);
        CORBA_exception_free (&ev);
    }

    gtk_entry_set_text (GTK_ENTRY (gnome_entry_gtk_entry (GNOME_ENTRY (vv->priv->entry))), "");
}


void
add_btn_clicked_cb (GtkWidget *btn, gpointer data)
{
    add_current_expression (GDF_VARIABLE_VIEWER (data));
}

void
entry_activate_cb (GtkWidget *editable, gpointer data)
{
    add_current_expression (GDF_VARIABLE_VIEWER (data));
}

void
tree_expand_cb (GtkWidget *ctree, GtkCTreeNode *node, gpointer data)
{
    CORBA_Environment ev;
    GdfVariableViewer *vv = GDF_VARIABLE_VIEWER (data);
    GtkCTreeNode *child;
    int sym_id;

    child = GTK_CTREE_ROW (node)->children;
    if (!child)
        return;
    
    sym_id = GPOINTER_TO_INT (gtk_ctree_node_get_row_data (GTK_CTREE (ctree),
                                                           child));
    
    /* if this is a dummy node, sym_id will be -1 */
    if (sym_id == -1) {
        int i;        
        GNOME_Development_SymbolSequence *seq;
        sym_id = 
            GPOINTER_TO_INT (gtk_ctree_node_get_row_data (GTK_CTREE (ctree), 
                                                          node));
        CORBA_exception_init (&ev);
        seq = GNOME_Development_SymbolSet_getSymbolChildren (vv->priv->ss, 
                                                             sym_id, &ev);
        CORBA_exception_free (&ev);

        /* Remove the old/dummy children */
        child = GTK_CTREE_ROW (node)->children;
        while (child) {
            GtkCTreeNode *next;
            next = GTK_CTREE_ROW (child)->sibling;
            gtk_ctree_remove_node (GTK_CTREE (vv->priv->ctree), child);
            child = next;
        }

        gtk_clist_freeze (GTK_CLIST (vv->priv->ctree));

        for (i = 0; i < seq->_length; i++) {
            add_node (vv, node, &(seq->_buffer[i]));
        }

        gtk_clist_thaw (GTK_CLIST (vv->priv->ctree));

        CORBA_free (seq);
    }
}

gint
int_cmp (gconstpointer a, gconstpointer b) 
{
    return (GPOINTER_TO_INT (a) < GPOINTER_TO_INT (b) 
            ? -1 
            : (GPOINTER_TO_INT (a) == GPOINTER_TO_INT (b) ? 0 : 1));
}

void
do_popup (GdfVariableViewer *vv, int symbol_id)
{
	GtkWidget *menu = NULL;

	gtk_object_set_data (GTK_OBJECT (vv), "symbol_id", 
                         GINT_TO_POINTER (symbol_id));

    popup_menu[0].user_data = vv;
    popup_menu[0].user_data = vv;

	menu = gnome_popup_menu_new (popup_menu);
	gnome_popup_menu_do_popup (menu, NULL, NULL, NULL, (gpointer)vv);
}

gint
button_press_cb (GtkWidget *widget, GdkEventButton *event, 
				 GdfVariableViewer *vv)
{
    CORBA_Environment ev;
	gint row;
    int sym_id;
    GNOME_Development_Symbol *sym;

	if (!gtk_clist_get_selection_info (GTK_CLIST (widget), 
									   (gint)event->x, (gint)event->y,
									   &row, NULL))
		return TRUE;
    
    sym_id = GPOINTER_TO_INT (gtk_ctree_node_get_row_data (GTK_CTREE (vv->priv->ctree),
                                                           gtk_ctree_node_nth (GTK_CTREE (vv->priv->ctree), row)));

    CORBA_exception_init (&ev);
    sym = GNOME_Development_SymbolSet_getSymbol (vv->priv->ss, sym_id, &ev);

    CORBA_exception_free (&ev);
	if (sym->type != GNOME_Development_TYPE_ARRAY_SLICE && event->button == 3)
		do_popup (vv, sym_id);

    CORBA_free (sym);

	return FALSE;
}

void 
set_value_selected_cb (GtkWidget *widget, 
                       GdfVariableViewer *vv)
{
    CORBA_Environment ev;
    int sym_id;
    GNOME_Development_Symbol *sym;
    GtkWidget *dlg;
    GtkWidget *entry;
    GtkWidget *label;
    GtkWidget *hbox;
    char *errmsg;
    int btn;
    char *value;
    
    sym_id = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (vv), 
                                                   "symbol_id"));
    CORBA_exception_init (&ev);
    sym = GNOME_Development_SymbolSet_getSymbol (vv->priv->ss, sym_id, &ev);
    
    label = gtk_label_new (_("Value :"));
    entry = gnome_entry_new ("gdf_value");

    gtk_entry_set_text (GTK_ENTRY(gnome_entry_gtk_entry (GNOME_ENTRY (entry))),
                        sym->value);

    CORBA_free (sym);

    hbox = gtk_hbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
    
    dlg = gnome_dialog_new (_("Set Value"), GNOME_STOCK_BUTTON_OK, 
                            GNOME_STOCK_BUTTON_CANCEL, NULL);

    gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), hbox, 
                        TRUE, TRUE, 0);

    gtk_widget_show (label);
    gtk_widget_show (entry);
    gtk_widget_show (hbox);

    btn = gnome_dialog_run (GNOME_DIALOG (dlg));
    
    if (btn != 0) {
        gnome_dialog_close (GNOME_DIALOG (dlg));
        return;
    }
    
    value = gtk_editable_get_chars (GTK_EDITABLE (gnome_entry_gtk_entry (GNOME_ENTRY (entry))), 0, -1);
    
    GNOME_Development_SymbolSet_setSymbolValue (vv->priv->ss, sym_id, value, &ev);
    
    gnome_dialog_close (GNOME_DIALOG (dlg));

    errmsg = NULL;
    if (BONOBO_EX (&ev)) {
        if (BONOBO_USER_EX (&ev, "IDL:GNOME/Development/SymbolSet/InvalidType:1.0")) {
            errmsg = _("Invalid type.");
        } else {
            errmsg = _("Unknown error.");
        }
    }

    if (errmsg) {
        errmsg = 
            g_strdup_printf (_("Could not set value: %s"), errmsg);
        dlg = gnome_error_dialog (errmsg);
        gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
    }
    CORBA_exception_free (&ev);
}

void 
cast_selected_cb (GtkWidget *widget, 
                  GdfVariableViewer *vv)
{
    CORBA_Environment ev;
    int sym_id;
    GNOME_Development_Symbol *sym;
    GtkWidget *dlg;
    GtkWidget *entry;
    GtkWidget *label;
    GtkWidget *hbox;
    char *errmsg;
    int btn;
    char *type;
    
    sym_id = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (vv), 
                                                   "symbol_id"));
    CORBA_exception_init (&ev);
    sym = GNOME_Development_SymbolSet_getSymbol (vv->priv->ss, sym_id, &ev);
    
    label = gtk_label_new (_("Cast to :"));
    entry = gnome_entry_new ("gdf_value");

    gtk_entry_set_text (GTK_ENTRY(gnome_entry_gtk_entry (GNOME_ENTRY (entry))),
                        sym->cast_to);

    CORBA_free (sym);

    hbox = gtk_hbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
    
    dlg = gnome_dialog_new (_("Cast"), GNOME_STOCK_BUTTON_OK, 
                            GNOME_STOCK_BUTTON_CANCEL, NULL);

    gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), hbox, 
                        TRUE, TRUE, 0);

    gtk_widget_show (label);
    gtk_widget_show (entry);
    gtk_widget_show (hbox);

    btn = gnome_dialog_run (GNOME_DIALOG (dlg));
    
    if (btn != 0) {
        gnome_dialog_close (GNOME_DIALOG (dlg));
        return;
    }
    
    type = gtk_editable_get_chars (GTK_EDITABLE (gnome_entry_gtk_entry (GNOME_ENTRY (entry))), 0, -1);
    
    GNOME_Development_SymbolSet_cast (vv->priv->ss, sym_id, type, &ev);
    
    gnome_dialog_close (GNOME_DIALOG (dlg));

    errmsg = NULL;
    if (BONOBO_EX (&ev)) {
        if (BONOBO_USER_EX (&ev, "IDL:GNOME/Development/SymbolSet/InvalidType:1.0")) {
            errmsg = _("Invalid type.");
        } else {
            errmsg = _("Unknown error.");
        }
    }
    
    if (errmsg) {
        errmsg = 
            g_strdup_printf (_("Could not cast: %s"), errmsg);
        dlg = gnome_error_dialog (errmsg);
        gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
    }
    CORBA_exception_free (&ev);
}

void
watchpoint_selected_cb (GtkWidget *widget, 
                        GdfVariableViewer *vv)
{
    CORBA_Environment ev;
    int sym_id;
    GNOME_Development_Symbol *sym;
    GdfDebuggerClientResult res;
    
    sym_id = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (vv), 
                                                   "symbol_id"));
    CORBA_exception_init (&ev);
    sym = GNOME_Development_SymbolSet_getSymbol (vv->priv->ss, sym_id, &ev);
    
    res = gdf_debugger_client_set_watchpoint (vv->priv->dbg, 
                                              sym->expression, 
                                              NULL, NULL);

    CORBA_exception_free (&ev);
    CORBA_free (sym);
}


