/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __GDF_VARIABLE_VIEWER_H__
#define __GDF_VARIABLE_VIEWER_H__

#include <gnome.h>
#include <gdf.h>

BEGIN_GNOME_DECLS

typedef struct _GdfVariableViewer      GdfVariableViewer;
typedef struct _GdfVariableViewerClass GdfVariableViewerClass;
typedef struct _GdfVariableViewerPriv  GdfVariableViewerPriv;

#define GDF_VARIABLE_VIEWER_TYPE        (gdf_variable_viewer_get_type ())
#define GDF_VARIABLE_VIEWER(o)          (GTK_CHECK_CAST ((o), GDF_VARIABLE_VIEWER_TYPE, GdfVariableViewer))
#define GDF_VARIABLE_VIEWER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDF_VARIABLE_VIEWER_TYPE, GdfVariableViewerClass))
#define GDF_IS_VARIABLE_VIEWER(o)       (GTK_CHECK_TYPE ((o), GDF_VARIABLE_VIEWER_TYPE))
#define GDF_IS_VARIABLE_VIEWER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_VARIABLE_VIEWER_TYPE))

struct _GdfVariableViewer {
	GtkFrame frame;

	GdfVariableViewerPriv *priv;
};

struct _GdfVariableViewerClass {
	GtkFrameClass parent_class;

    void (*debugger_set) (GdfVariableViewer *vv, GdfDebuggerClient *client);
};

GtkType gdf_variable_viewer_get_type (void);

GdfVariableViewer *gdf_variable_viewer_new (void);

void gdf_variable_viewer_view_symbol_set (GdfVariableViewer *vv,
                                          GNOME_Development_SymbolSet ss);
void gdf_variable_viewer_view_locals (GdfVariableViewer *vv);
void gdf_variable_viewer_view_symbol (GdfVariableViewer *vv, GNOME_Development_Symbol *sym);
void gdf_variable_viewer_set_debugger (GdfVariableViewer *vv,
                                       GdfDebuggerClient *dbg);
void gdf_variable_viewer_clear (GdfVariableViewer *vv);
#endif





