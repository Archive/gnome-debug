/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <gnome.h>
#include "gdf-stack-browser.h"

#include <gal/widgets/e-canvas.h>
#include <gal/widgets/e-popup-menu.h>
#include <gal/e-table/e-table-header-item.h>
#include <gal/e-table/e-table-item.h>
#include <gal/util/e-util.h>
#include <gal/e-table/e-table.h>
#include <gal/e-table/e-table-header-item.h>
#include <gal/e-table/e-table-item.h>
#include <gal/e-table/e-table-scrolled.h>
#include <gal/e-table/e-table-simple.h>
#include <gal/e-table/e-cell-text.h>
#include <gal/e-table/e-cell-checkbox.h>

#include "../../pixmaps/current-line.xpm"
#include "../../pixmaps/empty-15x15.xpm"

struct _GdfStackBrowserPriv {
    ETableModel *e_table_model;
    GNOME_Development_Stack *stack;

    int current_frame;

    GdfDebuggerClient *dbg;

    int frame_changed_sig;
    int corefile_loaded_sig;
    int corefile_unloaded_sig;
    int execution_stopped_sig;
    int execution_exited_sig;
    int execution_killed_sig;
    int program_unloaded_sig;
};

enum {
    ARG_0,
    ARG_DEBUGGER
};

enum {
    COL_CUR,
    COL_ID,
    COL_FUNCTION,
    COL_LINE,
    COL_FILE,
    COL_ADDRESS,
    LAST_COL
};


const char *stack_browser_spec =
"<ETableSpecification click-to-add=\"false\" draw-grid=\"true\">"
"  <ETableColumn model_col=\"0\" pixbuf=\"arrow\" expansion=\"0.25\" minimum_width=\"30\" resizable=\"false\" cell=\"arrow\" compare=\"integer\"/>"
"  <ETableColumn model_col=\"1\" _title=\"ID\" expansion=\"0.2\" minimum_width=\"20\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableColumn model_col=\"2\" _title=\"Function\" expansion=\"1.0\" minimum_width=\"75\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableColumn model_col=\"3\" _title=\"Line\" expansion=\"0.1\" minimum_width=\"30\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableColumn model_col=\"4\" _title=\"File\" expansion=\"1.0\" minimum_width=\"40\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableColumn model_col=\"5\" _title=\"Address\" expansion=\"0.1\" minimum_width=\"50\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableState>"
"    <column source=\"0\"/>"
"    <column source=\"1\"/>"
"    <column source=\"2\"/>"
"    <column source=\"3\"/>"
"    <column source=\"4\"/>"
"    <column source=\"5\"/>"
"    <grouping></grouping>"
"  </ETableState>"
"</ETableSpecification>";

static void stack_browser_init (GdfStackBrowser *sb);
static void stack_browser_class_init (GdfStackBrowserClass *klass);
static void stack_browser_destroy (GtkObject *object);
static void get_arg (GtkObject *object, 
                     GtkArg *arg, 
                     guint arg_id);
static void set_arg (GtkObject *object, 
                     GtkArg *arg, 
                     guint arg_id);
static void connect_debugger_signals (GdfStackBrowser *sb);
static void disconnect_debugger_signals (GdfStackBrowser *sb);
static void frame_changed_cb (GdfDebuggerClient *dbg, int frame_id, 
                              GdfStackBrowser *sb);
static void execution_stopped_cb (GdfDebuggerClient *dbg,
                                  GdfStackBrowser *sb);
static void create_children (GdfStackBrowser *sb);
static void clear_stack_browser (GdfStackBrowser *sb);

static GtkFrameClass *parent_class;

/*
 * Public Interface 
 */

GtkType 
gdf_stack_browser_get_type ()
{
    static GtkType type = 0;
	
    if (!type) {
        static const GtkTypeInfo info = {
            "GdfStackBrowser",
            sizeof (GdfStackBrowser),
            sizeof (GdfStackBrowserClass),
            (GtkClassInitFunc) stack_browser_class_init,
            (GtkObjectInitFunc) stack_browser_init,
            NULL,
            NULL,
            (GtkClassInitFunc)NULL
        };
		
        type = gtk_type_unique (gtk_frame_get_type (), &info);
    }
	
    return type;
}

GtkWidget *
gdf_stack_browser_new ()
{
    GdfStackBrowser *sb;
    
    sb = gtk_type_new (gdf_stack_browser_get_type ());
	
    return GTK_WIDGET (sb);
}

void
gdf_stack_browser_set_debugger (GdfStackBrowser *sb,
                                GdfDebuggerClient *dbg)
{
    g_return_if_fail (sb != NULL);
    g_return_if_fail (GNOME_Development_IS_STACK_BROWSER (sb));
    
    clear_stack_browser (sb);
    
    if (sb->priv->dbg) {
        disconnect_debugger_signals (sb);
        gtk_object_unref (GTK_OBJECT (sb->priv->dbg));
    }
	
    sb->priv->dbg = dbg;

    if (dbg) {
        gtk_object_ref (GTK_OBJECT (dbg));
        if (GTK_OBJECT_FLOATING (GTK_OBJECT (dbg)))
            gtk_object_sink (GTK_OBJECT (dbg));
        
        connect_debugger_signals (sb);
    }
}

/*
 * Class / Object functions
 */

void
stack_browser_class_init (GdfStackBrowserClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *)klass;

    gtk_object_add_arg_type ("GdfStackBrowser::debugger",
                             GTK_TYPE_OBJECT,
                             GTK_ARG_READWRITE, 
                             ARG_DEBUGGER);


    parent_class = gtk_type_class (gtk_frame_get_type ());
	
    object_class->destroy = stack_browser_destroy;
    object_class->get_arg = get_arg;
    object_class->set_arg = set_arg;
}

void
stack_browser_init (GdfStackBrowser *sb)
{
    sb->priv = g_new0 (GdfStackBrowserPriv, 1);
    
    sb->priv->dbg = NULL;
    sb->priv->stack = NULL;
    sb->priv->current_frame = -1;

    create_children (sb);
}

void
stack_browser_destroy (GtkObject *obj)
{
    GdfStackBrowser *sb = GNOME_Development_STACK_BROWSER (obj);
    
    if (sb->priv->dbg)
        gtk_object_unref (GTK_OBJECT (sb->priv->dbg));
    
    if (sb->priv->stack) {
        CORBA_free (sb->priv->stack);
        sb->priv->stack = NULL;
    }

    g_free (sb->priv);
    sb->priv = NULL;

    if (GTK_OBJECT_CLASS (parent_class)->destroy)
        (*GTK_OBJECT_CLASS (parent_class)->destroy) (obj);
}

void 
get_arg (GtkObject *object, 
         GtkArg *arg, 
         guint arg_id)
{
    GdfStackBrowser *sb = GNOME_Development_STACK_BROWSER (object);
    
    switch (arg_id) {
    case ARG_DEBUGGER :
        GTK_VALUE_OBJECT (*arg) = GTK_OBJECT (sb->priv->dbg);
        break;
    default :
        arg->type = GTK_TYPE_INVALID;
    }   
}

void 
set_arg (GtkObject *object, 
         GtkArg *arg, 
         guint arg_id)
{
    GdfStackBrowser *sb = GNOME_Development_STACK_BROWSER (object);
    
    switch (arg_id) {
    case ARG_DEBUGGER :
        gdf_stack_browser_set_debugger (sb,
                                        GTK_VALUE_OBJECT (*arg) ? GDF_DEBUGGER_CLIENT (GTK_VALUE_OBJECT (*arg)) : NULL);
        break;
    default :
        break;
    }   
}

/* 
 * Helper functions 
 */

void
connect_debugger_signals (GdfStackBrowser *sb)
{
    sb->priv->frame_changed_sig = 
        gtk_signal_connect (GTK_OBJECT (sb->priv->dbg), 
                            "stack_frame_changed",
                            GTK_SIGNAL_FUNC (frame_changed_cb),
                            (gpointer)sb);
    sb->priv->corefile_loaded_sig = 
        gtk_signal_connect (GTK_OBJECT (sb->priv->dbg), 
                            "corefile_loaded",
                            GTK_SIGNAL_FUNC (execution_stopped_cb),
                            (gpointer)sb);
    sb->priv->corefile_unloaded_sig = 
        gtk_signal_connect_object (GTK_OBJECT (sb->priv->dbg),
                                   "corefile_unloaded",
                                   GTK_SIGNAL_FUNC (clear_stack_browser),
                                   (gpointer)sb);
    sb->priv->execution_stopped_sig = 
        gtk_signal_connect (GTK_OBJECT (sb->priv->dbg), 
                            "execution_stopped",
                            GTK_SIGNAL_FUNC (execution_stopped_cb),
                            (gpointer)sb);
    sb->priv->execution_exited_sig = 
        gtk_signal_connect_object (GTK_OBJECT (sb->priv->dbg),
                            "execution_exited",
                            GTK_SIGNAL_FUNC (clear_stack_browser),
                            (gpointer)sb);
    sb->priv->execution_killed_sig = 
        gtk_signal_connect_object (GTK_OBJECT (sb->priv->dbg),
                                   "execution_killed",
                                   GTK_SIGNAL_FUNC (clear_stack_browser),
                                   (gpointer)sb);
    sb->priv->program_unloaded_sig = 
        gtk_signal_connect_object (GTK_OBJECT (sb->priv->dbg),
                                   "program_unloaded",
                                   GTK_SIGNAL_FUNC (clear_stack_browser),
                                   (gpointer)sb);
}

void
disconnect_debugger_signals (GdfStackBrowser *sb)
{
    gtk_signal_disconnect (GTK_OBJECT (sb->priv->dbg),
                           sb->priv->frame_changed_sig);
    gtk_signal_disconnect (GTK_OBJECT (sb->priv->dbg),
                           sb->priv->corefile_loaded_sig);
    gtk_signal_disconnect (GTK_OBJECT (sb->priv->dbg),
                           sb->priv->corefile_unloaded_sig);
    gtk_signal_disconnect (GTK_OBJECT (sb->priv->dbg),
                           sb->priv->execution_stopped_sig);
    gtk_signal_disconnect (GTK_OBJECT (sb->priv->dbg), 
                           sb->priv->execution_exited_sig);
    gtk_signal_disconnect (GTK_OBJECT (sb->priv->dbg), 
                           sb->priv->execution_killed_sig);
    gtk_signal_disconnect (GTK_OBJECT (sb->priv->dbg), 
                           sb->priv->program_unloaded_sig);
    
}

void
frame_changed_cb (GdfDebuggerClient *dbg,
                  int frame_id,
                  GdfStackBrowser *sb)
{
    e_table_model_pre_change (sb->priv->e_table_model);
    sb->priv->current_frame = frame_id;
    e_table_model_changed (sb->priv->e_table_model);
}

void
execution_stopped_cb (GdfDebuggerClient *dbg, 
                      GdfStackBrowser *sb)
{
    e_table_model_pre_change (sb->priv->e_table_model);

    if (sb->priv->stack) {
        CORBA_free (sb->priv->stack);
        sb->priv->stack = NULL;
    }

    gdf_debugger_client_get_backtrace (dbg, &sb->priv->stack);

    e_table_model_changed (sb->priv->e_table_model);
}

static int
col_count (ETableModel *etm, void *data)
{
    return LAST_COL;
}

static int
row_count (ETableModel *etm, void *data)
{
    GdfStackBrowser *sb = data;

    if (sb->priv->stack) {
        return sb->priv->stack->_length;
    } else {
        return 0;
    }
}                          

static void *
value_at (ETableModel *etm, int col, int row, void *data)
{
    GdfStackBrowser *sb = data;
    GNOME_Development_StackFrame *frame = &sb->priv->stack->_buffer[row];
    void *ret = NULL;

    static char *static_buf = NULL;
    if (static_buf) {
        g_free (static_buf);
        static_buf = 0;
    }

    switch (col) {
    case COL_CUR :
        ret = (void*)(row == sb->priv->current_frame ? 1 : 0);
        break;
    case COL_ID :
        ret = static_buf = g_strdup_printf ("%ld", (long)frame->id);
        break;
    case COL_FUNCTION :
        ret = frame->function;
        break;
    case COL_LINE :
        ret = static_buf = g_strdup_printf ("%ld", (long)frame->location.line);
        break;
    case COL_FILE :
        ret = frame->location.file;
        break;
    case COL_ADDRESS :
        ret = static_buf = g_strdup_printf ("%p", 
                                            (void*)frame->virtual_address);
        break;
    default :
        g_assert_not_reached ();
    }
    return ret;
}

static void
set_value_at (ETableModel *etm, int col, int row, const void *val, void *data)
{
    GdfStackBrowser *sb = data;
  
    if (val) {
        gdf_debugger_client_change_frame (sb->priv->dbg, row);
    }
}

static gboolean 
is_cell_editable (ETableModel *etm, int col, int row, void *data)
{
    return (col == COL_CUR);
}

static gboolean 
value_is_empty (ETableModel *etm, int col, const void *value, void *data)
{
    return FALSE;
}

static char *
value_to_string (ETableModel *etm, int col, const void *val, void *data)
{
    return g_strdup (val);

}

void
create_children (GdfStackBrowser *sb)
{
    GtkWidget *e_table;
    ETableExtras *extras;
    ECell *cell_current;
    GdkPixbuf *pixbufs[2];

    sb->priv->e_table_model = 
        e_table_simple_new (col_count, row_count, NULL, value_at,
                            set_value_at, is_cell_editable,
                            NULL, NULL, NULL, NULL, NULL,
                            value_is_empty,
                            value_to_string, sb);
    
    extras = e_table_extras_new ();

    pixbufs[0] =
        gdk_pixbuf_new_from_xpm_data ((const char **)empty_15x15_xpm);
    pixbufs[1] = 
        gdk_pixbuf_new_from_xpm_data ((const char **)current_line_xpm);
    
    cell_current = e_cell_toggle_new (0, 2, pixbufs);
    e_table_extras_add_cell (extras, "arrow", cell_current);
    e_table_extras_add_pixbuf (extras, "arrow", pixbufs[1]);

    e_table = 
        e_table_scrolled_new (sb->priv->e_table_model,
                              extras, stack_browser_spec, NULL);

    gtk_container_add (GTK_CONTAINER (sb), e_table);
    gtk_widget_show (e_table);
}

static void
clear_stack_browser (GdfStackBrowser *sb)
{   
    e_table_model_pre_change (sb->priv->e_table_model);
    if (sb->priv->stack) {
        CORBA_free (sb->priv->stack);
        sb->priv->stack = NULL;
    } 
    sb->priv->current_frame = -1;
    e_table_model_changed (sb->priv->e_table_model);
}
