/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <gnome.h>
#include "gdf-output-terminal.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <config.h>
#include <signal.h>
#include <sys/wait.h>

struct _GdfOutputTerminalPriv {
    GdfDebuggerClient *dbg;
    char *tty_name;
};

enum {
    ARG_0,
    ARG_DEBUGGER
};

static void output_terminal_init (GdfOutputTerminal *ot);
static void output_terminal_class_init (GdfOutputTerminalClass *klass);
static void output_terminal_destroy (GtkObject *object);
static void get_arg (GtkObject *object, 
                     GtkArg *arg, 
                     guint arg_id);
static void set_arg (GtkObject *object, 
                     GtkArg *arg, 
                     guint arg_id);
static void connect_debugger_signals (GdfOutputTerminal *ot);
static void disconnect_debugger_signals (GdfOutputTerminal *ot);

static ZvtTermClass *parent_class;

static const char shell_script_fmt[] =
"tty > %s; \
trap \"\" INT QUIT TSTP; \
exec <&-;exec >&-;
while :; do sleep 3600; done";


/*
 * Public Interface 
 */

GtkType 
gdf_output_terminal_get_type ()
{
    static GtkType type = 0;
	
    if (!type) {
        static const GtkTypeInfo info = {
            "GdfOutputTerminal",
            sizeof (GdfOutputTerminal),
            sizeof (GdfOutputTerminalClass),
            (GtkClassInitFunc) output_terminal_class_init,
            (GtkObjectInitFunc) output_terminal_init,
            NULL,
            NULL,
            (GtkClassInitFunc)NULL
        };
		
        type = gtk_type_unique (zvt_term_get_type (), &info);
    }
	
    return type;
}

GtkWidget *
gdf_output_terminal_new ()
{
    GdfOutputTerminal *ot;
    
    ot = gtk_type_new (gdf_output_terminal_get_type ());
	
    return GTK_WIDGET (ot);
}

void
gdf_output_terminal_set_debugger (GdfOutputTerminal *ot,
                                   GdfDebuggerClient *dbg)
{
    g_return_if_fail (ot != NULL);
    g_return_if_fail (GNOME_Development_IS_OUTPUT_TERMINAL (ot));
	
    if (ot->priv->dbg) {
        disconnect_debugger_signals (ot);
        gtk_object_unref (GTK_OBJECT (ot->priv->dbg));
    }
	
    ot->priv->dbg = dbg;

    if (dbg) {
        gtk_object_ref (GTK_OBJECT (dbg));
        if (GTK_OBJECT_FLOATING (GTK_OBJECT (dbg)))
            gtk_object_sink (GTK_OBJECT (dbg));
        
        connect_debugger_signals (ot);

        if (gdf_debugger_client_set_output_tty (ot->priv->dbg, 
                                                ot->priv->tty_name) 
            != GDF_DEBUGGER_CLIENT_OK) {
            
            g_warning ("Could not set output tty");
        }
    }
}


/*
 * Class / Object functions
 */

void
output_terminal_class_init (GdfOutputTerminalClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *)klass;

    gtk_object_add_arg_type ("GdfOutputTerminal::debugger",
                             GTK_TYPE_OBJECT,
                             GTK_ARG_READWRITE, 
                             ARG_DEBUGGER);
	
    parent_class = gtk_type_class (zvt_term_get_type ());
	
    object_class->destroy = output_terminal_destroy;
    object_class->get_arg = get_arg;
    object_class->set_arg = set_arg;
}

void
output_terminal_init (GdfOutputTerminal *ot)
{
    char tty_name[50];
	char fifo_name[] = "/tmp/dryad-ttywinXXXXXX";
	char *ptr_ch;
	int pid;
	int fd, num_read;

    ot->priv = g_new0 (GdfOutputTerminalPriv, 1);
    
    ot->priv->dbg = NULL;
	
	mktemp (fifo_name);
	unlink (fifo_name); /* make sure it does not exist */
	
#ifdef HAVE_MKFIFO
	if (mkfifo (fifo_name, SIRUSR | S_IWUSER) < 0) {
		g_warning (_("Unable to create fifo for output window!"));
		g_free (fifo_name);
		return;
	}
#else
	if (mknod (fifo_name, S_IFIFO | S_IRUSR | S_IWUSR, 0) < 0) {
		g_warning (_("Unable to create fifo for output window!"));
		g_free (fifo_name);
		return;
	}
#endif
	
	pid = zvt_term_forkpty (ZVT_TERM (ot), 0);
	
	if (pid < 0) {
		g_warning (_("Unable to fork tty process for output window!"));
		g_free (fifo_name);
		return;
	}
	
	if (pid == 0) {  /* child */
		
		/* spawn a shell which runs a script which will pass its tty back
		 * through the fifo */
		gchar *shell_script = g_strdup_printf (shell_script_fmt,
											   fifo_name);
		execlp ("sh", "sh", "-c", shell_script, NULL);
		g_error (_("Unable to spawn output window!"));
	}
	
	/* parent */
	fd = open (fifo_name, O_RDONLY);
	if (fd < 0) {
		unlink (fifo_name);
		g_free (fifo_name);
		return;
	}
	
	unlink (fifo_name);
	
	num_read = read (fd, tty_name, sizeof (tty_name) - sizeof (gchar));
	close (fd);
	
	/* remove the cr at the end */
	ptr_ch = strchr (tty_name, '\n');
	if (ptr_ch)
		*ptr_ch = '\0';
	
	ot->priv->tty_name = g_strdup (tty_name);
	return;
}

void
output_terminal_destroy (GtkObject *obj)
{
    GdfOutputTerminal *ot = GNOME_Development_OUTPUT_TERMINAL (obj);
    
    if (ot->priv->dbg)
        gtk_object_unref (GTK_OBJECT (ot->priv->dbg));
    
    g_free (ot->priv->tty_name);
    g_free (ot->priv);
    
    if (GTK_OBJECT_CLASS (parent_class)->destroy)
        (*GTK_OBJECT_CLASS (parent_class)->destroy) (obj);
}

void 
get_arg (GtkObject *object, 
         GtkArg *arg, 
         guint arg_id)
{
    GdfOutputTerminal *ot = GNOME_Development_OUTPUT_TERMINAL (object);
    
    switch (arg_id) {
    case ARG_DEBUGGER :
        GTK_VALUE_OBJECT (*arg) = GTK_OBJECT (ot->priv->dbg);
        break;
    default :
        arg->type = GTK_TYPE_INVALID;
    }   
}

void 
set_arg (GtkObject *object, 
         GtkArg *arg, 
         guint arg_id)
{
    GdfOutputTerminal *ot = GNOME_Development_OUTPUT_TERMINAL (object);
    
    switch (arg_id) {
    case ARG_DEBUGGER :
        gdf_output_terminal_set_debugger (ot,
                                          GTK_VALUE_OBJECT (*arg) ? GDF_DEBUGGER_CLIENT (GTK_VALUE_OBJECT (*arg)) : NULL);
        break;
    default :
        break;
    }   
}


/* 
 * Helper functions 
 */

void
connect_debugger_signals (GdfOutputTerminal *ot)
{
}

void
disconnect_debugger_signals (GdfOutputTerminal *ot)
{
}
