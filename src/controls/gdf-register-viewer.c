/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <gnome.h>
#include "gdf-register-viewer.h"

#include <gal/widgets/e-canvas.h>
#include <gal/widgets/e-popup-menu.h>
#include <gal/e-table/e-table-header-item.h>
#include <gal/e-table/e-table-item.h>
#include <gal/util/e-util.h>
#include <gal/e-table/e-table.h>
#include <gal/e-table/e-table-header-item.h>
#include <gal/e-table/e-table-item.h>
#include <gal/e-table/e-table-scrolled.h>
#include <gal/e-table/e-table-simple.h>
#include <gal/e-table/e-cell-text.h>
#include <gal/e-table/e-cell-checkbox.h>

struct _GdfRegisterViewerPriv {
    GtkWidget *show_fp;
    
    ETableModel *e_table_model;

    GdfDebuggerClient *dbg;
    GNOME_Development_RegisterList *regs;

    int corefile_loaded_sig;
    int corefile_unloaded_sig;
    int execution_stopped_sig;
    int execution_exited_sig;
    int execution_killed_sig;
    int program_unloaded_sig;
};

enum {
    ARG_0,
    ARG_DEBUGGER
};

enum {
    COL_NAME,
    COL_VALUE,
    LAST_COL
};

static void register_viewer_init (GdfRegisterViewer *rv);
static void register_viewer_class_init (GdfRegisterViewerClass *klass);
static void register_viewer_destroy (GtkObject *object);
static void get_arg (GtkObject *object, 
                     GtkArg *arg, 
                     guint arg_id);
static void set_arg (GtkObject *object, 
                     GtkArg *arg, 
                     guint arg_id);
static void connect_debugger_signals (GdfRegisterViewer *rv);
static void disconnect_debugger_signals (GdfRegisterViewer *rv);
static void load_registers (GdfRegisterViewer *rv);
static void execution_stopped_cb (GdfDebuggerClient *dbg,
                                  GdfRegisterViewer *rv);
static void show_fp_toggled_cb (GtkWidget *toggle_bugger, 
                                GdfRegisterViewer *rv);
static void create_children (GdfRegisterViewer *rv);
static void clear_register_viewer (GdfRegisterViewer *rv);

static GtkFrameClass *parent_class;

const char *register_viewer_spec =
"<ETableSpecification click-to-add=\"false\" draw-grid=\"true\">"
"  <ETableColumn model_col=\"0\" _title=\"Reg\" expansion=\"0.25\" minimum_width=\"30\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableColumn model_col=\"1\" _title=\"Value\" expansion=\"1.0\" minimum_width=\"30\" resizable=\"true\" cell=\"string\" compare=\"string\"/>"
"  <ETableState>"
"    <column source=\"0\"/>"
"    <column source=\"1\"/>"
"    <grouping></grouping>"
"  </ETableState>"
"</ETableSpecification>";


/*
 * Public Interface 
 */

GtkType 
gdf_register_viewer_get_type ()
{
    static GtkType type = 0;
	
    if (!type) {
        static const GtkTypeInfo info = {
            "GdfRegisterViewer",
            sizeof (GdfRegisterViewer),
            sizeof (GdfRegisterViewerClass),
            (GtkClassInitFunc) register_viewer_class_init,
            (GtkObjectInitFunc) register_viewer_init,
            NULL,
            NULL,
            (GtkClassInitFunc)NULL
        };
		
        type = gtk_type_unique (gtk_frame_get_type (), &info);
    }
	
    return type;
}

GtkWidget *
gdf_register_viewer_new ()
{
    GdfRegisterViewer *rv;
    
    rv = gtk_type_new (gdf_register_viewer_get_type ());
	
    return GTK_WIDGET (rv);
}

void
gdf_register_viewer_set_debugger (GdfRegisterViewer *rv,
                                   GdfDebuggerClient *dbg)
{
    g_return_if_fail (rv != NULL);
    g_return_if_fail (GNOME_Development_IS_REGISTER_VIEWER (rv));
	
    if (rv->priv->dbg) {
        disconnect_debugger_signals (rv);
        gtk_object_unref (GTK_OBJECT (rv->priv->dbg));
    }
	
    rv->priv->dbg = dbg;

    if (dbg) {
        gtk_object_ref (GTK_OBJECT (dbg));
        if (GTK_OBJECT_FLOATING (GTK_OBJECT (dbg)))
            gtk_object_sink (GTK_OBJECT (dbg));
        
        connect_debugger_signals (rv);
    }
}


/*
 * Class / Object functions
 */

void
register_viewer_class_init (GdfRegisterViewerClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *)klass;

    gtk_object_add_arg_type ("GdfRegisterViewer::debugger",
                             GTK_TYPE_OBJECT,
                             GTK_ARG_READWRITE, 
                             ARG_DEBUGGER);
	
    parent_class = gtk_type_class (gtk_frame_get_type ());
	
    object_class->destroy = register_viewer_destroy;
    object_class->get_arg = get_arg;
    object_class->set_arg = set_arg;
}

void
register_viewer_init (GdfRegisterViewer *rv)
{
    rv->priv = g_new0 (GdfRegisterViewerPriv, 1);
    
    rv->priv->dbg = NULL;
    rv->priv->regs = NULL;
	
    create_children (rv);
}

void
register_viewer_destroy (GtkObject *obj)
{
    GdfRegisterViewer *rv = GNOME_Development_REGISTER_VIEWER (obj);
     
    if (rv->priv->dbg)
        gtk_object_unref (GTK_OBJECT (rv->priv->dbg));
    
    if (rv->priv->regs) {
        CORBA_free (rv->priv->regs);
        rv->priv->regs = NULL;
    }

    g_free (rv->priv);
    
    if (GTK_OBJECT_CLASS (parent_class)->destroy)
        (*GTK_OBJECT_CLASS (parent_class)->destroy) (obj);
}

void 
get_arg (GtkObject *object, 
         GtkArg *arg, 
         guint arg_id)
{
    GdfRegisterViewer *rv = GNOME_Development_REGISTER_VIEWER (object);
    
    switch (arg_id) {
    case ARG_DEBUGGER :
        GTK_VALUE_OBJECT (*arg) = GTK_OBJECT (rv->priv->dbg);
        break;
    default :
        arg->type = GTK_TYPE_INVALID;
    }   
}

void 
set_arg (GtkObject *object, 
         GtkArg *arg, 
         guint arg_id)
{
    GdfRegisterViewer *rv = GNOME_Development_REGISTER_VIEWER (object);
    
    switch (arg_id) {
    case ARG_DEBUGGER :
        gdf_register_viewer_set_debugger (rv,
                                          GTK_VALUE_OBJECT (*arg) ? GDF_DEBUGGER_CLIENT (GTK_VALUE_OBJECT (*arg)) : NULL);
        break;
    default :
        break;
    }   
}

/* 
 * Helper functions 
 */

void
connect_debugger_signals (GdfRegisterViewer *rv)
{
    rv->priv->corefile_loaded_sig = 
        gtk_signal_connect (GTK_OBJECT (rv->priv->dbg), 
                            "corefile_loaded",
                            GTK_SIGNAL_FUNC (execution_stopped_cb),
                            (gpointer)rv);
    rv->priv->corefile_unloaded_sig = 
        gtk_signal_connect_object (GTK_OBJECT (rv->priv->dbg),
                                   "corefile_unloaded",
                                   GTK_SIGNAL_FUNC (clear_register_viewer),
                                   (gpointer)rv);
    rv->priv->execution_stopped_sig = 
        gtk_signal_connect (GTK_OBJECT (rv->priv->dbg), 
                            "execution_stopped",
                            GTK_SIGNAL_FUNC (execution_stopped_cb),
                            (gpointer)rv);
    rv->priv->execution_exited_sig = 
        gtk_signal_connect_object (GTK_OBJECT (rv->priv->dbg),
                                   "execution_exited",
                                   GTK_SIGNAL_FUNC (clear_register_viewer),
                                   (gpointer)rv);
    rv->priv->execution_killed_sig = 
        gtk_signal_connect_object (GTK_OBJECT (rv->priv->dbg),
                                   "execution_killed",
                                   GTK_SIGNAL_FUNC (clear_register_viewer),
                                   (gpointer)rv);
    rv->priv->program_unloaded_sig = 
        gtk_signal_connect_object (GTK_OBJECT (rv->priv->dbg),
                                   "program_unloaded",
                                   GTK_SIGNAL_FUNC (clear_register_viewer),
                                   (gpointer)rv);
}

void
disconnect_debugger_signals (GdfRegisterViewer *rv)
{
    gtk_signal_disconnect (GTK_OBJECT (rv->priv->dbg),
                           rv->priv->corefile_loaded_sig);
    gtk_signal_disconnect (GTK_OBJECT (rv->priv->dbg),
                           rv->priv->corefile_unloaded_sig);
    gtk_signal_disconnect (GTK_OBJECT (rv->priv->dbg),
                           rv->priv->execution_stopped_sig);
    gtk_signal_disconnect (GTK_OBJECT (rv->priv->dbg), 
                           rv->priv->execution_exited_sig);
    gtk_signal_disconnect (GTK_OBJECT (rv->priv->dbg),
                           rv->priv->program_unloaded_sig);
}

void
load_registers (GdfRegisterViewer *rv)
{
    GdfDebuggerClientResult res;
    e_table_model_pre_change (rv->priv->e_table_model);

    if (rv->priv->regs) {
        CORBA_free (rv->priv->regs);
    }

    res = gdf_debugger_client_get_registers (rv->priv->dbg,
                                             (gboolean)GTK_TOGGLE_BUTTON (rv->priv->show_fp)->active,
                                             &rv->priv->regs);

    e_table_model_changed (rv->priv->e_table_model);
}

void
execution_stopped_cb (GdfDebuggerClient *dbg, 
                      GdfRegisterViewer *rv)
{
    load_registers (rv);
}

void
show_fp_toggled_cb (GtkWidget *toggle_bugger, GdfRegisterViewer *rv)
{
    if (rv->priv->dbg) {
        load_registers (rv);
    }
}

static int
col_count (ETableModel *etm, void *data)
{
    return LAST_COL;
}

static int
row_count (ETableModel *etm, void *data)
{
    GdfRegisterViewer *rv = data;
    if (rv->priv->regs) {
        return rv->priv->regs->_length;
    } else {
        return 0;
    }
}

static void *
value_at (ETableModel *etm, int col, int row, void *data)
{
    GdfRegisterViewer *rv = data;
    if (rv->priv->regs) {                         
        if (col == COL_NAME) {
            return rv->priv->regs->_buffer[row].name;
        } else {
            return rv->priv->regs->_buffer[row].value;
        }
    } else {
        return 0;
    }
}

static void
set_value_at (ETableModel *etm, int col, int row, const void *val, void *data)
{
    GdfRegisterViewer *rv = data;
    GdfDebuggerClientResult res;
    char *new_val;
    
    g_return_if_fail (col == COL_VALUE);

    res = gdf_debugger_client_set_register_value (rv->priv->dbg,
                                                  rv->priv->regs->_buffer[row].name,
                                                  val);
    if (res == GDF_DEBUGGER_CLIENT_OK) {
        CORBA_free (rv->priv->regs->_buffer[row].value);
        res = gdf_debugger_client_get_register_value (rv->priv->dbg,
                                                      rv->priv->regs->_buffer[row].name,
                                                      &new_val);
        if (res == GDF_DEBUGGER_CLIENT_OK) {
            e_table_model_pre_change (etm);
            rv->priv->regs->_buffer[row].value = CORBA_string_dup (new_val);
            g_free (new_val);
            e_table_model_row_changed (etm, row);
        }
    } 
}

static gboolean 
is_cell_editable (ETableModel *etm, int col, int row, void *data)
{
    return (col == COL_VALUE);
}

static gboolean 
value_is_empty (ETableModel *etm, int col, const void *value, void *data)
{
    return !(value && *(char*)value);
}

static char *
value_to_string (ETableModel *etm, int col, const void *val, void *data)
{
    return g_strdup (val);
}

void
create_children (GdfRegisterViewer *rv)
{
    GtkWidget *e_table;
    GtkWidget *vbox;

    rv->priv->show_fp = 
        gtk_check_button_new_with_label (_("Include floating point"));

    gtk_signal_connect (GTK_OBJECT (rv->priv->show_fp), "toggled",
                        GTK_SIGNAL_FUNC (show_fp_toggled_cb), rv);

    rv->priv->e_table_model = 
        e_table_simple_new (col_count, row_count, NULL, value_at,
                            set_value_at, is_cell_editable,
                            NULL, NULL, NULL, NULL, NULL,
                            value_is_empty,
                            value_to_string, rv);
    
    e_table = 
        e_table_scrolled_new (rv->priv->e_table_model,
                              NULL, register_viewer_spec,
                              NULL);

    vbox = gtk_vbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), rv->priv->show_fp, 
                        FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), e_table,
                        TRUE, TRUE, 0);
    gtk_container_add (GTK_CONTAINER (rv), vbox);
    gtk_widget_show (rv->priv->show_fp);
    gtk_widget_show (e_table);
    gtk_widget_show (vbox);
}

void
clear_register_viewer (GdfRegisterViewer *rv)
{
    e_table_model_pre_change (rv->priv->e_table_model);
    CORBA_free (rv->priv->regs);
    rv->priv->regs = NULL;
    e_table_model_changed (rv->priv->e_table_model);
}

